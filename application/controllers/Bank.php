<?php

/**
 * Created by PhpStorm.
 * User: development
 * Date: 1/1/2019
 * Time: 3:46 PM
 */
class Bank extends Global_Controller
{
    //Contains Primary model object
    private $pModel = "";

    //declare reusable variables
    private $folder = "bank";
    private $moduleName = "Bank Accounts";

    function __construct()
    {
        parent::__construct();
        //Load models here
        $this->load->model("Bank_model");
        $this->pModel = $this->Bank_model;
    }


    // Show listing
    public function index()
    {

        $data = array(
            "heading" => $this->moduleName . " Managment",
            "title" => $this->moduleName . " Managment",
            "description" => "Manage " . $this->moduleName . " from here !",
            "pmKey" => $this->pModel->tableId
        );

        //Set columns for grid
        $data['cols'] = array(
            "bank_name" => "Bank Name",
            "bank_iban" => "Bank Iban",
            "bank_address" => "Bank Address",
            "bank_swift" => "Bank Swift",
        );

        //fetch data from database
        $data['items'] = $this->pModel->getData();
        /*echo $this->db->last_query();
        exit;*/
        $this->template($this->folder . "/listing", $data);
    }

    // Add & Submit form
    public function add()
    {

        $data = array(
            "heading" => $this->moduleName . " Managment",
            "title" => $this->moduleName,
            "description" => "Manage " . $this->moduleName . "  from here !"
        );

        //check if form is submitted
        if ($this->input->post("btn_submit") !== NULL) {

            $item = array(
                "bank_name" => $this->input->post("bank_name"),
                "bank_iban" => $this->input->post("bank_iban"),
                "bank_address" => $this->input->post("bank_Address"),
                "bank_swift" => $this->input->post("bank_swift"),
                "created_at" => date('Y-m-d H:i:s'),
            );
            $message = $this->pModel->add($item);
            if ($message) {
                $this->session->set_flashdata("msg", '<div class="alert alert-success">' . $this->moduleName . ' Saved successfully!</div>');
            } else {
                $this->session->set_flashdata("msg", '<div class="alert alert-danger">Error occured! </div>');
            }

        }
        $this->template($this->folder . "/add", $data);
    }

    // View & update form
    public function edit($id = 0)
    {

        $data = array(
            "heading" => $this->moduleName . " Managment",
            "title" => "Edit " . $this->moduleName,
            "description" => "Manage " . $this->moduleName . " from here !",
            "pmKey" => $this->pModel->tableId
        );

        //check if form is submitted
        if ($this->input->post("btn_submit") !== NULL) {

            //if file selected else only update without file
            $item = array(
                "bank_name" => $this->input->post("bank_name"),
                "bank_iban" => $this->input->post("bank_iban"),
                "bank_address" => $this->input->post("bank_Address"),
                "bank_swift" => $this->input->post("bank_swift"),
                "updated_at" => date('Y-m-d H:i:s'),
            );
            $this->pModel->edit($item, $id);

            $this->session->set_flashdata("msg", '<div class="alert alert-success">' . $this->moduleName . ' Saved successfully!</div>');
        }

        //Get data by id
        $data['item'] = $this->pModel->view($id);
        $this->template($this->folder . "/edit", $data);
    }


    // Delete  Record
    public function delete($id = 0)
    {
        $this->pModel->delete($id);
        $this->session->set_flashdata("msg", '<div class="alert alert-success">' . $this->moduleName . ' deleted successfully!</div>');
        redirect($this->uri->segment(1));
    }

}