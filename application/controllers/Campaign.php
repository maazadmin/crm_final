<?php



class Campaign extends Global_Controller
{
	//Contains Primary model object
    private $pModel = "";

    //declare reusable variables
    private $folder = "campaign";
    private $moduleName = "Campaign";
	
	function __construct()
	{
		parent::__construct();
		$this->load->model("Campaign_model");
        $this->pModel = $this->Campaign_model;
	}


	public function index()
    {

        $data = array(
            "heading" => $this->moduleName . " Managment",
            "title" => $this->moduleName . " Managment",
            "description" => "Manage " . $this->moduleName . " from here !",
            "pmKey" => $this->pModel->tableId
        );

        //Set columns for grid
        $data['cols'] = array(
            "campaign_name" => "Campaign Name",
        );

        //fetch data from database
        $data['items'] = $this->pModel->getData();

        $this->template($this->folder . "/listing", $data);
    }

    // Add & Submit form
    public function add()
    {
        $data = array(
            "heading" => $this->moduleName . " Managment",
            "title" => $this->moduleName,
            "description" => "Manage " . $this->moduleName . "  from here !"
        );

        //check if form is submitted
        if ($this->input->post("btn_submit") !== NULL) {

            $item = array(
                "campaign_name" => $this->input->post("campaign_name"),
                "created_at" => date('H:i:s Y-m-d')
            );
            $message = $this->pModel->add($item);
            if ($message) {
                $this->session->set_flashdata("msg", '<div class="alert alert-success">' . $this->moduleName . ' Saved successfully!</div>');
                redirect($this->uri->segment(1));
            } else {
                $this->session->set_flashdata("msg", '<div class="alert alert-danger">Error occured! </div>');
            }


        }
        $this->template($this->folder . "/add", $data);
    }

    // View & update form
    public function edit($id = 0)
    {
        $data = array(
            "heading" => $this->moduleName . " Managment",
            "title" => "Edit " . $this->moduleName,
            "description" => "Manage " . $this->moduleName . " from here !",
            "pmKey" => $this->pModel->tableId
        );

        //check if form is submitted
        if ($this->input->post("btn_submit") !== NULL) {


            //if file selected else only update without file
            $item = array(
                "campaign_name" => $this->input->post("campaign_name"),
                "updated_at" => date('H:i:s Y-m-d')
            );
            $mes = $this->pModel->edit($item, $id);
            // echo $this->db->last_query();
            // exit();

            // if(!empty($mes)){
                $this->session->set_flashdata("msg", '<div class="alert alert-success">' . $this->moduleName . ' Update successfully!</div>');

                 redirect($this->uri->segment(1));  
            // }
          

        }

        //Get data by id
        $data['item'] = $this->pModel->view($id);
        $this->template($this->folder . "/edit", $data);
    }


    // Delete  Record
    public function delete($id = 0)
    {
        $this->pModel->delete($id);
        $this->session->set_flashdata("msg", '<div class="alert alert-success">' . $this->moduleName . ' deleted successfully!</div>');
        redirect($this->uri->segment(1));
    }

}