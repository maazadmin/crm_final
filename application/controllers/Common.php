<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Common extends Global_Controller {

    function __construct()
    {
        parent::__construct();
    }

    public function fill() {

        $response = array();
        if ( $this->input->post('table') !== NULL ) {
            $table = $this->input->post('table');
            $column = $this->input->post('column');
            $value = $this->input->post('value');

            $this->load->model('Base_model');
            $filter = array(
                "table" => $table,
                "column" => $column,
                "value" => $value
            );
            $data = $this->Base_model->get($filter);

            $response['data'] = $data;
            $response['status'] = 1;
        } else {
            $response['data'] = "";
            $response['status'] = 0;
        }

        $this->apiOutput($response);
    }

}
