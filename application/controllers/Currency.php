<?php



class Currency extends Global_Controller
{
	//Contains Primary model object
    private $pModel = "";

    //declare reusable variables
    private $folder = "currency";
    private $moduleName = "Currency";
	
	function __construct()
	{
		parent::__construct();
		$this->load->model("Currency_model");
        $this->pModel = $this->Currency_model;
	}


	public function index()
    {
        
// echo 'Pakistan';
// exit();
        $data = array(
            "heading" => $this->moduleName . " Managment",
            "title" => $this->moduleName . " Managment",
            "description" => "Manage " . $this->moduleName . " from here !",
            "pmKey" => $this->pModel->tableId
        );

        //Set columns for grid
        $data['cols'] = array(
            "currency_name" => "Currency Name",
            "region_name" => "Region Name",
            "currency_symbol" => "Currency Sysmbol",
        );
       

        //fetch data from database
        $data['items'] = $this->pModel->getData();
        // echo $this->db->last_query();
        // exit();

        $this->template($this->folder . "/listing", $data);
    }

    // Add & Submit form
    public function add()
    {
        
        $data = array(
            "heading" => $this->moduleName . " Managment",
            "title" => $this->moduleName,
            "description" => "Manage " . $this->moduleName . "  from here !"
        );
          //Set columns for grid
       

        //check if form is submitted
        if ($this->input->post("btn_submit") !== NULL) {



            $item = array(
                "currency_name" => $this->input->post("currency_name"),
                "region_id" => $this->input->post("region_id"),
                "currency_symbol" => $this->input->post("currency_symbol"),
                "created_at" => date('H:i:s Y-m-d')
            );
            $message = $this->pModel->add($item);
            if ($message) {
                $this->session->set_flashdata("msg", '<div class="alert alert-success">' . $this->moduleName . ' Saved successfully!</div>');
                redirect($this->uri->segment(1));
            } else {
                $this->session->set_flashdata("msg", '<div class="alert alert-danger">Error occured! </div>');
            }


        }




       // foreach ($data['region_dropdownn'] as $drop_value) {
       //    echo '<pre>';
       //    print_r($drop_value);
       //    echo '</pre>';
       // }
            $this->load->model("Region_model");
       $data['region_dropdownn'] = $this->Region_model->getData();
        $this->template($this->folder . "/add", $data);
    }

    // View & update form
    public function edit($id = 0)
    {
        $data = array(
            "heading" => $this->moduleName . " Managment",
            "title" => "Edit " . $this->moduleName,
            "description" => "Manage " . $this->moduleName . " from here !",
            "pmKey" => $this->pModel->tableId
        );

        //check if form is submitted
        if ($this->input->post("btn_submit") !== NULL) {


            //if file selected else only update without file
            $item = array(
                "currency_name" => $this->input->post("currency_name"),
                "region_id" => $this->input->post("region_id"),
                "currency_symbol" => $this->input->post("currency_symbol"),
                "updated_at" => date('H:i:s Y-m-d')
            );
            $this->pModel->edit($item, $id);
            // echo $this->db->last_query();
            // exit();
            $this->session->set_flashdata("msg", '<div class="alert alert-success">' . $this->moduleName . ' Update successfully!</div>');
                redirect($this->uri->segment(1));
            

        }
$this->load->model("Region_model");
       $data['region_dropdownn'] = $this->Region_model->getData();
        //Get data by id
        $data['item'] = $this->pModel->view($id);
        $this->template($this->folder . "/edit", $data);
    }


    // Delete  Record
    public function delete($id = 0)
    {
        $this->pModel->delete($id);
        $this->session->set_flashdata("msg", '<div class="alert alert-success">' . $this->moduleName . ' deleted successfully!</div>');
        redirect($this->uri->segment(1));
    }

}