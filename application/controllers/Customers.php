<?php

/**
 * Created by PhpStorm.
 * User: development
 * Date: 1/4/2019
 * Time: 10:11 AM
 */
class Customers extends Global_Controller
{
    ///Contains Primary model object
    private $pModel = "";

    ///declare reusable variables
    private $folder = "customers";
    private $moduleName = "Customers";

    function __construct()
    {
        parent::__construct();
        //Load models here
        $this->load->model("Customers_model");
        $this->pModel = $this->Customers_model;
    }


    // Show listing
    public function index()
    {

        $data = array(
            "heading" => $this->moduleName . " Managment",
            "title" => $this->moduleName . " Managment",
            "description" => "Manage " . $this->moduleName . " from here !",
            "pmKey" => $this->pModel->tableId
        );

        //Set columns for grid
        /* $data['cols'] = array(
             "customer_name" => "Name",
             "customer_email" => "Email",
             "customer_phone" => "Phone",
             "customer_dob_month" => "Month",
             "customer_dob_year" => "Year",
             // "description" => "Description",
             //"path" => "File"
         );*/

        $data['cols'] = array(
            "title" => "Title",
            "person" => "Person",
            "person_email" => "Email",
            "telephone" => "Telephone",
            "designation" => "Designation",
            "view_detail" => "View Detail",
            //"path" => "File"
        );

        //fetch data from database
        $data['items'] = $this->pModel->getData();
        $this->template($this->folder . "/listing", $data);
    }
    //Customer Details
    public function  view_detail($id  = 0){
        $data = array(
            "heading" => $this->moduleName . " Managment",
            "title" => $this->moduleName . " Managment",
            "description" => "Manage " . $this->moduleName . " from here !",
            "pmKey" => $this->pModel->tableId
        );

        //Set columns for grid
        /* $data['cols'] = array(
             "customer_name" => "Name",
             "customer_email" => "Email",
             "customer_phone" => "Phone",
             "customer_dob_month" => "Month",
             "customer_dob_year" => "Year",
             // "description" => "Description",
             //"path" => "File"
         );*/

        $data['cols'] = array(
            "title" => "Title",
            "address"  => "Address",
            "city"  => "City",
            "country"  => "Country",
            "person"  => "Person",
            "person_email" => "Person email",
            "designation"  => "Designation",
            "telephone"  => "Telephone",
            "fax" => "Fax",
            "email" => "Email",
            "cell" => "Cell",
        );

        //fetch data from database
        $data['items'] = $this->pModel->getData($id);
        $this->template($this->folder . "/view_detail", $data);
    }

    // Add & Submit form
    public function add()
    {
        $data = array(
            "heading" => $this->moduleName . " Managment",
            "title" => $this->moduleName,
            "description" => "Manage " . $this->moduleName . "  from here !"
        );

        //check if form is submitted
        if ($this->input->post("btn_submit") !== NULL) {


            $item = array(
                "title" => $this->input->post("title"),
                "address" => $this->input->post("address"),
                "city" => $this->input->post("city"),
                "country" => $this->input->post("country"),
                "person" => $this->input->post("person"),
                "designation" => $this->input->post("designation"),
                "telephone" => $this->input->post("telephone"),
                "fax" => $this->input->post("fax"),
                "email" => $this->input->post("email"),
                "cell" => $this->input->post("cell"),
                "person_email" => $this->input->post("person_email"),
                "created_at" => date('H:i:s Y-m-d'),
            );
            /*  $item = array(
                  "customer_name" => $this->input->post("customer_name"),
                  "customer_email" => $this->input->post("email"),
                  "customer_phone" => $this->input->post("phone"),
                  "customer_dob_month" => $this->input->post("dob_month"),
                  "customer_dob_year" => $this->input->post("dob_year"),
                  "created_at" => date('H:i:s Y-m-d'),
              );*/
            $message = $this->pModel->add($item);
            if ($message) {
                $this->session->set_flashdata("msg", '<div class="alert alert-success">' . $this->moduleName . ' Saved successfully!</div>');
            } else {
                $this->session->set_flashdata("msg", '<div class="alert alert-danger">Error occured! </div>');
            }


        }
        $this->template($this->folder . "/add", $data);
    }

    // View & update form
    public function edit($id = 0)
    {
        $data = array(
            "heading" => $this->moduleName . " Managment",
            "title" => "Edit " . $this->moduleName,
            "description" => "Manage " . $this->moduleName . " from here !",
            "pmKey" => $this->pModel->tableId
        );

        //check if form is submitted
        if ($this->input->post("btn_submit") !== NULL) {

            //if file selected else only update without file

            $item = array(
                "title" => $this->input->post("title"),
                "address" => $this->input->post("address"),
                "city" => $this->input->post("city"),
                "country" => $this->input->post("country"),
                "person" => $this->input->post("person"),
                "designation" => $this->input->post("designation"),
                "telephone" => $this->input->post("telephone"),
                "fax" => $this->input->post("fax"),
                "email" => $this->input->post("email"),
                "cell" => $this->input->post("cell"),
                "person_email" => $this->input->post("person_email"),
                "updated_at" => date('H:i:s Y-m-d'),
            );

            /*$item = array(
                "customer_name" => $this->input->post("customer_name"),
                "customer_email" => $this->input->post("email"),
                "customer_phone" => $this->input->post("phone"),
                "customer_dob_month" => $this->input->post("dob_month"),
                "customer_dob_year" => $this->input->post("dob_year"),
                "updated_at" => date('H:i:s Y-m-d'),
            );*/
            $this->pModel->edit($item, $id);
            $this->session->set_flashdata("msg", '<div class="alert alert-success">' . $this->moduleName . ' Saved successfully!</div>');

        }

        //Get data by id
        $data['item'] = $this->pModel->view($id);
        $this->template($this->folder . "/edit", $data);
    }


    // Delete  Record
    public function delete($id = 0)
    {
        $this->pModel->delete($id);
        $this->session->set_flashdata("msg", '<div class="alert alert-success">' . $this->moduleName . ' deleted successfully!</div>');
        redirect($this->uri->segment(1));
    }
}