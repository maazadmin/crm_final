<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends Global_Controller {

    function __construct()
    {
        parent::__construct();
    }

    public function index()
	{
        $this->load->model("Account_types_model");
        $this->load->model("Departments_model");
        $this->load->model("Gl_accounts_model");
        $this->load->model("Journal_entries_model");
        $this->load->model("Products_model");
        $this->load->model("Purchase_invoices_model");
        $this->load->model("Purchase_orders_model");
        $this->load->model("Report_model");
        $this->load->model("Roles_model");
        $this->load->model("Suppliers_model");
        $this->load->model("Users_model");
        $this->load->model("Warehouses_model");
        $this->load->model("Job_model");
		/*echo "Hello"; d
		exit;*/

        $data['count_ac_type']    = $this->Account_types_model->getData();
        $data['count_gl_account'] = $this->Gl_accounts_model->getData();

        $data['count_department'] = $this->Departments_model->getData();
        $data['count_user']       = $this->Users_model->getData();
        $data['count_roles']      = $this->Roles_model->getData();
        $data['count_warehouses'] = $this->Warehouses_model->getData();
        $data['count_purchase_invoices'] = $this->Purchase_invoices_model->getData();
        $data['count_purchase_order']    = $this->Purchase_orders_model->getData();
        $data['count_product']    = $this->Products_model->getData();
        $data['count_supplier']   = $this->Suppliers_model->getData();
        $data['count_getInvoice'] = $this->Job_model->getInvoice();
        $data['count_journal_entries'] = $this->Journal_entries_model->getData();

        /*print_r($data['count_ac_type']) ;
        exit;*/
		$this->template('dashboard',$data);
	}

    //Unauthorized
    public function unauthorized()
    {
        $this->template('unauthorized');
    }

}
