<?php



class Deal extends Global_Controller
{
	//Contains Primary model object
    private $pModel = "";
    public $last_id = "";
    //declare reusable variables
    private $folder = "deal";
    private $moduleName = "Deal";
	
	function __construct()
	{
		parent::__construct();
		$this->load->model("Deal_model");
        $this->pModel = $this->Deal_model;
        $this->load->model("Customer_info_model");
        $this->load->model("Region_model");
        $this->load->model("Deal_product");
        $this->load->model("Product_model");
        $this->load->model("Loan_info");
        $this->load->model("Payment_info_model");
        $this->load->model("Insert_notes_model");
        $this->load->model("Payment_Option_Model");
        $this->load->library('email');
	}


	public function index()
    {

        $data = array(
            "heading" => $this->moduleName . " Managment",
            "title" => $this->moduleName . " Managment",
            "description" => "Manage " . $this->moduleName . " from here !",
            // "pmKey" => $this->pModel->tableId
        );

        // //Set columns for grid
        // $data['cols'] = array(
        //     "campaign_name" => "Campaign Name",
        // );

        //fetch data from database
        //$data['items'] = $this->pModel->getData();
        $data['region_val'] = $this->Region_model->getData();
        $data['pro_val'] = $this->Product_model->getDataAdd();

        $this->template($this->folder . "/add", $data);
    }

    // Add & Submit form
    public function add()
    {
        $data = array(
            "heading" => $this->moduleName . " Managment",
            "title" => $this->moduleName,
            "description" => "Manage " . $this->moduleName . "  from here !"
        );
        $input_post =$this->input->post();
        //check if form is Draft submitted 
        if ($this->input->post("draft_customer_info") !== NULL) {

         //   $this->db->trans_start();
            $item = array(
                "user_id" => $this->session->userdata("id"),
                "admin_id" => $this->session->userdata('admin_id'),
                "deal_status" => 1,
                "created_at" => date('Y-m-d H:i:s')

            );
            $message1 = $this->pModel->add($item);
            $last_id = $this->db->insert_id();
                $item_customer_info = array(
                "deal_id" => $last_id,
                "admin_id" => $this->session->userdata('admin_id'),
                "first_name" => $input_post['first_name'],
                "middle_initial" => $input_post['middle_initial'],
                "last_name" => $input_post['last_name'],
                "street_address" => $input_post['street_address'],
                "zip_code" => $input_post['zip_code'],
                "state_province_code" => $input_post['state_province'],
                "country" => $input_post['country'],
                "home_phone" => $input_post['home_phone'],
                "work_phone" => $input_post['work_phone'],
                "mobile_phone" => $input_post['mobile_phone'],
                "email_address" => $input_post['email_address'],
                "website" => $input_post['website'],
                "date_of_birth" => $input_post['date_of_birth'],
                "ssn" => $input_post['ssn'],
                "mmn" => $input_post['mmn'],
                "created_at" => date('Y-m-d H:i:s')
            );
                $this->log($last_id,'null',$item_customer_info,'Created deal','comment');

            $message = $this->Customer_info_model->add($item_customer_info);
          
            if ($message) {
                $this->session->set_flashdata("msg", '<div class="alert alert-success">'.'Customer Info Saved successfully!</div>');
                $final_url =  $this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$last_id.'?step=product';

                redirect($final_url);
            } else {
                $this->session->set_flashdata("msg", '<div class="alert alert-danger">Error occured! </div>');
            }


            }
            // else  if ($this->input->post("save_customer_info") !== NULL) {

            //  $item = array(
            //                 "user_id" => $this->session->userdata("id"),
            //                 "admin_id" => $this->session->userdata('admin_id'),
            //                 "deal_status" => 1,
            //                 "created_at" => date('Y-m-d H:i:s')

            //             );

            // $message1 = $this->pModel->add($item);
            // $last_id = $this->db->insert_id();

            // $item_customer_info = array(
            // "deal_id" => $last_id,
            // "admin_id" => $this->session->userdata('admin_id'),
            // "first_name" => $input_post['first_name'],
            // "middle_initial" => $input_post['middle_initial'],
            // "last_name" => $input_post['last_name'],
            // "street_address" => $input_post['street_address'],
            // "zip_code" => $input_post['zip_code'],
            // "state_province_code" => $input_post['state_province'],
            // "country" => $input_post['country'],
            // "home_phone" => $input_post['home_phone'],
            // "work_phone" => $input_post['work_phone'],
            // "mobile_phone" => $input_post['mobile_phone'],
            // "email_address" => $input_post['email_address'],
            // "website" => $input_post['website'],
            // "date_of_birth" => $input_post['date_of_birth'],
            // "ssn" => $input_post['ssn'],
            // "mmn" => $input_post['mmn'],
            // "created_at" => date('Y-m-d H:i:s')
            // );

            // $message = $this->Customer_info_model->add($item_customer_info);

            // if ($message) {
            //     $this->session->set_flashdata("msg", '<div class="alert alert-success">' . $this->moduleName . ' Saved successfully!</div>');
            //       $final_url =   $this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$last_id.'?step=product';
            //      redirect($final_url);
            // } else {
            //     $this->session->set_flashdata("msg", '<div class="alert alert-danger">Error occured! </div>');
            // }


        //}
           if ($this->input->post("draft_deal_product") !== NULL) {

                $item_customer_info = array(
                "deal_id" => $input_post['product_last_id'],
                "admin_id" => $this->session->userdata('admin_id'),
                "product_name" => $input_post['product_name'],
                "product_description" => $input_post['product_description'],
                "created_at" => date('Y-m-d H:i:s')
            );

             $this->log($input_post['product_last_id'],'null',$item_customer_info,'Deal Product Add','comment');
            $message = $this->Deal_product->add($item_customer_info);

            if ($message) {
                $this->session->set_flashdata("msg", '<div class="alert alert-success">' . 'Product Saved successfully!</div>');
                $final_url =  $this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$input_post['product_last_id'].'?step=loan';

                redirect($final_url);
            } else {
                $this->session->set_flashdata("msg", '<div class="alert alert-danger">Error occured! </div>');
            }


        }



        // else if ($this->input->post("save_deal_product") !== NULL) {
        //         $item_customer_info = array(
        //         "deal_id" => $input_post['product_last_id'],
        //         "admin_id" => $this->session->userdata('admin_id'),
        //         "product_name" => $input_post['product_name'],
        //         "product_description" => $input_post['product_description'],
        //         "created_at" => date('Y-m-d H:i:s')
        //     );

        //     $message = $this->Deal_product->add($item_customer_info);

        //     if ($message) {
        //         $this->session->set_flashdata("msg", '<div class="alert alert-success">' . $this->moduleName . ' Saved successfully!</div>');
        //           $final_url =  $this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$input_post['product_last_id'].'?step=loan';

        //         redirect($final_url);
        //     } else {
        //         $this->session->set_flashdata("msg", '<div class="alert alert-danger">Error occured! </div>');
        //     }


        // }
        // if ($this->input->post("draft_loan_info") !== NULL) {
        //     $c1 =  $input_post['cc1'];
        //     $c2 = $input_post['cc2'];
        //     $c3 = $input_post['cc3'];
        //     $c4 = $input_post['cc4'];
        //     $month = $input_post['month'];
        //     $year = $input_post['year'];
        //     $final_exp = $month.'-'.$year;
        //     $final_cc = $c1.'-'.$c2.'-'.$c3.'-'.$c4;

        //     $item_loan_info = array(
        //         "deal_id" => $input_post['loan_last_id'],
        //         "admin_id" => $this->session->userdata('admin_id'),
        //         "select_loan_type" => $input_post['select_loan_type'],
        //         "contact" => $input_post['contact'],
        //         "lender_name" => $input_post['lender_name'],
        //         "start_date" => $input_post['start_date'],
        //         "end_date" => $input_post['end_date'],
        //         "account_number" => $input_post['account_number1'],
        //         "cc" => $final_cc,
        //         "exp_date" => $final_exp,
        //         "cvc" => $input_post['cvc1'],
        //         "apr" => $input_post['apr'],
        //         "total_owe" => $input_post['total_owe'],
        //         "available_balance" => $input_post['available_balance'],
        //         "min_payment" => $input_post['min_payment'],
        //         "last_payment" => $input_post['last_payment'],
        //         "next_payment" => $input_post['next_payment'],
        //         "annual_intrest_amount" => $input_post['annual_interest'],
        //         "paid_off_duration" => $input_post['paid_off_duration'],
        //         "created_at" => date('Y-m-d H:i:s')
        //     );

        //     $message = $this->Loan_info->add($item_loan_info);

        //     if ($message) {
        //         $this->session->set_flashdata("msg", '<div class="alert alert-success">' . $this->moduleName . ' Saved successfully!</div>');
        //         $final_url =  $this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$input_post['loan_last_id'].'?step=payment';

        //         redirect($final_url);
        //     } else {
        //         $this->session->set_flashdata("msg", '<div class="alert alert-danger">Error occured! </div>');
        //     }


        // }
        if ($this->input->post("save_loan_info") !== NULL) {
            // new work
            $input_post = $this->input->post();
            for ($i=0; $i < count($input_post['account_number1_new']); $i++) { 
            $item_loan_info = array(
                "deal_id" => $input_post['loan_last_id'],
                "admin_id" => $this->session->userdata('admin_id'),
                "select_loan_type" => $input_post['select_loan_type_new'][$i],
                "contact" => $input_post['contact_new'][$i],
                "start_date" => $input_post['start_date_new'][$i],
                "end_date" => $input_post['end_date_new'][$i],
                "min_payment" => $input_post['min_payment_new'][$i],
                "last_payment" => $input_post['last_payment_new'][$i],
                "next_payment" => $input_post['next_payment_new'][$i],
                "annual_intrest_amount" => $input_post['annual_interest_new'][$i],
                "paid_off_duration" => $input_post['paid_off_duration_new'][$i],
                "cc" => $input_post['cc_new'][$i],
                "lender_name" => $input_post['lender_name_new'][$i],
                "account_number" => $input_post['account_number1_new'][$i],
                "total_owe" => $input_post['total_owe_new'][$i],
                "available_balance" => $input_post['available_balance_new'][$i],
                "apr" => $input_post['apr_new'][$i],
                "cvc" => $input_post['cvc1_new'][$i],
                "exp_date" => $input_post['exp_new'][$i],
                "created_at" => date('Y-m-d H:i:s'),
            );
            $this->log($input_post['loan_last_id'],'null',$item_loan_info,'Loan Info Add','comment');
            $message = $this->Loan_info->add($item_loan_info);
        }

 // echo $this->db->last_query();
 // exit();
           // $this->db->trans_complete();

            if ($message) {
                $this->session->set_flashdata("msg", '<div class="alert alert-success">' .  'Loan Info Saved successfully!</div>');
                 $final_url =  $this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$input_post['loan_last_id'].'?step=payment';

                redirect($final_url);
            } else {
                $this->session->set_flashdata("msg", '<div class="alert alert-danger">Error occured! </div>');
            }


        }
         if ($this->input->post("draft_payment_info") !== NULL) {

           // $this->db->trans_start();
            $input_post =$this->input->post();
            // $item = array(
            //     "user_id" => $this->session->userdata("id"),
            //     "admin_id" => 0,
            //     "deal_status" => 0,
            //     "created_at" => date('Y-m-d H:i:s')

            // );
            // $message1 = $this->pModel->add($item);

           // $last_id = $this->db->insert_id();

            $ac =  $input_post['ac_number'];
            $ex = $input_post['exp'];
            $cv =  $input_post['cvc'];
            $final_amount =   $ac.$ex.$cv;

  $item_payment_info = array(
                "deal_id" => $input_post['payment_last_id'],
                "admin_id" => $this->session->userdata('admin_id'),
                "payment_product" => $input_post['payment_product'],
                "payment_mode" => $input_post['mode_of_payment'],
                "package_price" => $input_post['package_price'],
                "special_instruction" => $input_post['special_instruction'],
                "billing_address" => $input_post['billing_address'],
                "transaction_id" => $input_post['transaction_id'],
                "descriptor" => $input_post['descriptor'],
                "dateandtime" => $input_post['dateandtime'],
                "created_at" => date('Y-m-d H:i:s')
            );


            for ($i=0; $i < count($input_post['account_number']) ; $i++) { 
   
             $exp =    $input_post['exp'][$i];
              $ac =   $input_post['account_number'][$i];
               $cvc =  $input_post['cvc'][$i];
               $final_ac = $ac.'-'.$cvc.'-'.$exp;

                $payment_detail = array(
                    "deal_id" => $input_post['payment_last_id'],
                    'ac' => $final_ac, 
                    'charge_amount' => $input_post['charge_amount'][$i], 
                );
                $this->log($input_post['payment_last_id'],'null',$payment_detail,'Payment Detail Add','comment');
                $message = $this->Payment_Option_Model->add($payment_detail);
            }

$this->log($input_post['payment_last_id'],'null',$item_payment_info,'Payment Add','comment');
 $message = $this->Payment_info_model->add($item_payment_info);
 // echo $this->db->last_query();
 // exit();
           // $this->db->trans_complete();

            if ($message) {
                $this->session->set_flashdata("msg", '<div class="alert alert-success">' .  'Payment Info Saved successfully!</div>');
                 $final_url =  $this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$input_post['payment_last_id'].'?step=notes';

                redirect($final_url);
            } else {
                $this->session->set_flashdata("msg", '<div class="alert alert-danger">Error occured! </div>');
            }


        }
 //        else if ($this->input->post("save_payment_info") !== NULL) {
 // $ac =  $input_post['ac_number'];
 //            $ex = $input_post['exp'];
 //           $cv =  $input_post['cvc'];
 //         $final_amount =   $ac.$ex.$cv;
 //           // $this->db->trans_start();
 //            $input_post =$this->input->post();
 //            // $item = array(
 //            //     "user_id" => $this->session->userdata("id"),
 //            //     "admin_id" => 0,
 //            //     "deal_status" => 0,
 //            //     "created_at" => date('Y-m-d H:i:s')

 //            // );
 //            // $message1 = $this->pModel->add($item);

 //           // $last_id = $this->db->insert_id();

          

 //  $item_payment_info = array(
 //                "deal_id" => $input_post['payment_last_id'],
 //                "admin_id" => $this->session->userdata('admin_id'),
 //                "payment_product" => $input_post['payment_product'],
 //                "payment_mode" => $input_post['mode_of_payment'],
 //                "package_price" => $input_post['package_price'],
 //                "special_instruction" => $input_post['special_instruction'],
 //                "billing_address" => $input_post['billing_address'],
 //                "transaction_id" => $input_post['transaction_id'],
 //                "descriptor" => $input_post['descriptor'],
 //                "dateandtime" => $input_post['dateandtime'],
 //            //    "payment_option" => $final_amount,
 //              //  "charge_amount" => $input_post['charge_amount'],
                
 //                "created_at" => date('Y-m-d H:i:s')


 //            );
 //   for ($i=0; $i < count($input_post['account_number']) ; $i++) { 
   
 //             $exp =    $input_post['exp'][$i];
 //              $ac =   $input_post['account_number'][$i];
 //               $cvc =  $input_post['cvc'][$i];
 //               $final_ac = $ac.'-'.$cvc.'-'.$exp;

 //                $payment_detail = array(
 //                    "deal_id" => $input_post['payment_last_id'],
 //                    'ac' => $final_ac, 
 //                    'charge_amount' => $input_post['charge_amount'][$i], 
 //                );
 //                $message = $this->Payment_Option_Model->add($payment_detail);
 //            }

 // $message = $this->Payment_info_model->add($item_payment_info);
 // // echo $this->db->last_query();
 // // exit();
 //           // $this->db->trans_complete();

 //            if ($message) {
 //                $this->session->set_flashdata("msg", '<div class="alert alert-success">' . $this->moduleName . ' Saved successfully!</div>');
 //                 $final_url =  $this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$input_post['payment_last_id'].'?step=notes';

 //                redirect($final_url);
 //            } else {
 //                $this->session->set_flashdata("msg", '<div class="alert alert-danger">Error occured! </div>');
 //            }


 //        }
         if ($this->input->post("draft_insert_notes") !== NULL) {

           // $this->db->trans_start();
            $input_post =$this->input->post();
            // $item = array(
            //     "user_id" => $this->session->userdata("id"),
            //     "admin_id" => 0,
            //     "deal_status" => 0,
            //     "created_at" => date('Y-m-d H:i:s')

            // );
            // $message1 = $this->pModel->add($item);

            //$last_id = $this->db->insert_id();

         

  $item_insert_notes = array(
                "deal_id" => $input_post['notes_last_id'],
                "admin_id" => $this->session->userdata('admin_id'),
                "agent" => $input_post['agent'],
                "manager" => $input_post['manager'],
                "verifier" => $input_post['verifier'],  
                "created_at" => date('Y-m-d H:i:s')
            );
$this->log($input_post['notes_last_id'],'null',$item_insert_notes,'Notes Add','comment');
 $message = $this->Insert_notes_model->add($item_insert_notes);
 // echo $this->db->last_query();
 // exit();
           // $this->db->trans_complete();

            if ($message) {
                $this->session->set_flashdata("msg", '<div class="alert alert-success">Deal Completely Saved successfully!</div>');
              $final_url =  $this->uri->segment(1).'/'.$this->uri->segment(2);
              // $final_url =  $this->uri->segment(1);

                redirect($final_url);
            } else {
                $this->session->set_flashdata("msg", '<div class="alert alert-danger">Error occured! </div>');
            }


        }
 //        else  if ($this->input->post("save_insert_notes") !== NULL) {

 //           // $this->db->trans_start();
 //            $input_post =$this->input->post();
 //            // $item = array(
 //            //     "user_id" => $this->session->userdata("id"),
 //            //     "admin_id" => 0,
 //            //     "deal_status" => 1,
 //            //     "created_at" => date('Y-m-d H:i:s')

 //            // );
 //            // $message1 = $this->pModel->add($item);

 //            //$last_id = $this->db->insert_id();

         

 //  $item_insert_notes = array(
 //                "deal_id" => $input_post['notes_last_id'],
 //                "admin_id" => $this->session->userdata('admin_id'),
 //                "agent" => $input_post['agent'],
 //                "manager" => $input_post['manager'],
 //                "verifier" => $input_post['verifier'],
              
                
 //                "created_at" => date('Y-m-d H:i:s')


 //            );

 // $message = $this->Insert_notes_model->add($item_insert_notes);
 // // echo $this->db->last_query();
 // // exit();
 //           // $this->db->trans_complete();

 //            if ($message) {
 //                $this->session->set_flashdata("msg", '<div class="alert alert-success">' . $this->moduleName . ' Saved successfully!</div>');
 //                $final_url =  $this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$input_post['notes_last_id'];

 //                redirect($final_url);
 //            } else {
 //                $this->session->set_flashdata("msg", '<div class="alert alert-danger">Error occured! </div>');
 //            }


 //        }

        $deal_id_loan = $this->uri->segment(3);
        $data['option_detail'] = $this->Deal_model->get_data_by_id($deal_id_loan);
        $data['region_val'] = $this->Region_model->getData();
        $data['pro_val'] = $this->Product_model->getDataAdd();
        $this->template($this->folder . "/add", $data);
    }

    // View & update form
    public function edit($id = 0)
    {
        $data = array(
            "heading" => $this->moduleName . " Managment",
            "title" => "Edit " . $this->moduleName,
            "description" => "Manage " . $this->moduleName . " from here !",
            "pmKey" => $this->pModel->tableId
        );

        //check if form is submitted
        if ($this->input->post("btn_submit_customer") !== NULL) {

            $customer_info_id = $this->input->post('customer_info_id');
              $customer_item = array(
                "first_name" => $this->input->post("first_name"),
                "middle_initial" => $this->input->post("middle_initial"),
                "last_name" => $this->input->post("last_name"),
                "street_address" => $this->input->post("street_address"),
                "zip_code" => $this->input->post("zip_code"),
                "state_province_code" => $this->input->post("state_province"),
                "country" => $this->input->post("country"),
                "home_phone" => $this->input->post("home_phone"),
                "work_phone" => $this->input->post("work_phone"),
                "mobile_phone" => $this->input->post("mobile_phone"),
                "email_address" => $this->input->post("email_address"),
                "website" => $this->input->post("website"),
                "date_of_birth" => $this->input->post("date_of_birth"),
                "ssn" => $this->input->post("ssn"),
                "mmn" => $this->input->post("mmn"),
               // "deal_status" => 1,
                "updated_at" => date('Y-m-d H:i:s')
            );
              // echo $id
              // exit

             $this->Customer_info_model->edit($customer_item, $customer_info_id);

                $this->session->set_flashdata("msg", '<div class="alert alert-success">' . ' Customer Updated successfully!</div>');
                $final_url =  $this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$this->input->post('deal_id').'?step=customer';
                 redirect($final_url);  

         }
         else if ($this->input->post("btn_submit_product") !== NULL){
             $pro_id = $this->input->post('deal_product_id');
                 $item = array(
                "product_name" => $this->input->post("product_name"),
                "product_description" => $this->input->post("product_description"),
               // "deal_status" => 1,
                //"updated_at" => date('H:i:s Y-m-d')
            );
              // echo $id
              // exit
             $this->Deal_product->edit($item, $pro_id);
             // echo $this->db->last_query();
             // exit();
               $this->session->set_flashdata("msg", '<div class="alert alert-success">' . 'Product Updated successfully!</div>');

                 $final_url =  $this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$this->input->post('deal_id').'?step=product';
                 redirect($final_url); 
                  
         }
         else if($this->input->post("btn_submit_loan") !== NULL){
            // echo $this->db->last_query();
 // exit(); $c1 =  $input_post['cc1'];
             $c1 = $this->input->post('cc1');
            $c2 = $this->input->post('cc2');
            $c3 = $this->input->post('cc3');
             $c4 = $this->input->post('cc4');
             $month = $this->input->post('month');
             $year = $this->input->post('year');
             $final_exp = $month.'-'.$year;
             $final_cc = $c1.'-'.$c2.'-'.$c3.'-'.$c4;


                 $loan_info_id = $this->input->post('loan_info_id');
              $loan_item = array(
                "select_loan_type" => $this->input->post("select_loan_type"),
                "lender_name" => $this->input->post("lender_name"),
                "contact" => $this->input->post("contact"),
                "start_date" => $this->input->post("start_date"),
                "end_date" => $this->input->post("end_date"),
                "account_number" => $this->input->post("account_number1"),
                "cc" =>  $final_cc,
                "exp_date" => $final_exp,
                "cvc" => $this->input->post("cvc1"),
                "apr" => $this->input->post("apr"),
                "total_owe" => $this->input->post("total_owe"),
                "available_balance" => $this->input->post("available_balance"),
                "min_payment" => $this->input->post("min_payment"),
                "last_payment" => $this->input->post("last_payment"),
                "next_payment" => $this->input->post("next_payment"),
                "annual_intrest_amount" => $this->input->post("annual_interest"),
                "paid_off_duration" => $this->input->post("paid_off_duration"),
               // "deal_status" => 1,
                "updated_at" => date('Y-m-d H:i:s')
            );
              // echo $id
              // exit
             $this->Loan_info->edit($loan_item, $loan_info_id);
               $this->session->set_flashdata("msg", '<div class="alert alert-success">' . 'Loan Info Updated successfully!</div>');

                 $final_url =  $this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$this->input->post('deal_id').'?step=loan';
                 redirect($final_url);
                
         }
             

 



else if($this->input->post("btn_submit_payment") !== NULL){
     $payment_info_id = $this->input->post('payment_info_id');



        $this->load->model('Payment_Option_Model');
      $payment_option = $this->Payment_Option_Model->view($payment_info_id);

      // echo '<pre>';
      // print_r( $payment_option);
      // echo '</pre>';
      // exit();




              $customer_item = array(
                "payment_product" => $this->input->post("payment_product"),
                "payment_mode" => $this->input->post("mode_of_payment"),
                "package_price" => $this->input->post("package_price"),
                "special_instruction" => $this->input->post("special_instruction"),
                "billing_address" => $this->input->post("billing_address"),
                "transaction_id" => $this->input->post("transaction_id"),
                "descriptor" => $this->input->post("descriptor"),
                "dateandtime" => $this->input->post("dateandtime"),
                
         
               // "deal_status" => 1,
                "updated_at" => date('Y-m-d H:i:s')
            );


              for ($i=0; $i < count($this->input->post('account_number')) ; $i++) { 
            $c1 = $this->input->post('account_number');
            $c2 = $this->input->post('exp');
            $c3 = $this->input->post('cvc');
           
             $final_cc = $c1.'-'.$c2.'-'.$c3;

                 $payment_detail = array(
                "ac" =>  $final_cc[$i],
                // "exp" => $this->input->post("exp")[$i],
                // "cvc" => $this->input->post("cvc")[$i],
                "charge_amount" => $this->input->post("charge_amount")[$i],
                 ); 
              }
              $this->db->where('payment_detail_id',$payment_info_id);
              $this->db->update('payment_detail',$payment_detail);
              // echo $this->db->last_query();
              // exit();
              // echo $id
              // exit

             $this->Payment_info_model->edit($customer_item, $payment_info_id);
               $this->session->set_flashdata("msg", '<div class="alert alert-success">' . 'Payment Updated successfully!</div>');

                $final_url =  $this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$this->input->post('deal_id').'?step=payment';
                 redirect($final_url); 
                   
}
else if($this->input->post("btn_submit_insert") !== NULL){
   $insert_notes_id = $this->input->post('insert_notes_id');
 $insert_notes = array(
 
                "agent" => $this->input->post("agent"),
                "manager" => $this->input->post("manager"),
                "verifier" => $this->input->post("verifier"),
               
                
         
               // "deal_status" => 1,
                "updated_at" => date('Y-m-d H:i:s')
            );

$this->Insert_notes_model->edit($insert_notes, $insert_notes_id);
  $this->session->set_flashdata("msg", '<div class="alert alert-success">' . 'Notes Updated successfully!</div>');

                 $final_url =  $this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$this->input->post('deal_id').'?step=notes';
                 redirect($final_url); 
                  
}else if ($this->input->post("btn_submit_final") !== NULL) {
   

    $deal_last_id = $this->input->post('deal_id');

    $forum_type_data = [
                    'deal_status' => '0',
                    'forum_type' => $this->input->post('forum_type'),
                    ];

    $this->pModel->edit($forum_type_data,$deal_last_id);
    echo "1";
    // echo $this->db->last_query();
    // exit;

    // $this->session->set_flashdata("msg", '<div class="alert alert-success">' . $this->moduleName . ' Saved successfully!</div>');

    //              redirect($this->uri->segment(1).'/draft');
}
                

      
            // }
          

        

        //Get data by id
        $data['items'] = $this->pModel->get_all_data_byid($id);
        $this->load->model('Payment_Option_Model');
     

        // echo '<pre>';
        // print_r($data['items']);

        // echo '</pre>';
        // exit();
        $data['product_val'] = $this->Product_model->getData($data['items'][0]['product_name']);
        $data['region_val'] = $this->Region_model->getData();
        // $this->Payment_Option_Model->view($data['items'][0]['deal_id']);
        $data['comments'] = $this->Deal_model->get_comments_by_id($data['items'][0]['deal_id']);
         $data['option_detail'] = $this->Deal_model->get_data_by_id($data['items'][0]['deal_id']);
      // echo   $this->db->last_query();
      // exit();
        $this->template($this->folder . "/edit", $data);
    }

    public function invoice($id = 0)
    {
        $data = array(
            "heading" => 'INVOICE',
            "title" => "View invoice",
            //"description" => "e-Sign",
            "pmKey" => $this->pModel->tableId
        );

        //Get data by id
        $data['items'] = $this->pModel->get_all_data_byid($id);
        $this->load->model('Payment_Option_Model');
        $data['product_val'] = $this->Product_model->getData($data['items'][0]['product_name']);
        $data['region_val'] = $this->Region_model->getData();
        $data['comments'] = $this->Deal_model->get_comments_by_id($data['items'][0]['deal_id']);
        $data['option_detail'] = $this->Deal_model->get_data_by_id($data['items'][0]['deal_id']);
        $this->load->view($this->folder . "/invoice", $data);
        //$this->template($this->folder . "/invoice", $data);
    }

    public function invoice_send($id = 0)
    {
        //Get data by id
        $items = $this->pModel->get_all_data_byid($id);
        $this->load->model('Payment_Option_Model');
        $product_val = $this->Product_model->getData($data['items'][0]['product_name']);
        $region_val = $this->Region_model->getData();
        $comments = $this->Deal_model->get_comments_by_id($data['items'][0]['deal_id']);
        $option_detail = $this->Deal_model->get_data_by_id($data['items'][0]['deal_id']);


        $system_name = 'CRM';
        $site_footer = '2019 © CRM. Developed by Sky Tech Sol';
        $from_email = 'info@skytechsol.com';
        $to_email = $items[0]['email_address'];
        $complete_name = 'Arman Sheikh';
        //$this->pModel->get_relation_pax('');
        $details = $this->pModel->getProductByMinStock();
       
        $mypayment = "";

        if($items[0]['payment_mode'] == '1')  { $mypayment .= 'Cod';}
                        if($items[0]['payment_mode'] == '2')  {$mypayment .=  'CC';}
                        if($items[0]['payment_mode'] == '3')  {$mypayment .=  'Package Price';}
                        if($items[0]['payment_mode'] == '4')  {$mypayment .=  'Online';}
                        if($items[0]['payment_mode'] == '5')  {$mypayment .=  'Check';}



        //User Email Send
        $msg = '
<html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    
    
    <title>Invoice</title>
    
   
    
    <style type="text/css">
/*
     CSS-Tricks Example
     by Chris Coyier
     http://css-tricks.com
*/

* { margin: 0; padding: 0; }
body { font: 14px/1.4 Georgia, serif; }
#page-wrap { width: 800px; margin: 0 auto; }

textarea { border: 0; font: 14px Georgia, Serif; overflow: hidden; resize: none; }
table { border-collapse: collapse; }
table td, table th { border: 1px solid black; padding: 5px; }

#header {     height: 15px;
    width: 100%;
    margin: 1px 0; background: #222; text-align: center; color: white; font: bold 15px Helvetica, Sans-Serif; text-decoration: uppercase; letter-spacing: 20px; padding: 8px 0px; }

#address { width: 250px; height: 95px; float: left; }
#customer { overflow: hidden; }

.logo { text-align: right; float: right; position: relative; margin-top: 5px; border: 1px solid #fff; max-width: 540px; max-height: 100px; overflow: hidden; }
#logo:hover, #logo.edit { border: 1px solid #000; margin-top: 0px; max-height: 125px; }
#logoctr { display: none; }
#logo:hover #logoctr, #logo.edit #logoctr { display: block; text-align: right; line-height: 25px; background: #eee; padding: 0 5px; }
#logohelp { text-align: left; display: none; font-style: italic; padding: 10px 5px;}
#logohelp input { margin-bottom: 5px; }
.edit #logohelp { display: block; }
.edit #save-logo, .edit #cancel-logo { display: inline; }
.edit #image, #save-logo, #cancel-logo, .edit #change-logo, .edit #delete-logo { display: none; }
#customer-title { font-size: 20px; font-weight: bold; float: left; }

#meta { margin-top: 1px; width: 300px; float: right; }
#meta td { text-align: right;  }
#meta td.meta-head { text-align: left; background: #eee; }
#meta td textarea { width: 100%; height: 20px; text-align: right; }

#items { width: 100%; margin: 5px 0 0 0; border: 1px solid black; }
#items th { background: #eee; }
#items textarea { width: 80px; height: 40px; }
#items tr.item-row td { border: 0; vertical-align: top; }
#items td.description { width: 300px; }
#items td.item-name { width: 175px; }
#items td.description textarea, #items td.item-name textarea { width: 100%; }
#items td.total-line { border-right: 0; text-align: right; }
#items td.total-value { border-left: 0; padding: 10px; }
#items td.total-value textarea { height: 20px; background: none; }
#items td.balance { background: #eee; }
#items td.blank { border: 0; }

#terms { text-align: center; margin: 3px 0 0 0; }
#terms h5 { text-transform: uppercase; font: 13px Helvetica, Sans-Serif; letter-spacing: 10px; border-bottom: 1px solid black; padding: 0 0 8px 0; margin: 0 0 8px 0; }
#terms textarea { width: 100%; text-align: center;}

textarea:hover, textarea:focus, #items td.total-value textarea:hover, #items td.total-value textarea:focus, .delete:hover { /* background-color:#EEFF88; */ }

.delete-wpr { position: relative; }
.delete { display: block; color: #000; text-decoration: none; position: absolute; background: #EEEEEE; font-weight: bold; padding: 0px 3px; border: 1px solid; top: -6px; left: -22px; font-family: Verdana; font-size: 12px; }

    </style>

</head>

<body>

<div style="margin:10px 15px 0px 15px;">

                    <div style="clear:both;"></div>
</div>

    <div id="page-wrap">

        <textarea id="header" style="display:none;">INVOICE</textarea>
        
        <div id="identity" style="width: 100%;height: 100px !important;">
    
<div style="clear:both"></div>  
        
        </div>
        
        
        
        <div id="customer">

            <!--<textarea id="customer-title">Widget Corp.
c/o Steve Widget</textarea>-->
<div id="printDiv">
<table id="meta" style="float:left;">
                <tbody>
                <!-- <tr style="">
                    <td style="text-align: center;" colspan="2">SHIPPER</td>
                </tr> -->
                <tr>
                    <td class="meta-head">First Name</td>
                    <td align="left"><div style="text-align: left;" class="due">'.$items[0]['first_name'].'</div></td>
                </tr>
                <tr>

                    <td class="meta-head">Middle Initial</td>
                    <td align="left"><div style="text-align: left;" class="due">'.$items[0]['middle_initial'].'</div></td>
                </tr>
                <tr>
                    <td class="meta-head">Last Name</td>
                    <td align="left"><div style="text-align: left;" class="due">'.$items[0]['last_name'].'</div></td>
                </tr>
                <tr>
                    <td class="meta-head">Street Address</td>
                    <td align="left"><div style="text-align: left;" class="due">'.$items[0]['street_address'].'</div></td>
                </tr>
                <tr>
                    <td class="meta-head">Zip Code</td>
                    <td align="left"><div style="text-align: left;" class="due">'.$items[0]['zip_code'].'</div></td>
                </tr>
                <tr>
                    <td class="meta-head">State Provice Code</td>
                    <td align="left"><div style="text-align: left;" class="due">'.$items[0]['state_province_code'].'</div></td>
                </tr>
                <tr>
                    <td class="meta-head">Country</td>
                    <td align="left"><div style="text-align: left;" class="due">'.$items[0]['country'].'</div></td>
                </tr>
                <tr>
                    <td class="meta-head">Home Phone</td>
                    <td align="left"><div style="text-align: left;" class="due">'.$items[0]['home_phone'].'</div></td>
                </tr>
                <tr>
                    <td class="meta-head">Work Phone</td>
                    <td align="left"><div style="text-align: left;" class="due">'.$items[0]['work_phone'].'</div></td>
                </tr>
                <tr>
                    <td class="meta-head">Mobile Phone</td>
                    <td align="left"><div style="text-align: left;" class="due">'.$items[0]['mobile_phone'].'</div></td>
                </tr>
                <tr>
                    <td class="meta-head">Email Address</td>
                    <td align="left"><div style="text-align: left;" class="due">'.$items[0]['email_address'].'</div></td>
                </tr>
            </tbody></table>
            <table id="meta">
                <tbody>
                <!-- <tr>
                    <td style="text-align: center;" colspan="2">CONSIGNEE</td>
                </tr> -->
                
                <tr>
                    <td class="meta-head">Product</td>
                    <td colspan="3" align="left"><div style="text-align: left;" class="due">'.$items[0]['payment_product'].'</div></td>
                </tr>
                <tr>
                    <td class="meta-head">Mode Of Payment</td>
                    <td colspan="3" align="left"><div style="text-align: left;" class="due">
                        '.$mypayment.'
                    </div></td>
                </tr>
                <tr>
                    <td class="meta-head">Package Price</td>
                    <td colspan="3" align="left"><div style="text-align: left;" class="due">'.$items[0]['package_price'].'</div></td>
                </tr>
                <tr>
                    <td class="meta-head">Special Instruction</td>
                    <td colspan="3" align="left"><div style="text-align: left;" class="due">'.$items[0]['special_instruction'].'</div></td>
                </tr>
                <tr>
                    <td class="meta-head">Billing Address</td>
                    <td colspan="3" align="left"><div style="text-align: left;" class="due">'.$items[0]['billing_address'].'</div></td>
                </tr>
                <tr>
                    <td class="meta-head">Amount Total</td>
                    <td colspan="3" align="left"><div style="text-align: left;" class="due">'.$total_charged_amount.'</div></td>
                </tr>';


                

                  if(!empty($option_detail)){
                  foreach ($option_detail as $optionvalue) {
                    $total_charged_amount += $optionvalue['annual_intrest_amount'];
                

                
                $msg  .='<tr>
                    <td class="meta-head">Payment Option</td>
                    <td align="left"><div style="text-align: left;" class="due">'.$optionvalue['account_number'].'</div></td>
                    <td align="left"><div style="text-align: left;" class="due">'.$optionvalue['exp_date'].'</div></td>
                    <td align="left"><div style="text-align: left;" class="due">'.$optionvalue['cvc'].'</div></td>
                </tr>
                
                <tr>
                    <td class="meta-head">Charge Amount</td>
                    <td colspan="3" align="left"><div style="text-align: left;" class="due">'.$optionvalue['annual_intrest_amount'].'</div></td>
                </tr>';
                       
                                        }
                 }
                

               $msg  .= '<tr>
                    <td class="meta-head">Transaction ID</td>
                    <td colspan="3" align="left"><div style="text-align: left;" class="due">'.$items[0]['transaction_id'].'</div></td>
                </tr>
                <tr>
                    <td class="meta-head">Descriptor</td>
                    <td colspan="3" align="left"><div style="text-align: left;" class="due">'.$items[0]['descriptor'].'</div></td>
                </tr>
                <tr>
                    <td class="meta-head">Date And Time</td>
                    <td colspan="3" align="left"><div style="text-align: left;" class="due">'.$items[0]['dateandtime'].'</div></td>
                </tr>

            </tbody></table>
        <div style="clear:both"></div>  
        <div class="mt-90 row">
            <div class="mt-90 col-md-12">
                 <p>I authorize the amount of $_____________ to the credit card provided herein. I agree to pay for this service/product in accordance with the issuing bank </p>
            </div>
        </div>

         <div class="mt-90 row">
            <div class="mt-50 col-md-12">
                <div class="col-md-1">
                 <input type="checkbox" name="">
                </div>
                <div class="col-md-11 text-left">
                    <p>By signing this form I agree with the terms & conditions </p>
                </div>
            </div>
        </div>

        <div class="mt-90 row">
            <div class="mt-50 col-md-12">
                <div class="col-md-6">
                    <div class="row">
                        <label style="margin-top: 10px" class="control-label mb-10" for="exampleInputuname_1">Fullname&nbsp;: ______________________________</label>
                    </div>
                    <div class="row">
                        <label style="margin-top: 10px" class="control-label mb-10" for="exampleInputuname_1">Signature&nbsp;: ______________________________</label>
                    </div>
                    <div class="row">
                        <label style="margin-top: 10px" class="control-label mb-10" for="exampleInputuname_1">Date&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: ______________________________</label>
                    </div>
                </div>
                <div class="col-md-6"></div>
            </div>
        </div>
        </div>  
        </div>
    </div>
</body></html>';


        $config['charset'] = 'utf-8';
        $config['wordwrap'] = TRUE;
        $config['mailtype'] = 'html';
        $this->email->initialize($config);
        $this->email->from($from_email, $system_name);
        $this->email->to($to_email);
        $this->email->subject('Invoice');
        $this->email->message($msg);
        $this->email->send();
        redirect($this->uri->segment(1));

    
    }

    public function view_deal($id = 0)
    {
        $data = array(
            "heading" => $this->moduleName . " Managment",
            "title" => "View " . $this->moduleName,
            "description" => "Manage " . $this->moduleName . " from here !",
            "pmKey" => $this->pModel->tableId
        );

        //Get data by id
        $data['items'] = $this->pModel->get_all_data_byid($id);

        $this->load->model('Payment_Option_Model');
     

        // echo '<pre>';
        // print_r($data['items']);

        // echo '</pre>';
        // exit();
         $data['product_val'] = $this->Product_model->getData($data['items'][0]['product_name']);
        $data['region_val'] = $this->Region_model->getData();
        // $this->Payment_Option_Model->view($data['items'][0]['deal_id']);
        // new
        $data['comments'] = $this->Deal_model->get_comments_by_id($data['items'][0]['deal_id']);
         $data['option_detail'] = $this->Deal_model->get_data_by_id($data['items'][0]['deal_id']);
      // echo   $this->db->last_query();
      // exit();
        $this->template($this->folder . "/view_deal", $data);
    }



    // Delete  Record
    public function delete($id = 0)
    {
        $this->pModel->delete($id);
        $this->session->set_flashdata("msg", '<div class="alert alert-success">' . $this->moduleName . ' deleted successfully!</div>');
        redirect($this->uri->segment(1));
    }

    public function forward()
    {
        if ($this->input->post('assigned_user_id') > 0) {
            $assigned_user_id = NULL;
        }else
        {
            $assigned_user_id = NULL;
        }
    $deal_last_id = $this->input->post('deal_id');
    $forum_type_data = [
                    'deal_status' => '0',
                    'forum_type' => $this->input->post('forum_type'),
                    'assigned_user_id' => $assigned_user_id
                    ];
    $this->pModel->edit($forum_type_data,$deal_last_id);
    echo "1";
    $this->log($deal_last_id,$forum_type_data,'null','Forward/Revert','comment');
    }

    public function pick_deal()
    {
        $id = $this->input->post('deal_id');
        $login_user_id = $this->input->post('user_id');
        // $logs_data = [
        //               'deal_id' => $id,
        //               'user_id' => $login_user_id,
        //               'date' => date('Y-m-d H:i:s'),
        //               'status' => 'pick',
        //               'comment' => $this->input->post('comment'),
        //               'ip_address' => $this->input->ip_address(),
        //             ];
        // $this->db->insert('deal_logs',$logs_data);

        $c_user_id = $this->session->userdata('id');
        $data = [
            'assigned_user_id' => $c_user_id,
        ];   
        $this->log($id,$data,'null','Pick',$this->input->post('comment'));
        $this->pModel->edit($data,$id);
        echo "1";
        // echo $this->db->last_query();
        // exit;

        // $this->session->set_flashdata("msg", '<div class="alert alert-success">Deal picked successfully!</div>');
        // redirect($this->uri->segment(1));
    }

    public function leave_deal()
    {
        $id = $this->input->post('deal_id');
        $login_user_id = $this->input->post('user_id');
        // $logs_data = [
        //               'deal_id' => $id,
        //               'user_id' => $login_user_id,
        //               'date' => date('Y-m-d H:i:s'),
        //               'status' => 'leave',
        //               'comment' => $this->input->post('comment'),
        //               'ip_address' => $this->input->ip_address(),
        //             ];
        // $this->db->insert('deal_logs',$logs_data);

        $data = [
            'assigned_user_id' => NULL,
        ];   
        $this->pModel->edit($data,$id);
        $this->log($id,$data,'null','Leave',$this->input->post('comment'));
        echo "1";

        // // echo $this->db->last_query();
        // // exit;

        // $this->session->set_flashdata("msg", '<div class="alert alert-success">Deal leave successfully!</div>');
        // redirect($this->uri->segment(1));
    }

    public function draft(){

        $data = array(
            "heading" => $this->moduleName . " Managment",
            "title" => $this->moduleName . " Managment",
            "description" => "Manage " . $this->moduleName . " from here !",
            "pmKey" => $this->pModel->tableId
        );

        //Set columns for grid
        $data['cols'] = array(
            "deal_id" => "Deal ID",
            "created_at" => "Created at",
        );

           $data['items'] = $this->pModel->get_all_data();
           $this->template($this->folder."/listing",$data);


    }

        public function saved(){

        $data = array(
            "heading" => "Saved Deals",
            "title" => "Saved Deals Managment",
            "description" => "Manage " . $this->moduleName . " from here !",
            "pmKey" => $this->pModel->tableId
        );

        //Set columns for grid
        $data['cols'] = array(
            "deal_id" => "Deal ID",
            "created_at" => "Created at",
        );

           $c_user_id = $this->session->userdata('id');
           $status = '1';
           $data['items'] = $this->pModel->get_all_data_by_userid($c_user_id,$status);
           $this->template($this->folder."/saved",$data);


    }

        public function sent(){

        $data = array(
            "heading" => "Forward Deals",
            "title" => "Forward Deals Managment",
            "description" => "Manage " . $this->moduleName . " from here !",
            "pmKey" => $this->pModel->tableId
        );

        //Set columns for grid
        $data['cols'] = array(
            "deal_id" => "Deal ID",
            "created_at" => "Created at",
        );

           $c_user_id = $this->session->userdata('id');
           $status = '0';
           $data['items'] = $this->pModel->get_all_data_by_userid_sent($c_user_id,$status);
           $this->template($this->folder."/sent",$data);


    }

    public function  view_detail($id = 0)
    {
        $data = array(
            "heading" => $this->moduleName . " Managment",
            "title" => $this->moduleName . " Managment",
            "description" => "Manage " . $this->moduleName . " from here !",
            "pmKey" => $this->pModel->tableId
        );

        //Set columns for grid
        $data['cols'] = array(
            "deal_id" => "Deal ID",
            "created_at" => "Created at",
        );

        //fetch data from database
        $c_user_id = $this->session->userdata('id');
        $status = '0';
        $data['items'] = $this->pModel->get_all_data_by_userid($c_user_id,$status);
        $this->template($this->folder . "/view_detail", $data);
    }

        public function picked(){

        $data = array(
            "heading" => "Picked Deals",
            "title" => "Picked Deals Managment",
            "description" => "Manage " . $this->moduleName . " from here !",
            "pmKey" => $this->pModel->tableId
        );

        //Set columns for grid
        $data['cols'] = array(
            "deal_id" => "Deal ID",
            "created_at" => "Created at",
        );

        // $forum_type = $this->session->userdata('role_id');

        // if ($forum_type == 2) {
        //     $type = 'agent';
        // }elseif ($forum_type == 3) {
        //     $type = 'manager';
        // }elseif ($forum_type == 4) {
        //     $type = 'verifier';
        // }

       $c_user_id = $this->session->userdata('id');
       $data['items'] = $this->pModel->get_all_data_by_assignuserid($c_user_id);
       $this->template($this->folder."/picked",$data);


    }

        public function agent(){

        $data = array(
            "heading" => "Agent Forum",
            "title" => $this->moduleName . " Managment",
            "description" => "Manage " . $this->moduleName . " from here !",
            "pmKey" => $this->pModel->tableId
        );

        //Set columns for grid
        $data['cols'] = array(
            "deal_id" => "Deal ID",
            "created_at" => "Created at",
        );

           $type = 'agent';
           $data['items'] = $this->pModel->get_all_data($type);
           $this->template($this->folder."/listing",$data);


    }

      public function verifier(){

        $data = array(
            "heading" => "Verifier Forum",
            "title" => $this->moduleName . " Managment",
            "description" => "Manage " . $this->moduleName . " from here !",
            "pmKey" => $this->pModel->tableId
        );

        //Set columns for grid
        $data['cols'] = array(
            "deal_id" => "Deal ID",
            "created_at" => "Created at",
        );

           $type = 'verifier';
           $data['items'] = $this->pModel->get_all_data($type);
           $this->template($this->folder."/listing",$data);


    }

      public function manager(){

        $data = array(
            "heading" => "Manager Forum",
            "title" => $this->moduleName . " Managment",
            "description" => "Manage " . $this->moduleName . " from here !",
            "pmKey" => $this->pModel->tableId
        );

        //Set columns for grid
        $data['cols'] = array(
            "deal_id" => "Deal ID",
            "created_at" => "Created at",
        );

           $type = 'manager';
           $data['items'] = $this->pModel->get_all_data($type);
           $this->template($this->folder."/listing",$data);
    }

    public function complete(){

        $data = array(
            "heading" => "Complete Deals Forum",
            "title" => $this->moduleName . " Managment",
            "description" => "Manage " . $this->moduleName . " from here !",
            "pmKey" => $this->pModel->tableId
        );

        //Set columns for grid
        $data['cols'] = array(
            "deal_id" => "Deal ID",
            "created_at" => "Created at",
        );

           $type = 'complete';
           $data['items'] = $this->pModel->get_all_data($type);
           $this->template($this->folder."/listing",$data);
    }

     public function check_user()
    {
        $this->load->model('Users_model');
        $this->db->where('username',$this->input->post('user_name'));
        $this->db->where('password',$this->Users_model->hash($this->input->post('password')));
        $this->db->from('users');
        $user = $this->db->get()->result();
        if (count ($user) > 0) {
            echo $user[0]->user_id;
        }
        else 
        {
            echo '0';
        }  
    }

}