<?php

/**
 * Created by PhpStorm.
 * User: development
 * Date: 1/1/2019
 * Time: 5:28 PM
 */
class Gl_accounts extends Global_Controller
{
    //Contains Primary model object
    private $pModel = "";

    //declare reusable variables
    private $folder = "gl_accounts";
    private $moduleName = "Account Control";

    function __construct()
    {
        parent::__construct();
        //Load models here;
        $this->load->model("Gl_accounts_model");
        $this->pModel = $this->Gl_accounts_model;
    }

    // Show listing
    public function index()
    {
        $data = array(
            "heading" => $this->moduleName . " Managment",
            "title" => $this->moduleName . " Managment",
            "description" => "Manage " . $this->moduleName . " from here !",
            "pmKey" => $this->pModel->tableId
        );

        //Set columns for grid
        $data['cols'] = array(
            "gl_account_name" => "Account Name",
            "gl_account_code" => "Account Code",
            "account_type_name" => "Account Type",
        );

        //fetch data from database
        $data['items'] = $this->pModel->getData();

        $this->template($this->folder . "/listing", $data);
    }

    // Add & Submit form
    public function add()
    {
        $this->load->model("Account_types_model");

        $data = array(
            "heading" => $this->moduleName . " Managment",
            "title" => $this->moduleName,
            "description" => "Manage " . $this->moduleName . "  from here !"
        );

        //check if form is submitted
        if ($this->input->post("btn_submit") !== NULL) {


            $item = array(
                "gl_account_name" => $this->input->post("account_name"),
                "gl_account_code" => $this->input->post("account_code"),
                "parent_gl_account_id" => $this->input->post("parent_gl_account_id"),
                "account_type_id" => $this->input->post("account_type"),
                "created_at" => date('H:i:s Y-m-d'),
            );
            $message = $this->pModel->add($item);
            if ($message) {
                $this->session->set_flashdata("msg", '<div class="alert alert-success">' . $this->moduleName . ' Saved successfully!</div>');
            } else {
                $this->session->set_flashdata("msg", '<div class="alert alert-danger">Error occured! </div>');
            }


        }
        $data['account_type_list'] = $this->Account_types_model->getData();
        $data['items'] = $this->pModel->getData();
        //getData_account_type
        $this->template($this->folder . "/add", $data);
    }

    // View & update form
    public function edit($id = 0)
    {
        $this->load->model("Account_types_model");
        $data = array(
            "heading" => $this->moduleName . " Managment",
            "title" => "Edit " . $this->moduleName,
            "description" => "Manage " . $this->moduleName . " from here !",
            "pmKey" => $this->pModel->tableId
        );

        //check if form is submitted
        if ($this->input->post("btn_submit") !== NULL) {

            //Edit Account List
            $item = array(
                "gl_account_name" => $this->input->post("account_name"),
                "gl_account_code" => $this->input->post("account_code"),
                "parent_gl_account_id" => $this->input->post("parent_gl_account_id"),
                "account_type_id" => $this->input->post("account_type"),
                "updated_at" => date('H:i:s Y-m-d'),
            );


            $this->pModel->edit($item, $id);

            $this->session->set_flashdata("msg", '<div class="alert alert-success">' . $this->moduleName . ' Saved successfully!</div>');

        }

        //Get data by id
        $data['account_type_list'] = $this->Account_types_model->getData();
        $data['item'] = $this->pModel->view($id);
        $data['items'] = $this->pModel->getData();
        $this->template($this->folder . "/edit", $data);
    }


    // Delete  Record
    public function delete($id = 0)
    {
        $this->pModel->delete($id);
        $this->session->set_flashdata("msg", '<div class="alert alert-success">' . $this->moduleName . ' deleted successfully!</div>');
        redirect($this->uri->segment(1));
    }
}