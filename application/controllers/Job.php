<?php

/**
 * Created by PhpStorm.
 * User: development
 * Date: 1/4/2019
 * Time: 10:11 AM
 */
class Job extends Global_Controller
{
    //Contains Primary model object
    private $pModel = "";

    //declare reusable variables
    private $folder = "job";
    private $moduleName = "Job Order";

    function __construct()
    {
        parent::__construct();
        //Load models here
        $this->load->model("Job_model");
        $this->pModel = $this->Job_model;
    }


    // Show listing
    public function index()
    {

        $data = array(
            "heading" => $this->moduleName . " Managment",
            "title" => $this->moduleName . " Managment",
            "description" => "Manage " . $this->moduleName . " from here !",
            "pmKey" => $this->pModel->tableId
        );

        //Set columns for grid
        $data['cols'] = array(
            "job_ref" => "Job Ref#",
            "date" => "Date",
            "remarks" => "Remarks",
            "us_pkr" => "US$ PKR",
            "held_on" => "Held On",
            "status" => "Status",
            "view_detail" => "View Detail",
        );

        //fetch data from database
        $data['items'] = $this->pModel->getData();

        $this->template($this->folder . "/listing", $data);
    }

    //Create Invoice Job Order
    public function invoice($id = 0)
    {

        $job_order_id = $id;
        $post = $this->input->post();
        // Load Suppliers models here
        $this->load->model("Job_model");
        $this->load->model("Products_model");
        $this->load->model("Customers_model");



        //check if form is submitted
        if ($this->input->post("btn_submit") !== NULL) {


            /*echo "<pre>";
            print_r($post);
            exit;*/

            $this->db->trans_start(); // Query will be rolled back

            $item1 = array(
                "is_sales_invoice" => 1,
            );

            $this->db->where('job_id', $job_order_id);
            $message = $this->db->update('job_order', $item1);

            $item = array(
                "inv_no" => $this->input->post("inv_no"),
                "inv_date" => $this->input->post("inv_date"),
                "customer_id" => $this->input->post("customer"),
                "job_order_id" => $job_order_id,
                "note_print_on_invoice" => $this->input->post("note_print"),
                "remarks" => $this->input->post("remarks"),
                "invoice_made_by_user_id" => $this->session->userdata('id'),
                "tax" => $this->input->post("tax"),
                "discount" => $this->input->post("discount"),
                "sub_total" => $this->input->post("total_amount"),
                "created_at" => date('Y-m-d H:i:s'),
            );
            /*echo "<pre>";
            print_r($item);
            exit;*/
            $message = $this->db->insert('sales_invoices', $item);
            $sales_invoice_id = $this->db->insert_id();
            //echo $purchase_order_id;

            if (!empty($post['product_name_new'])) {
                for ($i = 0; $i < count($post['product_id_new']); $i++) {
                    $product_id_new = $post['product_id_new'][$i];
                    $desc_new = $post['desc_new'][$i];
                    $unit_price_new = $post['unit_price_new'][$i];
                    $qty_new = $post['qty_new'][$i];
                    $amount_new = $post['amount_new'][$i];

                    $data = array(
                        'product_id' => $product_id_new,
                        'product_description' => $desc_new,
                        'product_quantity' => $qty_new,
                        'product_unit_sell_price' => $unit_price_new,
                        'line_item_total' => $amount_new,
                        'sales_invoice_id' => $sales_invoice_id,
                        'created_at' => date('Y-m-d H:i:s'),
                    );

                    $message = $this->db->insert('sales_invoice_items', $data);

                }
            }

            $this->db->trans_complete();
            //exit;
            if ($message) {
                $this->session->set_flashdata("msg", '<div class="alert alert-success">Invoice  successfully Created!</div>');
            } else {
                $this->session->set_flashdata("msg", '<div class="alert alert-danger">Error occured! </div>');
            }


        }
        $data = array(
            "heading" => $this->moduleName . " Managment",
            "title" => $this->moduleName,
            "description" => "Manage " . $this->moduleName . "  from here !"
        );
        $data['customer_list'] = $this->Customers_model->getData();
        $data['job_list'] = $this->Job_model->getData();
        $data['product_list'] = $this->Products_model->getData();
        $this->template($this->folder . "/invoice", $data);

    }

    //Invoice Listing
    public function invoice_listing()
    {
        $data = array(
            "heading" => "Invoice Managment",
            "title" => "Invoice Managment",
            "description" => "Manage Invoice from here !",
            "pmKey" => $this->pModel->tableId
        );

        //Set columns for grid
        $data['cols'] = array(
            "title" => "Customer",
            "inv_no" => "Inv No",
            "inv_date" => "Invoice Date",
            "view_detail" => "View Detail",
        );

        //fetch data from database
        $data['items'] = $this->pModel->getInvoice();
        $this->template($this->folder . "/invoice_listing", $data);

    }

    //Create Payment Invoice
    public function payment_invoice($sales_invoice_id = 0)
    {

        $data = array(
            "heading" => "Payment Managment",
            "title" => "Payment Managment",
            "description" => "Manage Payment from here !",
            "pmKey" => $this->pModel->tableId
        );
        //check if form is submitted
        if ($this->input->post("btn_submit") !== NULL) {

            $item = array(
                "payment_date" => $this->input->post("payment_date"),
                "payment_entered_by_user_id" => $this->session->userdata('id'),
                "payment_amount" => $this->input->post("payment_amount"),
                "sales_invoice_id" => $this->input->post("sales_invoice_id"),
                "payment_method" => $this->input->post("payment_method"),
                "note" => $this->input->post("notes"),
                "created_at" => date('H:i:s Y-m-d'),
            );

            /*echo "<pre>";
            print_r($item);
            exit;*/

            $message = $this->db->insert('sales_invoice_payments', $item);
            if ($message) {
                $this->session->set_flashdata("msg", '<div class="alert alert-success">Saved successfully!</div>');
            } else {
                $this->session->set_flashdata("msg", '<div class="alert alert-danger">Error occured! </div>');
            }

        }

        $data['payment_ivoice_list'] = $this->pModel->get_payment_invoice($sales_invoice_id);
        $data['items'] = $this->pModel->getInvoice($sales_invoice_id);

        $this->template($this->folder . "/payment_invoice", $data);
    }

    //Sales Invoice Detail
    public function sales_invoice_detail()
    {
        $this->load->model("Customers_model");

        $sales_invoice_id = $this->uri->segment(3);


        $data = array(
            "heading" => "Invoice Managment",
            "title" => "Invoice Managment",
            "description" => "Manage Invoice from here !",
            "pmKey" => $this->pModel->tableId
        );

        //Set columns for grid
        $data['cols'] = array(
            "product_name" => "Product Name",
            "product_description" => "Product Description",
            "product_quantity" => "Sales Quantity",
            "product_unit_sell_price" => "Unit Price",
            "line_item_total" => "Amount",
            //"path" => "File"
        );
        $data['items'] = $this->pModel->get_sales_item($sales_invoice_id);
        $data['item_new'] = $this->pModel->getInvoice_customer($sales_invoice_id);

        $this->template($this->folder . "/item_listing", $data);
    }

    //Job Order Detail
    public function  view_detail($id = 0)
    {
        $data = array(
            "heading" => $this->moduleName . " Managment",
            "title" => $this->moduleName . " Managment",
            "description" => "Manage " . $this->moduleName . " from here !",
            "pmKey" => $this->pModel->tableId
        );

        //Set columns for grid


        $data['cols'] = array(
            "job_ref" => "Job Ref#",
            "date" => "Date",
            "description" => "Remarks",
            "short_description" => "US$ PKR",
            "note_invoice" => "Held On",
            "remarks" => "Remarks",
            "bare" => "Bare",
            "shell" => "Shell",
            "us_pkr" => "US$ PKR",
            "held_on" => "Held On",
            "status" => "Status",
        );

        //fetch data from database
        $data['items'] = $this->pModel->getData($id);
        $this->template($this->folder . "/view_detail", $data);
    }

    // Add & Submit form
    public function add()
    {
        $data = array(
            "heading" => $this->moduleName . " Managment",
            "title" => $this->moduleName,
            "description" => "Manage " . $this->moduleName . "  from here !"
        );

        //check if form is submitted
        if ($this->input->post("btn_submit") !== NULL) {

            /* echo "<pre>";
             print_r($this->input->post());
             exit;*/

            $item = array(
                "job_ref" => $this->input->post("job_ref"),
                "date" => $this->input->post("date"),
                "description" => $this->input->post("description"),
                "short_description" => $this->input->post("short_description"),
                "note_invoice" => $this->input->post("note_invoice"),
                "remarks" => $this->input->post("remarks"),
                "bare" => $this->input->post("bare"),
                "shell" => $this->input->post("shell"),
                "us_pkr" => $this->input->post("us_pkr"),
                "held_on" => $this->input->post("held_on"),
                "status" => $this->input->post("status"),
                "created_at" => date('H:i:s Y-m-d'),
            );
            $message = $this->pModel->add($item);
            if ($message) {
                $this->session->set_flashdata("msg", '<div class="alert alert-success">' . $this->moduleName . ' Saved successfully!</div>');
            } else {
                $this->session->set_flashdata("msg", '<div class="alert alert-danger">Error occured! </div>');
            }


        }
        $this->template($this->folder . "/add", $data);
    }

    // View & update form
    public function edit($id = 0)
    {
        $data = array(
            "title" => "Edit " . $this->moduleName,
            "description" => "Manage " . $this->moduleName . " from here !",
            "pmKey" => $this->pModel->tableId
        );

        //check if form is submitted
        if ($this->input->post("btn_submit") !== NULL) {

            //if file selected else only update without file
            $item = array(
                "job_ref" => $this->input->post("job_ref"),
                "date" => $this->input->post("date"),
                "description" => $this->input->post("description"),
                "short_description" => $this->input->post("short_description"),
                "note_invoice" => $this->input->post("note_invoice"),
                "remarks" => $this->input->post("remarks"),
                "bare" => $this->input->post("bare"),
                "shell" => $this->input->post("shell"),
                "us_pkr" => $this->input->post("us_pkr"),
                "held_on" => $this->input->post("held_on"),
                "status" => $this->input->post("status"),
                "updated_at" => date('H:i:s Y-m-d'),
            );


            $this->pModel->edit($item, $id);
            $this->session->set_flashdata("msg", '<div class="alert alert-success">' . $this->moduleName . ' Saved successfully!</div>');

        }

        //Get data by id
        $data['item'] = $this->pModel->view($id);
        $this->template($this->folder . "/edit", $data);
    }


    // Delete  Record
    public function delete($id = 0)
    {
        $this->pModel->delete($id);
        $this->session->set_flashdata("msg", '<div class="alert alert-success">' . $this->moduleName . ' deleted successfully!</div>');
        redirect($this->uri->segment(1));
    }


}