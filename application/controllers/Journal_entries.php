<?php

/**
 * Created by PhpStorm.
 * User: development
 * Date: 1/1/2019
 * Time: 3:46 PM
 */
class Journal_entries extends Global_Controller
{
    //Contains Primary model object
    private $pModel = "";

    //declare reusable variables
    private $folder = "journal_entries";
    private $moduleName = "Journal Entries";

    function __construct()
    {
        parent::__construct();
        //Load models here
        $this->load->model("Journal_entries_model");
        $this->pModel = $this->Journal_entries_model;
    }


    // Show listing
    public function index()
    {

        $data = array(
            "heading" => $this->moduleName . " Managment",
            "title" => $this->moduleName . " Managment",
            "description" => "Manage " . $this->moduleName . " from here !",
            "pmKey" => $this->pModel->tableId
        );

        //Set columns for grid
        $data['cols'] = array(
            "journal_entry_date" => "Entry Date",
            "entry_ref" => "Ref",
            "gl_account_name" => "Account",
            "amount" => "Amount",
            "entry_type" => "Entry type",
        );

        //fetch data from database
        $data['items'] = $this->pModel->getData();
        /*echo $this->db->last_query();
        exit;*/
        /*echo '<pre>';
        print_r($data['items']);
        exit;*/
        $this->template($this->folder . "/listing", $data);
    }

    // Add & Submit form
    public function add()
    {
        $journal_id = $this->pModel->_get_journal_id();
        $this->load->model("Gl_accounts_model");
        $data = array(
            "heading" => $this->moduleName . " Managment",
            "title" => $this->moduleName,
            "description" => "Manage " . $this->moduleName . "  from here !"
        );

        //check if form is submitted
        if ($this->input->post("btn_submit") !== NULL) {

            $post = $this->input->post();
            $debit_amount1 = $post['debit_amount1'];
            $credit_amount1 = $post['credit_amount1'];

            $debit_amount2 = $post['debit_amount2'];
            $credit_amount2 = $post['credit_amount2'];
            //Insert First Record
            if (!empty($debit_amount1)) {
                //echo "Debit";
                $entry_type1 = "Debit";
                $amount1 = $debit_amount1;
            }
            if (!empty($credit_amount1)) {
                //echo "Credit";
                $entry_type1 = "Credit";
                $amount1 = $credit_amount1;
            }

            $first_row = array(
                "journal_entry_date" => $this->input->post("entry_date"),
                "amount" => $amount1,
                "entry_type" => $entry_type1,
                "entry_made_by_user_id" => $this->session->userdata('id'),
                "gl_account_id" => $this->input->post("account1"),
                "ref_note" => $this->input->post("narration"),
                "entry_ref" => $this->input->post("reference"),
                "journal_ref_id" => $journal_id,
                "created_at" => date('Y-m-d H:i:s'),
            );
            $message = $this->pModel->add($first_row);

            //Insert Second Record
            if (!empty($debit_amount2)) {
                //echo "Debit";
                $entry_type2 = "Debit";
                $amount2 = $debit_amount2;
            }
            if (!empty($credit_amount2)) {
                //echo "Credit";
                $entry_type2 = "Credit";
                $amount2 = $credit_amount2;
            }
            $second_row = array(
                "journal_entry_date" => $this->input->post("entry_date"),
                "amount" => $amount2,
                "entry_type" => $entry_type2,
                "entry_made_by_user_id" => $this->session->userdata('id'),
                "gl_account_id" => $this->input->post("account2"),
                "ref_note" => $this->input->post("narration"),
                "entry_ref" => $this->input->post("reference"),
                "journal_ref_id" => $journal_id,
                "created_at" => date('Y-m-d H:i:s'),
            );
            $message = $this->pModel->add($second_row);
            /*echo '<pre>';
            print_r($first_row);
            exit;*/

            if ($message) {
                $this->session->set_flashdata("msg", '<div class="alert alert-success">' . $this->moduleName . ' Saved successfully!</div>');
            } else {
                $this->session->set_flashdata("msg", '<div class="alert alert-danger">Error occured! </div>');
            }


        }
        $data['ac_list'] = $this->Gl_accounts_model->getData();
        $this->template($this->folder . "/add", $data);
    }

    // View & update form
    public function edit($id = 0)
    {
        $data = array(
            "heading" => $this->moduleName . " Managment",
            "title" => "Edit " . $this->moduleName,
            "description" => "Manage " . $this->moduleName . " from here !",
            "pmKey" => $this->pModel->tableId
        );

        //check if form is submitted
        if ($this->input->post("btn_submit") !== NULL) {

            //if file selected else only update without file
            $item = array(
                "account_type_name" => $this->input->post("account_type"),
                "updated_at" => date('H:i:s Y-m-d'),
            );
            $this->pModel->edit($item, $id);
            $this->session->set_flashdata("msg", '<div class="alert alert-success">' . $this->moduleName . ' Saved successfully!</div>');

        }

        //Get data by id
        $data['item'] = $this->pModel->view($id);
        $this->template($this->folder . "/edit", $data);
    }


    // Delete  Record
    public function delete($id = 0)
    {
        $this->pModel->delete($id);
        $this->session->set_flashdata("msg", '<div class="alert alert-success">' . $this->moduleName . ' deleted successfully!</div>');
        redirect($this->uri->segment(1));
    }

}