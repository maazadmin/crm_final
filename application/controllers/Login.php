<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model("Users_model");
        $this->load->helper("url");
        $this->load->library("session");
    }
    
    public function index($param = "")
	{
	    if($param=="logout")
        {
            $this->logout();
        }
		$this->load->view('login');
	}

	public function authenticate()
    {
        $user = $this->input->post("userName");
        $pwd  = md5($this->input->post("password"));
        $row = $this->Users_model->checkExist($user);
        if($row!=false)
        {

            if($row->pwd==$pwd)
            {
                $this->session->set_userdata('userID',$row->user_id);
                $this->session->set_userdata('userName',$row->user_name);
                redirect("/dashboard/");
            }else{
                $this->session->set_flashdata('error', '<div class="alert alert-danger">User name or password incorrect!</div>');
                redirect("/login/");
            }
        }else{
                $this->session->set_flashdata('error', '<div class="alert alert-danger">User name or password incorrect!</div>');
            redirect("/login/");
        }
    }


    public function check_user()
    {
        $this->load->model('Users_model');

        //Admin
        $this->db->where('username',$this->input->post('username'));
        $this->db->where('password',$this->Users_model->hash($this->input->post( 'password')));
        //$this->db->where('is_enable',1);
        $this->db->from('users');
        $this->db->join('user_roles' ,'user_roles.user_id = users.user_id');
        
        $user = $this->db->get()->result();
        $role_name = $this->Users_model->get_relation_pax('roles','role_name','role_id',$user[0]->role_id);
        $admin_name = $this->Users_model->get_relation_pax('users','full_name','user_id',$user[0]->admin_id);
        $now = new DateTime();
        $now->setTimezone(new DateTimezone('Asia/Karachi'));
        $current_date = $now->format('Y-m-d');

        $time = new DateTime();
        $time->setTimezone(new DateTimezone('Asia/Karachi'));
        $current_time = $time->format('H:i:s A');
        /*print_r($user);
        exit;*/
        //Admin Login
        if (count ($user) > 0) {
			
			$childNav = $this->getChildNav($user[0]->user_id);
            if ($user[0]->role_id == '1') {
                $user[0]->admin_id = $user[0]->user_id;
            }
             $att = [
                'user_id' => $user[0]->user_id,
                'admin_id' => $user[0]->admin_id,
                'timein' => $current_time,
                'date' => $current_date,
            ];
            $this->db->insert('attendance_hours',$att);
            $att_id = $this->db->insert_id();
            $data = array (
               'id' => $user[0]->user_id,
               'name' => $user[0]->username,
               'fullname' => $user[0]->full_name,
               'email' => $user[0]->email,
               'admin_id' => $user[0]->admin_id,
               'admin_name' => $admin_name,
             //  'user_type' => $user[0]->user_type,
               'role_id' => $user[0]->role_id,
               'role_name' => $role_name,
               'loggedin' => True,
			   'parent_navigations' => $this->getParentNav($childNav),
			   'child_navigations' => $childNav,
			   'allowed_nav' => $this->allowed_nav($childNav),
               'att_id' => $att_id,
            );
			
            

            // echo 'date'.$current_date.'time'.$current_time;
            // exit;

            
            
            /*$login_info['last_login']  = $now;*/

          /*  $this->db->set($login_info);
            $this->db->where('id', $user[0]->id);
            $this->db->update('login');*/

           
            // echo $this->db->last_query();
            // exit;

            $this->session->set_userdata ( $data );  
            $dashboard = 'Dashboard';
            redirect(base_url()."Dashboard/");  
        }
        else 
		{
                $this->session->set_flashdata('error', '<div class="alert alert-danger">User name or password incorrect!</div>');
                redirect(base_url()."login/");
        }  
    }



    public function logout() 
    {   
        $user_id = $this->session->userdata('id');
        $now = new DateTime();
        $now->setTimezone(new DateTimezone('Asia/Karachi'));
        $current_date = $now->format('Y-m-d');

        $checkTime_out = $this->Users_model->checkTime_out($user_id,$current_date);
        if ($checkTime_out) {
            // echo "time out first";
            $this->session->set_flashdata('error', '<div class="alert alert-danger">Please time out fisrt!</div>');
            redirect(base_url()."Dashboard/"); 
        }else{
        $this->Users_model->logout();
        redirect(base_url().'login','refresh');
        }
    }

    public function time_out()
    {
        // check if timein exist
        $user_id = $this->session->userdata('id');
        
        $now = new DateTime();
        $now->setTimezone(new DateTimezone('Asia/Karachi'));
        $current_date = $now->format('Y-m-d');

        $time = new DateTime();
        $time->setTimezone(new DateTimezone('Asia/Karachi'));
        $current_time = $time->format('H:i:s A');

        $check = $this->Users_model->checkAtt($user_id,$current_date);
        if ($check) {
            $att = [
            'timeout' => $current_time,
         ];
         $this->Users_model->editAtt($att,$user_id,$current_date);

         $this->Users_model->logout();
         redirect(base_url().'login','refresh');
        }else{
           $this->session->set_flashdata('error', '<div class="alert alert-danger">Something went wrong!</div>');
        }

    }
	
	private function getParentNav($childNav)
	{
        $result = [];

        if (count($childNav) > 0) {
            $this->load->model('Roles_model');
            $parentIds = array_keys($childNav);
            $navigations = $this->Roles_model->getNavigationByIds($parentIds);
            $result = $navigations ? $navigations : [];
        }

		return $result;
	}
	
	private function getChildNav($userId)
	{
		$result = [];
		$this->load->model('Roles_model');
		$navigations = $this->Roles_model->getUserNavigation($userId, 0);
		if ($navigations) {
			foreach ($navigations as $item) {
				$result[$item['parent_id']][] = $item;
			}
		}
		
		return $result;
	}
	
	private function allowed_nav($navigations)
	{
		$allowedNav = [];
		if (count($navigations) > 0) {
			foreach ($navigations as $item) {
				$allowedNav[] = $item['nav_link'];
			}
		}
		
		return $allowedNav;
	}
}
