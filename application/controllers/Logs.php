<?php

/**
 * Created by PhpStorm.
 * User: development
 * Date: 1/1/2019
 * Time: 3:46 PM
 */
class Logs extends Global_Controller
{
    //Contains Primary model object
    private $pModel = "";

    //declare reusable variables
    private $folder = "logs";
    private $moduleName = "Logs";

    function __construct()
    {
        parent::__construct();
        //Load models here
        $this->load->model("Logs_model");
        $this->pModel = $this->Logs_model;
    }


    // Show listing
    public function index()
    {

        $data = array(
            "heading" => $this->moduleName . " Managment",
            "title" => $this->moduleName . " Managment",
            "description" => "Manage " . $this->moduleName . " from here !",
            "pmKey" => $this->pModel->tableId
        );

        //Set columns for grid
        $data['cols'] = array(
            "deal_id" => "Deal ID",
            "admin_id" => "Admin ID",
            "user_id" => "User ID",
            "action" => "Action",
            "deal_before" => "Deal Before",
            "deal_after" => "Deal After",
        );

        //fetch data from database
        $data['items'] = $this->pModel->getData();

        $this->template($this->folder . "/listing", $data);
    }

    // Add & Submit form
    public function add()
    {
        $data = array(
            "heading" => $this->moduleName . " Managment",
            "title" => $this->moduleName,
            "description" => "Manage " . $this->moduleName . "  from here !"
        );

        //check if form is submitted
        if ($this->input->post("btn_submit") !== NULL) {

            $item = array(
                "warehouse_name" => $this->input->post("warehouse_name"),
                "warehouse_address" => $this->input->post("warehouse_address"),
                "manager_user_id" => $this->session->userdata('id'),
                "warehouse_phone_number" => $this->input->post("warehouse_phone"),
                "created_at" => date('H:i:s Y-m-d'),
            );
            $message = $this->pModel->add($item);
            if ($message) {
                $this->session->set_flashdata("msg", '<div class="alert alert-success">' . $this->moduleName . ' Saved successfully!</div>');
            } else {
                $this->session->set_flashdata("msg", '<div class="alert alert-danger">Error occured! </div>');
            }


        }
        $this->template($this->folder . "/add", $data);
    }

    // View & update form
    public function edit($id = 0)
    {
        $data = array(
            "heading" => $this->moduleName . " Managment",
            "title" => "Edit " . $this->moduleName,
            "description" => "Manage " . $this->moduleName . " from here !",
            "pmKey" => $this->pModel->tableId
        );

        //check if form is submitted
        if ($this->input->post("btn_submit") !== NULL) {


            //if file selected else only update without file
            $item = array(
                "warehouse_name" => $this->input->post("warehouse_name"),
                "warehouse_address" => $this->input->post("warehouse_address"),
                "manager_user_id" => $this->session->userdata('id'),
                "warehouse_phone_number" => $this->input->post("warehouse_phone"),
                "updated_at" => date('H:i:s Y-m-d'),
            );
            $this->pModel->edit($item, $id);
            $this->session->set_flashdata("msg", '<div class="alert alert-success">' . $this->moduleName . ' Saved successfully!</div>');

        }

        //Get data by id
        $data['item'] = $this->pModel->view($id);
        $this->template($this->folder . "/edit", $data);
    }


    // Delete  Record
    public function delete($id = 0)
    {
        $this->pModel->delete($id);
        $this->session->set_flashdata("msg", '<div class="alert alert-success">' . $this->moduleName . ' deleted successfully!</div>');
        redirect($this->uri->segment(1));
    }

}