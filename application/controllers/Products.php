<?php

/**
 * Created by PhpStorm.
 * User: development
 * Date: 1/4/2019
 * Time: 10:11 AM
 */
class Products extends Global_Controller
{
    //Contains Primary model object
    private $pModel = "";

    //declare reusable variables
    private $folder = "products";
    private $moduleName = "Products";

    function __construct()
    {
        parent::__construct();
        //Load models here
        $this->load->model("Products_model");
        $this->pModel = $this->Products_model;
    }


    // Show listing
    public function index()
    {
        $data = array(
            "heading" => $this->moduleName . " Managment",
            "title" => $this->moduleName . " Managment",
            "description" => "Manage " . $this->moduleName . " from here !",
            "pmKey" => $this->pModel->tableId
        );

        //Set columns for grid
        $data['cols'] = array(
            "product_code" => "Product code",
            "product_name" => "Product name",
            "product_description" => "Product description",
            // "is_active" => "is active",
            "unit_name" => "Unit of measurement",
            "min_stock_quantity" => "Min stock quantity",
            "max_stock_quantity" => "Max stock quantity",
            "brand_name" => "Brand",
            "sellprice" => "sell price",
            //"is_combo" => "is_combo",
            "tax_rate_name" => "Tax rate",
            //"is_service" => "is_service",
            //"picture" => "Picture",
            //"on_web" => "on web",
            //"path" => "File"
        );

        //fetch data from database
        $data['items'] = $this->pModel->getData();

        $this->template($this->folder . "/listing", $data);
    }

    // Add & Submit form
    /**
     *
     */
    public function add()
    {
        // Load Suppliers models here
        $this->load->model("Suppliers_model");
        $this->load->model("Warehouses_model");
        $data = array(
            "heading" => $this->moduleName . " Managment",
            "title" => $this->moduleName,
            "description" => "Manage " . $this->moduleName . "  from here !"
        );

        $post = $this->input->post();
        //check if form is submitted
        if ($this->input->post("btn_submit") !== NULL) {
            if (isset($post['is_active'])) {
                $is_active = 1;
            } else {
                $is_active = 0;
            }

            $this->db->trans_start();

            $item = array(
                "product_code" => $this->input->post('product_code'),
                "product_name" => $this->input->post("product_name"),
                "product_description" => $this->input->post("description"),
                "is_active" => $is_active,
                "unit_of_measurement" => $this->input->post("uom_id"),
                "min_stock_quantity" => $this->input->post("min_stock_quantity"),
                "max_stock_quantity" => $this->input->post("max_stock_quantity"),
                "product_brand_id" => $this->input->post("product_brand_id"),
                "sellprice" => $this->input->post("sellprice"),
                "tax_rate_id" => $this->input->post("tax_rate_id"),
                "created_at" => date('H:i:s Y-m-d'),
            );

            $message = $this->pModel->add($item);
            $product_id = $message;
            for ($i = 0; $i < count($post['supplier_name_new']); $i++) {
                $supplier_id_new = $post['supplier_id_new'][$i];
                $supplier_price_new = $post['supplier_price_new'][$i];
                $supplier_data = array(
                    'product_id' => $product_id,
                    'supplier_id' => $supplier_id_new,
                    'cost_price' => $supplier_price_new,
                    'created_at' => date('Y-m-d H:i:s'),
                );

                $message = $this->db->insert('product_suppliers', $supplier_data);
            }

            for ($i = 0; $i < count($post['warehouse_id_new']); $i++) {
                $warehouse_id_new = $post['warehouse_id_new'][$i];
                $warehouse_qty_new = $post['warehouse_qty_new'][$i];
                $warehouse_data = array(
                    'product_id' => $product_id,
                    'warehouse_id' => $warehouse_id_new,
                    'quantity' => $warehouse_qty_new,
                    'created_at' => date('Y-m-d H:i:s'),
                );

                $message = $this->db->insert('product_locations', $warehouse_data);
            }
            // exit;


            $this->db->trans_complete();
            if ($message) {
                $this->session->set_flashdata("msg", '<div class="alert alert-success">' . $this->moduleName . ' Saved successfully!</div>');
            } else {
                $this->session->set_flashdata("msg", '<div class="alert alert-danger">Error occured! </div>');
            }


        }
        $data['warehouses_list'] = $this->Warehouses_model->getData();
        $data['supplier_list'] = $this->Suppliers_model->getData();
        $data['unit_of_measurements_list'] = $this->pModel->getData_unit_of_measurements();
        $data['product_brands_list'] = $this->pModel->getData_product_brands();
        $data['tax_rates_list'] = $this->pModel->getData_tax_rates();
        $this->template($this->folder . "/add", $data);
    }

    //Get Supplier Name
    public function  get_supplier_id()
    {
        $this->load->model("Suppliers_model");
        $supplier_id = $this->input->post('supplier_id');

        $supplier_list = $this->Suppliers_model->view($supplier_id);
        $return = array();
        $return['title'] = $supplier_list['title'];
        $_return = json_encode($return);
        echo $_return;
    }

    //Get Warehouse Name
    public function  get_warehouse_id()
    {
        $this->load->model("Warehouses_model");
        $warehouse_id = $this->input->post('warehouse_id');

        $warehouse_list = $this->Warehouses_model->view($warehouse_id);
        $return = array();
        $return['warehouse_name'] = $warehouse_list['warehouse_name'];
        $_return = json_encode($return);
        echo $_return;
    }

    // View & update form
    public function edit($id = 0)
    {
        $this->load->model("Suppliers_model");
        $this->load->model("Warehouses_model");

        $data = array(
            "heading" => $this->moduleName . " Managment",
            "title" => "Edit " . $this->moduleName,
            "description" => "Manage " . $this->moduleName . " from here !",
            "pmKey" => $this->pModel->tableId
        );

        //check if form is submitted
        if ($this->input->post("btn_submit") !== NULL) {
            $post = $this->input->post();
            if (isset($post['is_active'])) {
                $is_active = 1;
            } else {
                $is_active = 0;
            }
            $this->db->trans_start();
            //Update Product
            $item = array(
                "product_code" => $this->input->post('product_code'),
                "product_name" => $this->input->post("product_name"),
                "product_description" => $this->input->post("description"),
                "is_active" => $is_active,
                "unit_of_measurement" => $this->input->post("uom_id"),
                "min_stock_quantity" => $this->input->post("min_stock_quantity"),
                "max_stock_quantity" => $this->input->post("max_stock_quantity"),
                "product_brand_id" => $this->input->post("product_brand_id"),
                "sellprice" => $this->input->post("sellprice"),
                "tax_rate_id" => $this->input->post("tax_rate_id"),
                "updated_at" => date('H:i:s Y-m-d'),
            );
            $this->pModel->edit($item, $id);
            $product_id = $id;

            //Product Supplier
            for ($i = 0; $i < count($post['supplier_name_new']); $i++) {
                $product_supplier_id_new = $post['product_supplier_id_new'][$i];
                $supplier_id_new = $post['supplier_id_new'][$i];
                $supplier_price_new = $post['supplier_price_new'][$i];
                //Update Product Supplier
                if (!empty($product_supplier_id_new)) {
                    $supplier_data = array(
                        'product_id' => $product_id,
                        'supplier_id' => $supplier_id_new,
                        'cost_price' => $supplier_price_new,
                        'updated_at' => date('Y-m-d H:i:s'),
                    );

                    $this->db->where('product_supplier_id', $product_supplier_id_new);
                    $message = $this->db->update('product_suppliers', $supplier_data);
                } else {
                    //ADD Product Supplier
                    $supplier_data = array(
                        'product_id' => $product_id,
                        'supplier_id' => $supplier_id_new,
                        'cost_price' => $supplier_price_new,
                        'created_at' => date('Y-m-d H:i:s'),
                    );
                    $message = $this->db->insert('product_suppliers', $supplier_data);
                }


            }
            // Product Warehouse
            for ($i = 0; $i < count($post['warehouse_id_new']); $i++) {
                $product_location_id_new = $post['product_location_id_new'][$i];
                $warehouse_id_new = $post['warehouse_id_new'][$i];
                $warehouse_qty_new = $post['warehouse_qty_new'][$i];
                //Update Product Warehouse
                if (!empty($product_location_id_new)) {
                    $warehouse_data = array(
                        'product_id' => $product_id,
                        'warehouse_id' => $warehouse_id_new,
                        'quantity' => $warehouse_qty_new,
                        'updated_at' => date('Y-m-d H:i:s'),
                    );
                    $this->db->where('product_location_id', $product_location_id_new);
                    $message = $this->db->update('product_locations', $warehouse_data);

                } else {
                    //ADD Product Warehouse
                    $warehouse_data = array(
                        'product_id' => $product_id,
                        'warehouse_id' => $warehouse_id_new,
                        'quantity' => $warehouse_qty_new,
                        'created_at' => date('Y-m-d H:i:s'),
                    );

                    $message = $this->db->insert('product_locations', $warehouse_data);
                }

            }

            $this->db->trans_complete();
            $this->session->set_flashdata("msg", '<div class="alert alert-success">' . $this->moduleName . ' Saved successfully!</div>');

        }


        //Get data by id
        $data['product_supplier'] = $this->pModel->getProduct_supplier($id);
        $data['product_warehouse'] = $this->pModel->getProduct_warehouse($id);
        $data['warehouses_list'] = $this->Warehouses_model->getData();
        $data['supplier_list'] = $this->Suppliers_model->getData();
        $data['unit_of_measurements_list'] = $this->pModel->getData_unit_of_measurements();
        $data['product_brands_list'] = $this->pModel->getData_product_brands();
        $data['tax_rates_list'] = $this->pModel->getData_tax_rates();
        $data['item'] = $this->pModel->view($id);
        $this->template($this->folder . "/edit", $data);
    }
    //
    //Delete Product Supplier
    public function delete_product_supplier(){
        $post =   $this->input->post();

        $this->db->where('product_supplier_id',$post['product_supplier_id']);
        $this->db->delete('product_suppliers');
    }

    //Delete Product Warehouses
    public function delete_product_location(){
        $post =   $this->input->post();

        $this->db->where('product_location_id',$post['product_locations_id']);
        $this->db->delete('product_locations');
    }

    // Delete  Record
    public function delete($id = 0)
    {
        $this->pModel->delete($id);
        $this->session->set_flashdata("msg", '<div class="alert alert-success">' . $this->moduleName . ' deleted successfully!</div>');
        redirect($this->uri->segment(1));
    }
}