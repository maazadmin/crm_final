<?php

/**
 * Created by PhpStorm.
 * User: development
 * Date: 1/1/2019
 * Time: 3:46 PM
 */
class Purchase_invoices extends Global_Controller
{
    //Contains Primary model object
    private $pModel = "";

    //declare reusable variables
    private $folder = "purchase_invoices";
    private $moduleName = "Purchase invoices";

    function __construct()
    {
        parent::__construct();
        //Load models here
        $this->load->model("Purchase_invoices_model");
        $this->pModel = $this->Purchase_invoices_model;
    }


    // Show listing
    public function index()
    {

        $data = array(
            "heading" => $this->moduleName . " Managment",
            "title" => $this->moduleName . " Managment",
            "description" => "Manage " . $this->moduleName . " from here !",
            "pmKey" => $this->pModel->tableId
        );

        //Set columns for grid
        $data['cols'] = array(
            "title" => "Supplier",
            "purchase_order_reference" => "Purchase Order Reference",
            "view_detail" => "View Detail"
        );

        //fetch data from database
        $data['items'] = $this->pModel->getData();

        $this->template($this->folder . "/listing", $data);
    }

    public function purchase_order_detail()
    {
        $this->load->model("Customers_model");


        $purchase_order_id = $this->uri->segment(3);

        $result = $this->pModel->view($purchase_order_id);
        $customer_id = $result['customer_id'];


        $data = array(
            "heading" => $this->moduleName . " Managment",
            "title" => $this->moduleName . " Managment",
            "description" => "Manage " . $this->moduleName . " from here !",
            "pmKey" => $this->pModel->tableId
        );

        //Set columns for grid
        $data['cols'] = array(
            "product_name" => "Product Name",
            "product_description" => "Product Description",
            "product_quantity" => "Order Quantity",
            "unit_price" => "Unit Price",
            "line_total" => "Amount",
            //"path" => "File"
        );
        $data['items'] = $this->pModel->get_purchase_item($purchase_order_id);
        $data['items_new2'] = $this->pModel->getData($purchase_order_id);
        /* echo "<pre>";
         print_r($data['items_new2']);
         exit;*/
        $data['item_new'] = $this->Customers_model->view($customer_id);
        //$data['item1'] = $this->Customers_model->view($purchase_order_id);
        $this->template($this->folder . "/item_listing", $data);
    }

    // Add & Submit form
    public function add()
    {
        $data = array(
            "heading" => $this->moduleName . " Managment",
            "title" => $this->moduleName,
            "description" => "Manage " . $this->moduleName . "  from here !"
        );

        //check if form is submitted
        if ($this->input->post("btn_submit") !== NULL) {


            $item = array(
                "account_type_name" => $this->input->post("account_type"),
                "created_at" => date('H:i:s Y-m-d'),
            );
            $message = $this->pModel->add($item);
            if ($message) {
                $this->session->set_flashdata("msg", '<div class="alert alert-success">' . $this->moduleName . ' Saved successfully!</div>');
            } else {
                $this->session->set_flashdata("msg", '<div class="alert alert-danger">Error occured! </div>');
            }


        }
        $this->template($this->folder . "/add", $data);
    }

    // View & update form
    public function edit($id = 0)
    {
        $data = array(
            "heading" => $this->moduleName . " Managment",
            "title" => "Edit " . $this->moduleName,
            "description" => "Manage " . $this->moduleName . " from here !",
            "pmKey" => $this->pModel->tableId
        );

        //check if form is submitted
        if ($this->input->post("btn_submit") !== NULL) {

            //if file selected else only update without file
            $item = array(
                "account_type_name" => $this->input->post("account_type"),
                "updated_at" => date('H:i:s Y-m-d'),
            );
            $this->pModel->edit($item, $id);
            $this->session->set_flashdata("msg", '<div class="alert alert-success">' . $this->moduleName . ' Saved successfully!</div>');

        }

        //Get data by id
        $data['item'] = $this->pModel->view($id);
        $this->template($this->folder . "/edit", $data);
    }


    // Delete  Record
    public function delete($id = 0)
    {
        $this->pModel->delete($id);
        $this->session->set_flashdata("msg", '<div class="alert alert-success">' . $this->moduleName . ' deleted successfully!</div>');
        redirect($this->uri->segment(1));
    }

    //Purchase Invoice Payment
    public function payment_invoice($purchase_invoice_id = 0)
    {

        $data = array(
            "heading" => "Payment Managment",
            "title" => "Payment Managment",
            "description" => "Manage Payment from here !",
            "pmKey" => $this->pModel->tableId
        );
        //check if form is submitted
        if ($this->input->post("btn_submit") !== NULL) {

            $item = array(
                "payment_date" => $this->input->post("payment_date"),
                "payment_entered_by_user_id" => $this->session->userdata('id'),
                "payment_amount" => $this->input->post("payment_amount"),
                "purchase_invoice_id" => $this->input->post("purchase_invoice_id"),
                "payment_method" => $this->input->post("payment_method"),
                "note" => $this->input->post("notes"),
                "created_at" => date('H:i:s Y-m-d'),
            );

            /*echo "<pre>";
            print_r($item);
            exit;*/

            $message = $this->db->insert('purchase_invoice_payments', $item);
            if ($message) {
                $this->session->set_flashdata("msg", '<div class="alert alert-success">Saved successfully!</div>');
            } else {
                $this->session->set_flashdata("msg", '<div class="alert alert-danger">Error occured! </div>');
            }

        }
        $data['payment_ivoice_list'] = $this->pModel->get_payment_invoice($purchase_invoice_id);
        $data['items'] = $this->pModel->getData($purchase_invoice_id);
        $this->template($this->folder . "/payment_invoice", $data);
    }

    //Print Invoice
    public function print_invoice(){
        $data = array();
        $this->load->model("Customers_model");
        $purchase_order_id = $this->uri->segment(3);

        $result = $this->pModel->view($purchase_order_id);
        $customer_id = $result['customer_id'];

        $data['items'] = $this->pModel->get_purchase_item($purchase_order_id);
        $data['items_new2'] = $this->pModel->getData($purchase_order_id);

        //Set columns for grid
        $data['cols'] = array(
            "product_name" => "Product Name",
            "product_description" => "Product Description",
            "product_quantity" => "Order Quantity",
            "unit_price" => "Unit Price",
            "line_total" => "Amount",
            //"path" => "File"
        );

        $data['item_new'] = $this->Customers_model->view($customer_id);
        $this->load->view('purchase_invoices/print_invoice',$data);
    }
}