<?php

/**
 * Created by PhpStorm.
 * User: development
 * Date: 1/4/2019
 * Time: 10:11 AM
 */
class Purchase_orders extends Global_Controller
{
    //Contains Primary model object
    private $pModel = "";

    //declare reusable variables
    private $folder = "purchase_orders";
    private $moduleName = "Purchase Orders";

    function __construct()
    {
        parent::__construct();
        //Load models here
        $this->load->model("Purchase_orders_model");
        $this->pModel = $this->Purchase_orders_model;
    }


    // Show listing
    public function index()
    {
        $data = array(
            "heading" => $this->moduleName . " Managment",
            "title" => $this->moduleName . " Managment",
            "description" => "Manage " . $this->moduleName . " from here !",
            "pmKey" => $this->pModel->tableId
        );

        //Set columns for grid
        $data['cols'] = array(
            "title" => "Supplier",
            "job_ref" => "Job ref",
            "deliverydate" => "Delivery Date",
            "view_detail" => "View Detail",
            //"path" => "File"
        );

        //fetch data from database
        $data['items'] = $this->pModel->getData();

        $this->template($this->folder . "/listing", $data);
    }

    public function purchase_order_detail()
    {

        $purchase_invoice_id = $this->uri->segment(3);


        $data = array(
            "heading" => $this->moduleName . " Managment",
            "title" => $this->moduleName . " Managment",
            "description" => "Manage " . $this->moduleName . " from here !",
            "pmKey" => $this->pModel->tableId
        );

        //Set columns for grid
        $data['cols'] = array(
            "product_name" => "Product Name",
            "product_description" => "Product Description",
            "ordered_quantity" => "Order Quantity",
            "product_unit_price" => "Unit Price",
            "line_item_total_price" => "Amount",
            //"path" => "File"
        );
        $data['items'] = $this->pModel->get_purchase_item($purchase_invoice_id);

        $this->template($this->folder . "/item_listing", $data);
    }

    // Add & Submit form
    public function add()
    {
        $post = $this->input->post();
        // Load Suppliers models here
        $this->load->model("Suppliers_model");
        $this->load->model("Job_model");
        $this->load->model("Products_model");
        $data = array(
            "heading" => $this->moduleName . " Managment",
            "title" => $this->moduleName,
            "description" => "Manage " . $this->moduleName . "  from here !"
        );


        //check if form is submitted
        if ($this->input->post("btn_submit") !== NULL) {

            /*echo "<pre>";
            print_r($post);
            exit;*/
            $this->db->trans_start(); // Query will be rolled back

            $item = array(
                "purchase_order_made_by_user_id" => $this->session->userdata('id'),
                "purchase_order_reference" => $this->input->post("purchase_order_reference"),
                "supplier_id" => $this->input->post("supplier"),
                "job_order_id" => $this->input->post("job_order_id"),
                "remarks" => $this->input->post("remarks"),
                "tax" => $this->input->post("tax"),
                "shipping_method" => $this->input->post("shipping_method"),
                "payment_terms" => $this->input->post("payment_terms"),
                "deliverydate" => $this->input->post("delivery_date"),
                "total_amount" => $this->input->post("total_amount"),
                //"delivery_address" => $this->input->post("delivery_address"),
                "created_at" => $this->input->post('po_date'),
            );

            $message = $this->pModel->add($item);
            $purchase_order_id = $message;
            //echo $purchase_order_id;

            if (!empty($post['product_name_new'])) {
                for ($i = 0; $i < count($post['product_id_new']); $i++) {
                    $product_id_new = $post['product_id_new'][$i];
                    $desc_new = $post['desc_new'][$i];
                    $unit_price_new = $post['unit_price_new'][$i];
                    $qty_new = $post['qty_new'][$i];
                    $amount_new = $post['amount_new'][$i];

                    $data = array(
                        'product_id' => $product_id_new,
                        'product_description' => $desc_new,
                        'ordered_quantity' => $qty_new,
                        'product_unit_price' => $unit_price_new,
                        'line_item_total_price' => $amount_new,
                        'purchase_order_id' => $purchase_order_id,
                        'created_at' => date('Y-m-d H:i:s'),
                    );

                    $message = $this->db->insert('purchase_order_items', $data);

                }
            }

            $this->db->trans_complete();
            //exit;
            if ($message) {
                $this->session->set_flashdata("msg", '<div class="alert alert-success">' . $this->moduleName . ' Saved successfully!</div>');
            } else {
                $this->session->set_flashdata("msg", '<div class="alert alert-danger">Error occured! </div>');
            }


        }
        $data['suppliers_list'] = $this->Suppliers_model->getData();
        $data['job_list'] = $this->Job_model->getData();
        $data['product_list'] = $this->Products_model->getData();
        $this->template($this->folder . "/add", $data);
    }

    //Get Product ID
    public function  get_product_id()
    {
        $this->load->model("Products_model");
        $product_id = $this->input->post('product_id');
        $product_list = $this->Products_model->view($product_id);
        $return = array();
        $return['product_name'] = $product_list['product_name'];
        $return['product_price'] = $product_list['sellprice'];
        $_return = json_encode($return);
        echo $_return;
    }

    // View & update form
    public function edit($id = 0)
    {
        $post = $this->input->post();
        $this->load->model("Suppliers_model");
        $this->load->model("Job_model");
        $this->load->model("Products_model");
        $data = array(
            "heading" => $this->moduleName . " Managment",
            "title" => "Edit " . $this->moduleName,
            "description" => "Manage " . $this->moduleName . " from here !",
            "pmKey" => $this->pModel->tableId
        );

        //check if form is submitted
        if ($this->input->post("btn_submit") !== NULL) {

            //if file selected else only update without file
            $this->db->trans_start(); // Query will be rolled back

            $item = array(
                "purchase_order_made_by_user_id" => $this->session->userdata('id'),
                "purchase_order_reference" => $this->input->post("purchase_order_reference"),
                "supplier_id" => $this->input->post("supplier"),
                "job_order_id" => $this->input->post("job_order_id"),
                "remarks" => $this->input->post("remarks"),
                "tax" => $this->input->post("tax"),
                "shipping_method" => $this->input->post("shipping_method"),
                "payment_terms" => $this->input->post("payment_terms"),
                "deliverydate" => $this->input->post("delivery_date"),
                "total_amount" => $this->input->post("total_amount"),
                "created_at" => $this->input->post('po_date'),
                "updated_at" => date('Y-m-d H:i:s'),
            );

            $message = $this->pModel->edit($item, $id);
            $purchase_order_id = $id;
            //echo $purchase_order_id;

            if (!empty($post['product_name_new'])) {
                for ($i = 0; $i < count($post['product_id_new']); $i++) {
                    $product_id_new = $post['product_id_new'][$i];
                    $desc_new = $post['desc_new'][$i];
                    $unit_price_new = $post['unit_price_new'][$i];
                    $qty_new = $post['qty_new'][$i];
                    $amount_new = $post['amount_new'][$i];
                    $item_id = $post['item_id'][$i];

                    if (!empty($item_id)) {
                        $data = array(
                            'product_id' => $product_id_new,
                            'product_description' => $desc_new,
                            'ordered_quantity' => $qty_new,
                            'product_unit_price' => $unit_price_new,
                            'line_item_total_price' => $amount_new,
                            // 'purchase_order_id' => $purchase_order_id,
                            'updated_at' => date('Y-m-d H:i:s'),
                        );


                        $this->db->where('purchase_order_item_id', $item_id);
                        $message = $this->db->update('purchase_order_items', $data);

                    } else {
                        $data1 = array(
                            'product_id' => $product_id_new,
                            'product_description' => $desc_new,
                            'ordered_quantity' => $qty_new,
                            'product_unit_price' => $unit_price_new,
                            'line_item_total_price' => $amount_new,
                            'purchase_order_id' => $purchase_order_id,
                            'created_at' => date('Y-m-d H:i:s'),
                        );

                        $message = $this->db->insert('purchase_order_items', $data1);
                    }


                }
            }

            $this->db->trans_complete();
            if ($message) {
                $this->session->set_flashdata("msg", '<div class="alert alert-success">' . $this->moduleName . ' Saved successfully!</div>');
            } else {
                $this->session->set_flashdata("msg", '<div class="alert alert-danger">Error occured! </div>');
            }

        }
        $data['job_list'] = $this->Job_model->getData();
        $data['product_list'] = $this->Products_model->getData();
        $data['suppliers_list'] = $this->Suppliers_model->getData();
        //Get data by id
        $data['item'] = $this->pModel->view($id);
        $data['item_list'] = $this->pModel->get_purchase_item($id);
        $this->template($this->folder . "/edit", $data);
    }

    //Delete Purchase Item
    public function delete_purchase_item(){
      $post =   $this->input->post();

      $this->db->where('purchase_order_item_id',$post['item_id']);
      $this->db->delete('purchase_order_items');
    }
    // Delete  Record
    public function delete($id = 0)
    {
        $this->pModel->delete($id);
        $this->session->set_flashdata("msg", '<div class="alert alert-success">' . $this->moduleName . ' deleted successfully!</div>');
        redirect($this->uri->segment(1));
    }

    //Invoice
    public function invoice($id = 0)
    {
        $purchase_order_id = $id;
        $post = $this->input->post();
        // Load Suppliers models here
        $this->load->model("Suppliers_model");
        $this->load->model("Job_model");
        $this->load->model("Products_model");
        $this->load->model("Customers_model");
        $data = array(
            "heading" => $this->moduleName . " Managment",
            "title" => $this->moduleName,
            "description" => "Manage " . $this->moduleName . "  from here !"
        );


        //check if form is submitted
        if ($this->input->post("btn_submit") !== NULL) {


            /*echo "<pre>";
            print_r($post);
            exit;*/
            $this->db->trans_start(); // Query will be rolled back

            $item1 = array(
                "is_purchase_invoice" => 1,
            );

                        $this->db->where('purchase_order_id',$purchase_order_id);
            $message =  $this->db->update('purchase_orders',$item1);

            $item = array(
                "from_purchase_order_id" => $purchase_order_id,
                "invoice_entered_by_user_id" => $this->session->userdata('id'),
                "inv_no" => $this->input->post("inv_no"),
                "inv_date" => $this->input->post("inv_date"),
                "customer_id" => $this->input->post("customer"),
                "supplier_id" => $this->input->post("supplier"),
                "job_order_id" => $this->input->post("job_order_id"),
                "us_pkr" => $this->input->post("us_pkr"),
                "sale_sqm" => $this->input->post("sale_sqm"),
                "combo_sale_combo" => $this->input->post("combo_sale_sqm"),
                "note_print_on_invoice" => $this->input->post("note_print"),
                "remarks" => $this->input->post("remarks"),
                "tax" => $this->input->post("tax"),
                "discount" => $this->input->post("discount"),
                "sub_total" => $this->input->post("total_amount"),
                "created_at" => date('Y-m-d H:i:s'),
            );
            /* echo "<pre>";
             print_r($item);
             exit;*/
            $message = $this->db->insert('purchase_invoices', $item);
            $purchase_invoice_id = $this->db->insert_id();
            //echo $purchase_order_id;

            if (!empty($post['product_name_new'])) {
                for ($i = 0; $i < count($post['product_id_new']); $i++) {
                    $product_id_new = $post['product_id_new'][$i];
                    $desc_new = $post['desc_new'][$i];
                    $unit_price_new = $post['unit_price_new'][$i];
                    $qty_new = $post['qty_new'][$i];
                    $amount_new = $post['amount_new'][$i];

                    $data = array(
                        'product_id' => $product_id_new,
                        'product_description' => $desc_new,
                        'product_quantity' => $qty_new,
                        'unit_price' => $unit_price_new,
                        'line_total' => $amount_new,
                        'purchase_invoice_id' => $purchase_invoice_id,
                        'created_at' => date('Y-m-d H:i:s'),
                    );

                    $message = $this->db->insert('purchase_invoice_items', $data);

                }
            }

            $this->db->trans_complete();
            //exit;
            if ($message) {
                $this->session->set_flashdata("msg", '<div class="alert alert-success">Invoice successfully Created!</div>');
            } else {
                $this->session->set_flashdata("msg", '<div class="alert alert-danger">Error occured! </div>');
            }


        }
        $data['suppliers_list'] = $this->Suppliers_model->getData();
        $data['customer_list'] = $this->Customers_model->getData();
        $data['job_list'] = $this->Job_model->getData();
        $data['product_list'] = $this->Products_model->getData();
        $this->template($this->folder . "/invoice", $data);

    }

}