<?php

/**
 * Created by PhpStorm.
 * User: development
 * Date: 1/1/2019
 * Time: 3:46 PM
 */
class Report extends Global_Controller
{
    //Contains Primary model object
    private $pModel = "";

    //declare reusable variables
    private $folder = "report";
    private $moduleName = "Report";

    function __construct()
    {
        parent::__construct();
        //Load models here
        $this->load->model("Report_model");
        $this->pModel = $this->Report_model;
    }


    // Balance Sheet
    public function balance_sheet()
    {

        $data = array(
            "heading" => "Balance Sheet",
            "title" => "Balance Sheet Managment",
            "description" => "Manage Balance Sheet from here !"
        );
		

		$data['report_range'] = '1/1/'.date('Y').' - '.date('d/m/Y');


        //Set columns for grid
        $data['cols'] = array(
            "account_type_name" => "Account Type",
        );

        //Get Assets Data
        $data['asset_report'] = $this->pModel->get_report('Asset');

        //Get liability Data
        $data['liability_report'] = $this->pModel->get_report('Liability');

        //Get equity Data
        $data['equity_report'] = $this->pModel->get_report('Equity');

        //fetch data from database
        $data['items'] = $this->pModel->getData();

        $this->template($this->folder . "/balance_sheet", $data);
    }

    // Bank Book
    public function bank_book()
    {

        $data = array(
            "heading" => "Ledger Report",
            "title" => "Ledger Report",
            "description" => "Manage Ledger Report from here !",
            "pmKey" => $this->pModel->tableId
        );

        //Set columns for grid
        $data['cols'] = array(
            "account_type_name" => "Account Type",
        );

        //fetch data from database
        $data['items'] = $this->pModel->getData();

        $this->template($this->folder . "/bank_book", $data);
    }

    // Income Statement
    public function income_statment()
    {

        $data = array(
            "heading" => "Income Statement",
            "title" => "Income Statement",
            "description" => "Manage Income Statement from here !",
            "pmKey" => $this->pModel->tableId
        );

        //Set columns for grid
        $data['cols'] = array(
            "account_type_name" => "Account Type",
        );

        //Get Income Data
        $data['income_report'] = $this->pModel->get_report('income');

        //Get Expense Data
        $data['expense_report'] = $this->pModel->get_report('expense');

        /*echo "<pre>";
        print_r($data['expense_report']);
        echo $this->db->last_query();
        exit;*/
        //fetch data from database
        $data['items'] = $this->pModel->getData();

        $this->template($this->folder . "/income_statment", $data);
    }

    // Cash Book
    public function cash_book()
    {

        $data = array(
            "heading" => "Ledger Report",
            "title" => "Ledger Report",
            "description" => "Manage Ledger Report from here !",
            "pmKey" => $this->pModel->tableId
        );

        //Set columns for grid
        $data['cols'] = array(
            "account_type_name" => "Account Type",
        );

        //fetch data from database
        $data['items'] = $this->pModel->getData();

        $this->template($this->folder . "/cash_book", $data);
    }

    // Cash Book
    public function ledger()
    {

        $data = array(
            "heading" => "Ledger Report",
            "title" => "Ledger Report",
            "description" => "Manage Ledger Report from here !",
            "pmKey" => $this->pModel->tableId
        );

        //Set columns for grid
        $data['cols'] = array(
            "account_type_name" => "Account Type",
        );

        //fetch data from database
        $data['items'] = $this->pModel->getData();

        $this->template($this->folder . "/ledger", $data);
    }



}