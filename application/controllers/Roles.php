<?php

/**
 * Created by PhpStorm.
 * User: development
 * Date: 1/1/2019
 * Time: 3:46 PM
 */
class Roles extends Global_Controller
{
    //Contains Primary model object
    private $pModel = "";

    //declare reusable variables
    private $folder = "roles";
    private $moduleName = "Roles";

    function __construct()
    {
        parent::__construct();
        //Load models here
        $this->load->model("Roles_model");
        $this->pModel = $this->Roles_model;
    }

    // Show listing
    public function index()
    {

        $data = array(
            "heading" => $this->moduleName . " Managment",
            "title" => $this->moduleName . " Managment",
            "description" => "Manage " . $this->moduleName . " from here !",
            "pmKey" => $this->pModel->tableId
        );
        //
        //Set columns for grid
        $data['cols'] = array(
            "role_name" => "Role Name",
        );

        //fetch data from database
        $data['items'] = $this->pModel->getData();

        $this->template($this->folder . "/listing", $data);
    }

    //User Role Module/Navigation
    public function get_nav_list($user_id)
    {

        $data = array(
            "heading" => $this->moduleName . " Managment",
            "title" => $this->moduleName,
            "description" => "Manage " . $this->moduleName . "  from here !"
        );
        //Set columns for grid
        $data['cols'] = array(
            "role_navigation_id" => "Module ID",
            "nav_name" => "Module Name",
        );

        //fetch data from database
        $data['items'] = $this->pModel->get_role_navigation($user_id);
        //role_listing
        $this->template($this->folder . "/nav_listing", $data);
    }

    // Add & Submit form
    public function add()
    {
        $data = array(
            "heading" => $this->moduleName . " Managment",
            "title" => $this->moduleName,
            "description" => "Manage " . $this->moduleName . "  from here !"
        );

        //check if form is submitted
        if ($this->input->post("btn_submit") !== NULL) {
            $post = $this->input->post();

            $this->db->trans_start(); // Query will be rolled back
            $item = array(
                "role_name" => $this->input->post("role_name"),
                "created_at" => date('H:i:s Y-m-d'),
            );
            $message = $this->pModel->add($item);
            $role_id = $message;
            //ADD Module For Roles
            for ($i = 0; $i < count($post['nav_id_new']); $i++) {
                $nav_id_new = $post['nav_id_new'][$i];
                $data = array(
                    'role_id' => $role_id,
                    'navigation_id' => $nav_id_new,
                    'created_at' => date('Y-m-d H:i:s'),
                );

                $message = $this->db->insert('role_navigations', $data);
            }
            $this->db->trans_complete();

            if ($message) {
                $this->session->set_flashdata("msg", '<div class="alert alert-success">' . $this->moduleName . ' Saved successfully!</div>');
            } else {
                $this->session->set_flashdata("msg", '<div class="alert alert-danger">Error occured! </div>');
            }

        }
        $data['navigation_list'] = $this->pModel->getData_navigation();
        $this->template($this->folder . "/add", $data);
    }

    // View & update form
    public function edit($id = 0)
    {
        $data = array(
            "heading" => $this->moduleName . " Managment",
            "title" => "Edit " . $this->moduleName,
            "description" => "Manage " . $this->moduleName . " from here !",
            "pmKey" => $this->pModel->tableId
        );

        //check if form is submitted
        if ($this->input->post("btn_submit") !== NULL) {
            $post = $this->input->post();

            $this->db->trans_start(); // Query will be rolled back

            $item = array(
                "role_name" => $this->input->post("role_name"),
                "updated_at" => date('H:i:s Y-m-d'),
            );

            $message = $this->pModel->edit($item, $id);
            $role_id = $id;
            //ADD Module For Roles
            for ($i = 0; $i < count($post['nav_id_new']); $i++) {
                $nav_id_new = $post['nav_id_new'][$i];
                $role_navigation_id = $post['role_navigation_id'][$i];

                //Update Role Navigation
                if (!empty($role_navigation_id)) {

                    $data = array(
                        'role_id' => $role_id,
                        'navigation_id' => $nav_id_new,
                        'updated_at' => date('Y-m-d H:i:s'),
                    );
                    $this->db->where('role_navigation_id', $role_navigation_id);
                    $message = $this->db->update('role_navigations', $data);

                } //ADD Role Navigation
                else {
                    $data = array(
                        'role_id' => $role_id,
                        'navigation_id' => $nav_id_new,
                        'created_at' => date('Y-m-d H:i:s'),
                    );
                    $message = $this->db->insert('role_navigations', $data);
                }


            }
            $this->db->trans_complete();

            $this->session->set_flashdata("msg", '<div class="alert alert-success">' . $this->moduleName . ' Saved successfully!</div>');

        }


        $data['item'] = $this->pModel->view($id);
        $data['navigation_list'] = $this->pModel->getData_navigation();
        $data['role_navigation'] = $this->pModel->get_role_navigation($id);
        $this->template($this->folder . "/edit", $data);
    }

    //Get Nav ID
    public function  get_nav_id()
    {
        $nav_id = $this->input->post('nav_id');
        $nav_list = $this->pModel->getData_navigation($nav_id);
        $return = array();
        $return['nav_name'] = $nav_list[0]['nav_name'];
        $_return = json_encode($return);
        echo $_return;
    }
    //Delete role Navigation
    public function  delete_roles_nav(){
        $post =   $this->input->post();

        $this->db->where('role_navigation_id',$post['role_navigation_id']);
        $this->db->delete('role_navigations');
    }

    // Delete  Record
    public function delete($id = 0)
    {
        $this->pModel->delete($id);
        $this->session->set_flashdata("msg", '<div class="alert alert-success">' . $this->moduleName . ' deleted successfully!</div>');
        redirect($this->uri->segment(1));
    }


}