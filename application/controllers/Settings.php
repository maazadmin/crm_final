<?php

/**
 * Created by PhpStorm.
 * User: development
 * Date: 1/1/2019
 * Time: 3:46 PM
 */
class Settings extends Global_Controller
{
    //Contains Primary model object
    private $pModel = "";

    //declare reusable variables
    private $folder = "settings";
    private $moduleName = "Accounting";

    function __construct()
    {
        parent::__construct();
        //Load models here
        $this->load->model("Settings_model");
        $this->pModel = $this->Settings_model;
    }


    /// Show listing
    public function index()
    {

        $data = array(
            "heading" => $this->moduleName . " Managment",
            "title" => $this->moduleName . " Managment",
            "description" => "Manage " . $this->moduleName . " from here !",
            "pmKey" => $this->pModel->tableId
        );

        ///Set columns for grid
        $data['cols'] = array(
            "account_type_name" => "Account Type",
        );

        //fetch data from database
        $data['items'] = $this->pModel->getData();

        $this->template($this->folder . "/listing", $data);
    }


    // Add & Submit form
    public function ledger_settings()
    {
        $this->load->model("Gl_accounts_model");
        $data = array(
            "heading" => $this->moduleName . " Managment",
            "title" => $this->moduleName,
            "description" => "Manage " . $this->moduleName . "  from here !"
        );


        /* echo $data['po_gl_enabled']['setting_id'];
           exit;*/
        ///check if form is submitted
        if ($this->input->post("btn_purchase_submit") !== NULL) {
            $post = $this->input->post();

            $po_gl_enabled = 0;
            $po_credit_gl_id = "";
            $po_debit_gl_id = "";
            $po_gl_note = "";

            // Purchase Record

            $this->db->trans_start(); // Query will be rolled back
            //Is Enable
            //Update Record
            if (!empty($post['po_gl_enabled'])) {
                $item = array("value" => $post['po_is_enable'],
                    "updated_at" => date('Y-m-d H:i:s'));
                $message = $this->pModel->edit($item, $post['po_gl_enabled']);
            }//Insert Record
            else {
                $item = array(
                    'key'        => 'po_gl_enabled',
                    'value'      => $post['po_is_enable'],
                    'created_at' => date('Y-m-d H:i:s'));
                $message = $this->pModel->add($item);
            }

            //PO_CREDIT_GL_ID

            //Update Record
            if (!empty($post['po_credit_gl_id'])) {
                $item = array("value" => $post['po_gl_credit_account'],
                    "updated_at" => date('Y-m-d H:i:s'));
                $message = $this->pModel->edit($item, $post['po_credit_gl_id']);
            }//Insert Record
            else {
                $item = array(
                    'key' => 'po_credit_gl_id',
                    'value' => $post['po_gl_credit_account'],
                    'created_at' => date('Y-m-d H:i:s'));
                $message = $this->pModel->add($item);
            }


            //PO_DEBIT_GL_ID

            //Update Record
            if (!empty($post['po_debit_gl_id'])) {
                $item = array("value" => $post['po_gl_account_debit'],
                    "updated_at" => date('Y-m-d H:i:s'));
                $message = $this->pModel->edit($item, $post['po_debit_gl_id']);
            }//Insert Record
            else {
                $item = array(
                    'key' => 'po_debit_gl_id',
                    'value' => $post['po_gl_account_debit'],
                    'created_at' => date('Y-m-d H:i:s'));
                $message = $this->pModel->add($item);
            }


            //PO_GL_NOTE

            //Update Record
            if (!empty($post['po_gl_note'])) {
                $item = array("value" => $post['po_notes'],
                    "updated_at" => date('Y-m-d H:i:s'));
                $message = $this->pModel->edit($item, $post['po_gl_note']);
            }//Insert Record
            else {
                $item = array(
                    'key' => 'po_gl_note',
                    'value' => $post['po_notes'],
                    'created_at' => date('Y-m-d H:i:s'));
                $message = $this->pModel->add($item);
            }

            $this->db->trans_complete();

            if ($message) {
                /*echo "Success";
                exit;*/
                $this->session->set_flashdata("msg", '<div class="alert alert-success">' . $this->moduleName . ' Saved successfully!</div>');
            } else {
                 /*
                   echo "Falied";
                   exit;
                 */
               // $this->session->set_flashdata("msg", '<div class="alert alert-danger">Error occured! </div>');
                $this->session->set_flashdata("msg", '<div class="alert alert-success"> Saved Changes successfully!</div>');

            }


        }
        //Purchase
        $data['po_gl_enabled'] = $this->pModel->get_ledger_setting('po_gl_enabled');
        $data['po_credit_gl_id'] = $this->pModel->get_ledger_setting('po_credit_gl_id');
        $data['po_debit_gl_id'] = $this->pModel->get_ledger_setting('po_debit_gl_id');
        $data['po_gl_note'] = $this->pModel->get_ledger_setting('po_gl_note');

        $data['ac_list'] = $this->Gl_accounts_model->getData();
        $this->template($this->folder . "/ledger_settings", $data);
    }

    // View & update form
    public function edit($id = 0)
    {
        $data = array(
            "heading" => $this->moduleName . " Managment",
            "title" => "Edit " . $this->moduleName,
            "description" => "Manage " . $this->moduleName . " from here !",
            "pmKey" => $this->pModel->tableId
        );

        //check if form is submitted
        if ($this->input->post("btn_submit") !== NULL) {

            //if file selected else only update without file
            $item = array(
                "account_type_name" => $this->input->post("account_type"),
                "updated_at" => date('H:i:s Y-m-d'),
            );
            $this->pModel->edit($item, $id);
            $this->session->set_flashdata("msg", '<div class="alert alert-success">' . $this->moduleName . ' Saved successfully!</div>');

        }

        //Get data by id
        $data['item'] = $this->pModel->view($id);
        $this->template($this->folder . "/edit", $data);
    }


    // Delete  Record
    public function delete($id = 0)
    {
        $this->pModel->delete($id);
        $this->session->set_flashdata("msg", '<div class="alert alert-success">' . $this->moduleName . ' deleted successfully!</div>');
        redirect($this->uri->segment(1));
    }

}