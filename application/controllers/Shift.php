<?php



class Shift extends Global_Controller
{
	//Contains Primary model object
    private $pModel = "";

    //declare reusable variables
    private $folder = "shift";
    private $moduleName = "Shift";
	
	function __construct()
	{
		parent::__construct();
		$this->load->model("Shift_model");
        $this->pModel = $this->Shift_model;
	}


	public function index()
    {
// echo 'Pakistan';
// exit();
        $data = array(
            "heading" => $this->moduleName . " Managment",
            "title" => $this->moduleName . " Managment",
            "description" => "Manage " . $this->moduleName . " from here !",
            "pmKey" => $this->pModel->tableId
        );

        //Set columns for grid
        $data['cols'] = array(
            "shift_name" => "Shift Name",
            "shift_start" => "Shift Start",
            "shift_end" => "Shift End",
        );

        //fetch data from database
        $data['items'] = $this->pModel->getData();

        $this->template($this->folder . "/listing", $data);
    }

    // Add & Submit form
    public function add()
    {
        $data = array(
            "heading" => $this->moduleName . " Managment",
            "title" => $this->moduleName,
            "description" => "Manage " . $this->moduleName . "  from here !"
        );

        //check if form is submitted
        if ($this->input->post("btn_submit") !== NULL) {

            $item = array(
                "shift_name" => $this->input->post("shift_name"),
                "shift_start" => $this->input->post("shift_start"),
                "shift_end" => $this->input->post("shift_end"),
                "created_at" => date('H:i:s Y-m-d')
            );
            $message = $this->pModel->add($item);
            if ($message) {
                $this->session->set_flashdata("msg", '<div class="alert alert-success">' . $this->moduleName . ' Saved successfully!</div>');
                redirect($this->uri->segment(1));
            } else {
                $this->session->set_flashdata("msg", '<div class="alert alert-danger">Error occured! </div>');
            }


        }
        $this->template($this->folder . "/add", $data);
    }

    // View & update form
    public function edit($id = 0)
    {
        $data = array(
            "heading" => $this->moduleName . " Managment",
            "title" => "Edit " . $this->moduleName,
            "description" => "Manage " . $this->moduleName . " from here !",
            "pmKey" => $this->pModel->tableId
        );

        //check if form is submitted
        if ($this->input->post("btn_submit") !== NULL) {


            //if file selected else only update without file
            $item = array(
                "shift_name" => $this->input->post("region_name"),
                "shift_start" => $this->input->post("shift_start"),
                "shift_end" => $this->input->post("shift_end"),
                "updated_at" => date('H:i:s Y-m-d')
            );
            $this->pModel->edit($item, $id);
            // echo $this->db->last_query();
            // exit();
            $this->session->set_flashdata("msg", '<div class="alert alert-success">' . $this->moduleName . ' Saved successfully!</div>');
            redirect($this->uri->segment(1));

        }

        //Get data by id
        $data['item'] = $this->pModel->view($id);
        $this->template($this->folder . "/edit", $data);
    }


    // Delete  Record
    public function delete($id = 0)
    {
        $this->pModel->delete($id);
        $this->session->set_flashdata("msg", '<div class="alert alert-success">' . $this->moduleName . ' deleted successfully!</div>');
        redirect($this->uri->segment(1));
    }

}