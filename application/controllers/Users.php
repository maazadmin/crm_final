<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends Global_Controller
{

    //Contains Primary model object
    private $pModel = "";
    private $rModel = "";

    //declare reusable variables
    private $folder = "users";
    private $moduleName = "Users";

    function __construct()
    {
        parent::__construct();
        //Load models here
        $this->load->model("Users_model");
        $this->pModel = $this->Users_model;
        $this->load->model("User_role_model");
        $this->rModel = $this->User_role_model;
    }

    // Show listing
    public function index()
    {

        $data = array(
            "heading" => $this->moduleName . " Managment",
            "title" => $this->moduleName . " Managment",
            "description" => "Manage " . $this->moduleName . " from here !",
            "pmKey" => $this->pModel->tableId
        );

        //Set columns for grid
        $data['cols'] = array(
            "username" => "Username",
            "full_name" => "Full name",
            "email" => "Email",
        );

        //fetch data from database
        $data['items'] = $this->pModel->getData();

        $this->template($this->folder . "/listing", $data);
    }

    public function my_account(){

        $data = array(
            "heading" => "My Account",
            "title" => "My Account",
            "description" => "Manage your  from here !"
        );

        //check if form is submitted
        if ($this->input->post("btn_submit") !== NULL) {
            $post = $this->input->post();

            //Update Password
            if(isset($post['change_password'])){

                $item = array(
                    //"username" => $this->input->post("username"),
                    "password" => $this->pModel->hash($this->input->post("password")),
                    "email" => $this->input->post("email"),
                    "updated_at" => date('H:i:s Y-m-d'),
                );
            }
            //Update Email
            else{

                $item = array(
                    "email" => $this->input->post("email"),
                    "updated_at" => date('H:i:s Y-m-d'),
                );
            }
                        /*$this->db->where('user_id',$this->session->userdata('id'));
            $message =  $this->db->update('users',$item);*/
            $message =$this->pModel->edit($item,$this->session->userdata('id'));


            if ($message) {

                $this->session->set_flashdata("msg", '<div class="alert alert-success"> Saved Changes successfully!</div>');
            } else {
                $this->session->set_flashdata("msg", '<div class="alert alert-danger">Error occured! </div>');
            }


        }

        $data['item'] = $this->pModel->view($this->session->userdata('id'));

        $this->template($this->folder . "/my_account", $data);
    }
    //User Role Listing
    public function get_user_roles($user_id)
    {

        $data = array(
            "heading" => $this->moduleName . " Managment",
            "title" => $this->moduleName,
            "description" => "Manage " . $this->moduleName . "  from here !"
        );
        //Set columns for grid
        $data['cols'] = array(
            "role_id" => "Role ID",
            "role_name" => "Role Name",
        );

        //fetch data from database
        $data['items'] = $this->pModel->get_user_roles($user_id);

        //role_listing
        $this->template($this->folder . "/role_listing", $data);
    }

    // Add & Submit form
    public function add()
    {
        $this->load->model("Roles_model");
        $this->load->model("Users_model");
        $this->load->model('Region_model');
        $this->load->model('Campaign_model');
        $this->load->model('Currency_model');
        $this->load->model('Shift_model');
        $this->load->model('Center_model');
        $this->load->model('Product_model');
        $data = array(
            "heading" => $this->moduleName . " Managment",
            "title" => $this->moduleName,
            "description" => "Manage " . $this->moduleName . "  from here !"
        );

        //check if form is submitted
        if ($this->input->post("btn_submit") !== NULL) {
            $post = $this->input->post();

            $this->db->trans_start(); // Query will be rolled back

            $item = array(
                "region_id" => $this->input->post("region_id"),
                "password" => $this->pModel->hash($this->input->post("password")),
                "currency_id" => $this->input->post("currency_id"),
                "center_id" => $this->input->post("center_id"),
                "username" => $this->input->post("username"),
                "email" => $this->input->post("email"),
                "full_name" => $this->input->post("full_name"),
                "admin_id" => $this->input->post("admin_id"),
                "is_login_allowed" => $this->input->post("active_login"),
                "created_at" => date('H:i:s Y-m-d'),
            );
            $message = $this->pModel->add($item);
            $last_id = $this->db->insert_id();

                $roles_data = array(
                    'user_id' => $last_id, 
                    'role_id' => $this->input->post("user_type_id"), 
                     "created_at" => date('H:i:s Y-m-d'),

                );
                $this->db->insert('user_roles', $roles_data);



          $my_post = $this->input->post();

           for ($i=0; $i < count($this->input->post('product_value')) ; $i++) { 
               
                $product_list = array(
                   "products_id" =>  $my_post['product_value'][$i],
                   "users_id" => $last_id,
                  
                );
                $this->db->insert('users_products',$product_list);


           }
            for ($i=0; $i < count($this->input->post('campaign_value')) ; $i++) { 
               
                $product_list = array(
                   "campaign_id" =>  $my_post['campaign_value'][$i],
                   "users_id" => $last_id,
     
                );
                $this->db->insert('users_campaign',$product_list);


           }
            for ($i=0; $i < count($this->input->post('shift_value')) ; $i++) { 
               
                $shift_list = array(
                   "shift_id" =>  $my_post['shift_value'][$i],
                   "users_id" => $last_id,

                );
                $this->db->insert('users_shift',$shift_list);


           }
          




//             if(isset($post['campaign_chk'])){
// $last_id = $this->db->insert_id();
//             $total_campaign = $this->Campaign_model->getData();





//             foreach ($total_campaign as $campaign_value) {

// // echo '<pre>';
// // print_r($campaign_value);
// // echo '</pre>';
// // exit();
//                 $campaign_list = array(
//                    "campaign_id" => $campaign_value['campaign_id'],
//                    "users_id" => $last_id,
//                 );
//                 $this->db->insert('users_campaign',$campaign_list);
               
                
//             }

           
//               }
//                  if(isset($post['product_chk'])){
//             $last_id = $this->db->insert_id();
//             $total_product = $this->Product_model->getData();
//            // $total_campaign = $this->Campaign_model->getData();





//             foreach ($total_product as $product_value) {

// // echo '<pre>';
// // print_r($product_value);
// // echo '</pre>';
// // exit();
//                 $product_list = array(
//                    "products_id" => $product_value['product_id'],
//                    "users_id" => $last_id,
//                 );
//                 $this->db->insert('users_products',$product_list);
               
                
//             }

           
//               }


            // echo $this->db->last_query();
            // exit();
            // $user_id = $message;

            // for ($i = 0; $i < count($post['role_id_new']); $i++) {
            //     $role_id_new = $post['role_id_new'][$i];
            //     $data = array(
            //         'user_id' => $user_id,
            //         'role_id' => $role_id_new,
            //         'created_at' => date('Y-m-d H:i:s'),
            //     );

            //     $message = $this->db->insert('user_roles', $data);
            // }


            $this->db->trans_complete();
            if ($message) {
                $this->session->set_flashdata("msg", '<div class="alert alert-success">' . $this->moduleName . ' Saved successfully!</div>');
                    redirect($this->uri->segment(1));
            } else {
                $this->session->set_flashdata("msg", '<div class="alert alert-danger">Error occured! </div>');
            }


        }


        $data['region_list'] = $this->Region_model->getData();
        $data['shift_list'] = $this->Shift_model->getData();
        $data['currency_list'] = $this->Currency_model->getData();
        $data['campaign_list'] = $this->Campaign_model->getData();
        $data['center_list'] = $this->Center_model->getData();
        $data['product_list'] = $this->Product_model->getData();


        $data['roles_list'] = $this->Roles_model->getData();
        $data['admin_list'] = $this->Users_model->getDataAdmin();
        $this->template($this->folder . "/add", $data);
    }

    //Get Role ID
    public function  get_role_id()
    {
        $this->load->model("Roles_model");
        $role_id = $this->input->post('role_id');
        $role_list = $this->Roles_model->view($role_id);
        $return = array();
        $return['role_name'] = $role_list['role_name'];
        $_return = json_encode($return);
        echo $_return;
    }

    // View & update form
    public function edit($id = 0)
    {
         $this->load->model("Roles_model");
        $this->load->model('Region_model');
        $this->load->model('Campaign_model');
        $this->load->model('Currency_model');
        $this->load->model('Shift_model');
        $this->load->model('Center_model');
        $this->load->model('Product_model');
        $this->load->model("Roles_model");
        $data = array(
            "heading" => $this->moduleName . " Managment",
            "title" => "Edit " . $this->moduleName,
            "description" => "Manage " . $this->moduleName . " from here !",
            "pmKey" => $this->pModel->tableId
        );

        //check if form is submitted
        if ($this->input->post("btn_submit") !== NULL) {
            $post = $this->input->post();
            $this->db->trans_start(); // Query will be rolled back
            //if file selected else only update without file


              $item = array(
                "region_id" => $this->input->post("region_id"),
                //"password" => $this->pModel->hash($this->input->post("password")),
                "currency_id" => $this->input->post("currency_id"),
                "center_id" => $this->input->post("center_id"),
                "username" => $this->input->post("username"),
                "email" => $this->input->post("email"),
                "full_name" => $this->input->post("full_name"),
                "is_login_allowed" => $this->input->post("active_login"),
                "created_at" => date('H:i:s Y-m-d'),
            );
            // $item = array(
            //       //"region_id" => $this->input->post("region_id"),
            //     "username" => $this->input->post("username"),
            //     //"password" => $this->pModel->hash($this->input->post("password")),
            //     "full_name" => $this->input->post("full_name"),
            //     "email" => $this->input->post("email"),
            //     "is_login_allowed" => $this->input->post("active_login"),
            //     "updated_at" => date('H:i:s Y-m-d'),
            // );
            $message = $this->pModel->edit($item, $id);
            //$user_id = $id;
           
           //$fianl_result =  $this->pModel->get_role_idd($id);

         
          //   echo $user_role_idd;
          // // $fianl_result = $this->get_role_id();
          //  echo '<pre>';
          //  print_r($role_data);
          //  echo '</pre>';
          //  exit();
               $role_id =  $this->input->post('roles_id');
               // echo $role_id;
               // exit();
            $user_role = array(
               // 'user_id' => $user_id, 
                'role_id' => $this->input->post("user_type_id"), 
                'updated_at' => date('Y-m-d H:i:s')
            );

            $this->rModel->edit($user_role,$role_id);


            // echo $this->db->last_query();
            // exit();




            // if (!empty($post['role_id_new'])) {
            //     for ($i = 0; $i < count($post['role_id_new']); $i++) {
            //         $role_id_new = $post['role_id_new'][$i];
            //         $user_role_id = $post['user_role_id'][$i];

            //         //Update User Roles
            //         if (!empty($user_role_id)) {
            //             $data = array(
            //                 'user_id' => $user_id,
            //                 'role_id' => $role_id_new,
            //                 'updated_at' => date('Y-m-d H:i:s'),
            //             );
            //             $this->db->where('user_role_id', $user_id);
            //             $message = $this->db->update('user_roles', $data);
            //         } //Add User Roles
            //         else {
            //             $data = array(
            //                 'user_id' => $user_id,
            //                 'role_id' => $role_id_new,
            //                 'created_at' => date('Y-m-d H:i:s'),
            //             );
            //             $message = $this->db->insert('user_roles', $data);
            //         }
            //     }
            // }


            $this->db->trans_complete();

            $this->session->set_flashdata("msg", '<div class="alert alert-success">' . $this->moduleName . ' Saved successfully!</div>');
            redirect($this->uri->segment(1));

        }

        //Get data by id
         $data['region_list'] = $this->Region_model->getData();
        $data['shift_list'] = $this->Shift_model->getData();
        $data['currency_list'] = $this->Currency_model->getData();
        $data['campaign_list'] = $this->Campaign_model->getData();
        $data['center_list'] = $this->Center_model->getData();
        $data['product_list'] = $this->Product_model->getData();


        $data['roles_list'] = $this->Roles_model->getData();
           $data['role_data'] = $this->pModel->role_view($id);
            // $user_role_idd = $role_data[0]->user_role_id;

        $data['item'] = $this->pModel->view($id);
        //$data['roles_list'] = $this->Roles_model->getData();
       // $data['user_roles'] = $this->pModel->get_user_roles($id);

        $this->template($this->folder . "/edit", $data);
    }

    //Delete User Roles
    public function delete_user_roles()
    {
        $post = $this->input->post();

        $this->db->where('user_role_id', $post['user_role_id']);
        $this->db->delete('user_roles');
    }


    // Delete  Record
    public function delete($id = 0)
    {
        $this->pModel->delete($id);
        $this->session->set_flashdata("msg", '<div class="alert alert-success">' . $this->moduleName . ' deleted successfully!</div>');
        redirect($this->uri->segment(1));
    }
    // Check Email Exists
    public function email_exists()
    {
        $email = $this->input->post('email');
        $email_record = $this->pModel->checkExist_email($email);
        if(!empty($email_record)){

            echo '1';

        }
        else{
            echo '0';

        }
    }
    //Check User Exists
     // Check Email Exists
    public function username_exists()
    {
        $username = $this->input->post('username');
        $username_record = $this->pModel->checkExist_username($username);
        if(!empty($username_record)){

            echo '1';

        }
        else{
            echo '0';

        }
    }

}
