<?php
defined('BASEPATH') OR exit('No direct script access allowed');
ini_set ( 'max_execution_time', 1200);

class Global_Controller extends CI_Controller {

	function __construct()
    {
        parent::__construct();
        //set default timezone
        date_default_timezone_set("Asia/Karachi");

        //load global helpers  and libaries
        $this->load->library('session');
        $this->load->helper('url');

        //check for login
        if(!$this->session->has_userdata('id'))
        {
            redirect("/login/index");
        }
		
		//check permission
		//$this->authenticatePermission();
    }

    public function do_upload($folder,$fileName,$types = "pdf")
    {
        $config['upload_path'] = $folder;
        $config['file_name'] = $fileName;
        $config['allowed_types'] = $types;
        $config['max_size']     = '1024';

        $this->load->library('upload', $config);
        if ( ! $this->upload->do_upload('resFile'))
        {
            return $this->upload->display_errors();
        }
        else
        {
            return "1";
        }
    }

    /**
     * To output json for apis
     * @param $response
     * @return json
     */
    public function apiOutput($response = array()){

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
            ->_display();
        exit;
    }

	/**
	 *  Output templates 
	 *  @param (string)$page
	 *  @param (array)$data
	 *  @return (view)
	 */
    public function template($page,$data = array())
    {
		$data['navigations'] = $this->getNavigations(); 
        $this->load->view("layout/header", $data);
        $this->load->view($page, $data);
        $this->load->view("layout/footer");
        $this->load->view("js_functions");
    }
	
	private function authenticatePermission()
	{
		if($this->session->has_userdata('allowed_nav'))
        {
			$allowedNavigations = $this->session->userdata('allowed_nav');
			if (!in_array($this->uri->segment(1), $allowedNavigations)) {
				redirect(base_url().'unauthorized');
			}
		}
	}
	
	private function getNavigations()
	{
		$navigations = [];
		if($this->session->has_userdata('parent_navigations'))
        {
		  $navigations['parent_nav'] = $this->session->userdata('parent_navigations');
		  $navigations['child_nav'] = $this->session->userdata('child_navigations');
		}
		
		return $navigations;
	}

    public function log($deal_id, $deal_before_data, $deal_after_data, $action, $comment)
    {
        $this->load->model('logs_model');
        $data = [
          "deal_id" => $deal_id,
          "user_id" => $this->session->userdata('id'),
          "admin_id" =>  $this->session->userdata('admin_id'),
          "url" => current_url(),
          "deal_before" => json_encode($deal_before_data),
          "deal_after" => json_encode($deal_after_data),
          "action" => $action,
          "comment" => $comment,
          "ip_address" => $this->input->ip_address(),
          "date" => date('Y-m-d H:i:s'),
        ];
        $this->logs_model->add($data);
    }
}
