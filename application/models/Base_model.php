<?php

class Base_model extends CI_Model{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function get($filter)
    {
        if (!empty($filter['column']) && !empty($filter['value'])) {
            $result = $this->db->get_where($filter['table'], array($filter['column'] => $filter['value']));
        }
        else {
            $result = $this->db->get($filter['table']);
        }

        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }
}