<?php


class Center_model extends CI_Model
{

    public $tableName = "center";
    public $tableId   = "center_id";

    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function add($data)
    {
        $this->db->insert($this->tableName, $data);
        return $this->db->insert_id();
    }

    public function edit($data, $id)
    {
        $this->db->where($this->tableId, $id);
        $this->db->update($this->tableName, $data);
    }

    public function delete($id)
    {
        return $this->db->delete($this->tableName, array($this->tableId => $id));
    }

    public function view($id)
    {
        $result = $this->db->get_where($this->tableName, array($this->tableId => $id));
        if ($result->num_rows() > 0) {
            return $result->row_array();
        } else {
            return false;
        }
    }

public function getData()
    {
        /*$this->db->order_by($this->tableId, "DESC");
        $result = $this->db->get($this->tableName);*/

        $this->db->select('*');
        $this->db->from($this->tableName);
        // $this->db->join('currency', 'currency.currency_id = '.$this->tableName.'.'.'currency_id');

        $result = $this->db->get();

        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }
    // public function getData()
    // {
    //     //$this->db->order_by($this->tableId, "DESC");
    //     $result = $this->db->get($this->tableName);
    //     if ($result->num_rows() > 0) {
    //         return $result->result_array();
    //     } else {
    //         return false;
    //     }
    // }
}