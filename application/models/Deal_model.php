<?php


class Deal_model extends CI_Model
{

    public $tableName = "deal";
    public $tableId   = "deal_id";

    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function add($data)
    {
        $this->db->insert($this->tableName, $data);
        return $this->db->insert_id();
    }

    public function edit($data, $id)
    {
        $this->db->where($this->tableId, $id);
        $this->db->update($this->tableName, $data);
    }

    public function delete($id)
    {
        return $this->db->delete($this->tableName, array($this->tableId => $id));
    }

    public function view($id)
    {
        $result = $this->db->get_where($this->tableName, array($this->tableId => $id));
        if ($result->num_rows() > 0) {
            return $result->row_array();
        } else {
            return false;
        }
    }

    public function get_data_by_id($deal_id){
      $this->db->where('deal_id',$deal_id);
      $deal_data = $this->db->get('loan_info');
      return $deal_data->result_array();
    }

     public function get_comments_by_id($deal_id){
      $this->db->where('deal_id',$deal_id);
      $data = $this->db->get('logs');
      return $data->result_array();
    }


    public function get_all_data_by_assignuserid($id){

        $this->db->where('assigned_user_id',$id);
       // $this->db->where('forum_type',$forum_type);
        $this->db->order_by($this->tableId, "DESC");
        $result = $this->db->get($this->tableName);
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
      }

        public function get_all_data_by_userid($id,$status){

        $this->db->where('user_id',$id);
        $this->db->where('deal_status',$status);
        $this->db->where('assigned_user_id',NULL);
        $this->db->order_by($this->tableId, "DESC");
        $result = $this->db->get($this->tableName);
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
      }

        public function get_all_data_by_userid_sent($id,$status){

        $this->db->where('user_id',$id);
        $this->db->where('deal_status',$status);
        $this->db->order_by($this->tableId, "DESC");
        $result = $this->db->get($this->tableName);
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
      }
    
    public function get_all_data($forum_type = NULL){
       
        
       

        // if ($this->session->userdata('admin_id') == 0) {
        //     $this->db->or_where('user_id',$this->session->userdata('id'));
        //     $this->db->where('admin_id',0);
        //     $this->db->or_where('admin_id',$this->session->userdata('id'));
        // }else{
        //     $this->db->or_where('user_id',$this->session->userdata('admin_id'));
        //     $this->db->where('admin_id',0);
        //     $this->db->or_where('user_id',$this->session->userdata('id'));
        //     $this->db->where('admin_id',$this->session->userdata('admin_id'));
        // }
        

        if(!empty($forum_type)){
         $this->db->where('forum_type',$forum_type);
        }
        $this->db->where('admin_id',$this->session->userdata('admin_id'));
        $this->db->where('assigned_user_id',NULL);
        $this->db->where('deal_status',0);
        $this->db->order_by($this->tableId, "DESC");
        $result = $this->db->get($this->tableName);

        // echo $this->db->last_query();
        // exit();
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }

    // $this->db->select('*');
    // // I use aliasing make joins easier
    // $this->db->where('deal_status',0);
    // $this->db->from('deal As del');
    // $this->db->join('customer_info AS ci', 'del.deal_id = ci.deal_id');
    // $this->db->join('deal_product AS dp', 'del.deal_id = dp.deal_id');
    // $this->db->join('loan_info AS li', 'del.deal_id = li.deal_id');
    // $this->db->join('payment_info AS pi', 'del.deal_id = pi.deal_id');
    // $this->db->join('insert_notes AS in', 'del.deal_id = in.deal_id');
    // $this->db->join('payment_detail AS pd', 'del.deal_id = pd.deal_id');
    // $result = $this->db->get();
    // return $result->result_array();
    
    }


    public function get_all_data_byid($id){







$this->db->select('*');
    // I use aliasing make joins easier
   // $this->db->where('deal_status',0);

    $this->db->from('deal As del');
    $this->db->where('del.deal_id',$id);
   // $this->db->from('vw_deal');
  //   $this->db->join('loan_info AS li', 'del.deal_id = li.deal_id');
  //  $this->db->join('payment_info AS pi', 'del.deal_id = pi.deal_id');
  //  $this->db->join('customer_info AS ci', 'del.deal_id = ci.deal_id');
  // $this->db->join('insert_notes AS in', 'del.deal_id = in.deal_id');
  //  $this->db->join('deal_product AS dp', 'del.deal_id = dp.deal_id');





       $this->db->join('customer_info AS ci', 'del.deal_id = ci.deal_id');
   $this->db->join('deal_product AS dp', 'del.deal_id = dp.deal_id');
   $this->db->join('loan_info AS li', 'del.deal_id = li.deal_id');
  $this->db->join('payment_info AS pi', 'del.deal_id = pi.deal_id');
   $this->db->join('insert_notes AS in', 'del.deal_id = in.deal_id');
   $this->db->join('payment_detail AS pd', 'del.deal_id = pd.deal_id');
    
    $result = $this->db->get();
    // echo $this->db->last_query();
    // exit();
    return $result->result_array();






    // $this->db->select('*');
    //$this->db->where('deal_status',0);
    // $this->db->from('vw_deal');// I use aliasing make joins easier
   //  $this->db->join('loan_info AS li', 'del.deal_id = li.deal_id','outer');
   // $this->db->join('payment_info AS pi', 'del.deal_id = pi.deal_id','full outer');
   // $this->db->join('customer_info AS ci', 'del.deal_id = ci.deal_id','full outer');
   // $this->db->join('insert_notes AS in', 'del.deal_id = in.deal_id','full outer');
   //  $this->db->join('deal_product AS dp', 'del.deal_id = dp.deal_id','full outer');
    // $this->db->where('deal_id',$id);
    // $result = $this->db->get();
    // return $result->result_array();
    //$this->db->join('region', 'region.region_id = '.$this->tableName.'.'.'region_id');
    
    }

    public function getData()
    {
        //$this->db->order_by($this->tableId, "DESC");
        $result = $this->db->get($this->tableName);
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }
        //check relation reference
    public function get_relation_pax($table,$column,$reference,$input)
    {
        $this->db->select($column);
        $this->db->where($reference,$input);
        $result =  $this->db->get($table)->result();
        return $result[0]->$column;
    }
}