<?php


class Job_model extends CI_Model
{

    public $tableName = "job_order";
    public $tableId = "job_id";

    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function add($data)
    {
        $this->db->insert($this->tableName, $data);
        return $this->db->insert_id();
    }

    public function edit($data, $id)
    {
        $this->db->where($this->tableId, $id);
        $this->db->update($this->tableName, $data);
    }

    public function delete($id)
    {
        return $this->db->delete($this->tableName, array($this->tableId => $id));
    }

    public function view($id)
    {
        $result = $this->db->get_where($this->tableName, array($this->tableId => $id));
        if ($result->num_rows() > 0) {
            return $result->row_array();
        } else {
            return false;
        }
    }

    public function getData($id = 0)
    {
        if (!empty($id)) {
            $this->db->where($this->tableId, $id);
        }
        $this->db->order_by($this->tableId, "DESC");
        $result = $this->db->get($this->tableName);
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    //Get Invoice List
    public function getInvoice($sales_invoice_id = 0)
    {
        $this->db->select('*');
        $this->db->from('sales_invoices');
        if(!empty($sales_invoice_id)){
        $this->db->where('sales_invoices.sales_invoice_id',$sales_invoice_id);
        }
        $this->db->join('customers', 'customers.customer_id = sales_invoices.customer_id');
        $result = $this->db->get();
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }
    //
    public function getInvoice_customer($id)
    {
        $this->db->select('*');

        $this->db->from('sales_invoices');
        $this->db->where('sales_invoices.sales_invoice_id',$id);
        $this->db->join('customers', 'customers.customer_id = sales_invoices.customer_id');
        $result = $this->db->get();
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    //Get Invoice Order Item
    public function  get_sales_item($sales_invoice_id)
    {

        $this->db->select('products.*, sales_invoice_items.*,sales_invoice_items.product_description as item_description');
        $this->db->from('sales_invoice_items');
        $this->db->where('sales_invoice_items.sales_invoice_id', $sales_invoice_id);
        $this->db->join('products', 'products.product_id = sales_invoice_items.product_id');

        $result = $this->db->get();
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    //Get Payment Invoice
    public function get_payment_invoice($sales_invoice_id){
        $this->db->select('*');
        $this->db->from('sales_invoice_payments');
        $this->db->where('sales_invoice_payments.sales_invoice_id', $sales_invoice_id);
        $this->db->join('sales_invoices', 'sales_invoices.sales_invoice_id = sales_invoice_payments.sales_invoice_id');

        $result = $this->db->get();
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }
}