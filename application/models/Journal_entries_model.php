<?php


class Journal_entries_model extends CI_Model
{

    public $tableName = "journal_entries";
    public $tableId = "journal_entry_id";

    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function add($data)
    {
        $this->db->insert($this->tableName, $data);
        return $this->db->insert_id();
    }

    public function edit($data, $id)
    {
        $this->db->where($this->tableId, $id);
        $this->db->update($this->tableName, $data);
    }

    public function delete($id)
    {
        return $this->db->delete($this->tableName, array($this->tableId => $id));
    }

    public function view($id)
    {
        $result = $this->db->get_where($this->tableName, array($this->tableId => $id));
        if ($result->num_rows() > 0) {
            return $result->row_array();
        } else {
            return false;
        }
    }

    public function getData()
    {
        $this->db->select('*');
        $this->db->from($this->tableName);
       // $this->db->group_by($this->tableName.'.journal_ref_id');
        //$this->db->group_by($this->tableName.'.entry_type');
        $this->db->join('gl_accounts', 'gl_accounts.gl_account_id = ' . $this->tableName . '.' . 'gl_account_id');
        $result = $this->db->get();
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    //Get Last Id Journal List
    public function _get_journal_id()
    {
        $this->db->select('*');
        $this->db->order_by($this->tableId, "DESC");
        $this->db->limit(1);
        $result = $this->db->get($this->tableName);
        if( $result->num_rows() > 0 )
        {
            $last_num = $result->row_array();
            $initel_num = $last_num[$this->tableId];
            $regi_code = ($initel_num + 1 );
            return $regi_code;
        }
        else
        {
            $regi_code = 1 ;
            return $regi_code;
        }
    }
}