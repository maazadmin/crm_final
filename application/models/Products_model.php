<?php


class Products_model extends CI_Model
{

    public $tableName = "products";
    public $tableId = "product_id";

    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function add($data)
    {
        $this->db->insert($this->tableName, $data);
        return $this->db->insert_id();
    }

    public function edit($data, $id)
    {
        $this->db->where($this->tableId, $id);
        $this->db->update($this->tableName, $data);
    }

    public function delete($id)
    {
        return $this->db->delete($this->tableName, array($this->tableId => $id));
    }

    public function view($id)
    {
        $result = $this->db->get_where($this->tableName, array($this->tableId => $id));
        if ($result->num_rows() > 0) {
            return $result->row_array();
        } else {
            return false;
        }
    }

    public function getData()
    {
        $this->db->select('*');
        $this->db->from($this->tableName);
        $this->db->join('product_brands', 'product_brands.product_brand_id = ' . $this->tableName . '.' . 'product_brand_id');
        $this->db->join('tax_rates', 'tax_rates.tax_rate_id = ' . $this->tableName . '.' . 'tax_rate_id');
        $this->db->join('unit_of_measurements', 'unit_of_measurements.uom_id = ' . $this->tableName . '.' . 'unit_of_measurement');
        $result = $this->db->get();
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function getData_unit_of_measurements()
    {
        $this->db->order_by("uom_id", "DESC");
        $result = $this->db->get("unit_of_measurements");


        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    //Get Products Brands
    public function getData_product_brands()
    {
        $this->db->order_by("product_brand_id", "DESC");
        $result = $this->db->get("product_brands");


        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    //GET Tax Rates
    public function getData_tax_rates()
    {
        $this->db->order_by("tax_rate_id", "DESC");
        $result = $this->db->get("tax_rates");


        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    //Get Product Supplier
    public function getProduct_supplier($product_id)
    {

        $this->db->select('*');
        $this->db->from('product_suppliers');
        $this->db->where('product_suppliers.product_id', $product_id);
        $this->db->join('suppliers', 'suppliers.supplier_id = product_suppliers.supplier_id');
        $result = $this->db->get();
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    //Get Product Warehouse
    public function getProduct_warehouse($product_id)
    {

        $this->db->select('*');
        $this->db->from('product_locations');
        $this->db->where('product_locations.product_id', $product_id);
        $this->db->join('warehouses', 'warehouses.warehouse_id = product_locations.warehouse_id');
        $result = $this->db->get();
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }
}