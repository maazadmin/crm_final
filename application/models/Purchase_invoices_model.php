<?php


class Purchase_invoices_model extends CI_Model
{

    public $tableName = "purchase_invoices";
    public $tableId = "purchase_invoice_id";

    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function add($data)
    {
        $this->db->insert($this->tableName, $data);
        return $this->db->insert_id();
    }

    public function edit($data, $id)
    {
        $this->db->where($this->tableId, $id);
        $this->db->update($this->tableName, $data);
    }

    public function delete($id)
    {
        return $this->db->delete($this->tableName, array($this->tableId => $id));
    }

    public function view($id)
    {
        $result = $this->db->get_where($this->tableName, array($this->tableId => $id));
        if ($result->num_rows() > 0) {
            return $result->row_array();
        } else {
            return false;
        }
    }

    public function getData($id = 0)
    {
        $this->db->select('*');
        $this->db->from($this->tableName);
        if (!empty($id)) {
            $this->db->where('purchase_invoices.purchase_invoice_id', $id);
        }
        $this->db->join('suppliers', 'suppliers.supplier_id = ' . $this->tableName . '.' . 'supplier_id');
        $this->db->join('purchase_orders', 'purchase_orders.purchase_order_id = ' . $this->tableName . '.' . 'from_purchase_order_id');
        $result = $this->db->get();
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    /*public function getData_view($id)
    {
        $this->db->select('*');
        $this->db->from($this->tableName);
        $this->db->where('purchase_invoices.purchase_invoice_id', $id);
        $this->db->join('suppliers', 'suppliers.supplier_id = ' . $this->tableName . '.' . 'supplier_id');
        $this->db->join('purchase_orders', 'purchase_orders.purchase_order_id = ' . $this->tableName . '.' . 'from_purchase_order_id');
        $result = $this->db->get();
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }*/

//Get Purchase Order Item
    public function  get_purchase_item($purchase_invoice_id)
    {

        $this->db->select('products.*, purchase_invoice_items.*,purchase_invoice_items.product_description as item_description');
        $this->db->from('purchase_invoice_items');
        $this->db->where('purchase_invoice_items.purchase_invoice_id', $purchase_invoice_id);
        $this->db->join('products', 'products.product_id = purchase_invoice_items.product_id');

        $result = $this->db->get();
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    //Get Payment Invoice
    public function get_payment_invoice($purchase_invoice_id){
        $this->db->select('*');
        $this->db->from('purchase_invoice_payments');
        $this->db->where('purchase_invoice_payments.purchase_invoice_id', $purchase_invoice_id);
        $this->db->join('purchase_invoices', 'purchase_invoices.purchase_invoice_id = purchase_invoice_payments.purchase_invoice_id');

        $result = $this->db->get();
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }
}