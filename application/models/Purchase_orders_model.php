<?php


class Purchase_orders_model extends CI_Model
{

    public $tableName = "purchase_orders";
    public $tableId = "purchase_order_id";

    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function add($data)
    {
        $this->db->insert($this->tableName, $data);
        return $this->db->insert_id();
    }

    public function edit($data, $id)
    {
        $this->db->where($this->tableId, $id);
        $this->db->update($this->tableName, $data);
    }

    public function delete($id)
    {
        return $this->db->delete($this->tableName, array($this->tableId => $id));
    }

    public function view($id)
    {
        $result = $this->db->get_where($this->tableName, array($this->tableId => $id));
        if ($result->num_rows() > 0) {
            return $result->row_array();
        } else {
            return false;
        }
    }

    public function getData()
    {
        /*$this->db->order_by($this->tableId, "DESC");
        $result = $this->db->get($this->tableName);
        */
        $this->db->select('*');
        $this->db->from($this->tableName);
        $this->db->join('suppliers', 'suppliers.supplier_id = ' . $this->tableName . '.' . 'supplier_id');
        $this->db->join('job_order', 'job_order.job_id = ' . $this->tableName . '.' . 'job_order_id');
        // $this->db->join('warehouses', 'warehouses.warehouse_id = '.$this->tableName.'.'.'warehouse_id');
        $result = $this->db->get();
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    //Get Purchase Order Item
    public function  get_purchase_item($purchase_order_id)
    {

        $this->db->select('products.*, purchase_order_items.*,purchase_order_items.product_description as item_description');
        $this->db->from('purchase_order_items');
        $this->db->where('purchase_order_items.purchase_order_id', $purchase_order_id);
        $this->db->join('products', 'products.product_id = purchase_order_items.product_id');

        $result = $this->db->get();
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }
}