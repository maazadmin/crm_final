<?php


class Report_model extends CI_Model
{

    public $tableName = "journal_entries";
    public $tableId = "journal_entry_id";

    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function add($data)
    {
        $this->db->insert($this->tableName, $data);
        return $this->db->insert_id();
    }

    public function edit($data, $id)
    {
        $this->db->where($this->tableId, $id);
        $this->db->update($this->tableName, $data);
    }

    public function delete($id)
    {
        return $this->db->delete($this->tableName, array($this->tableId => $id));
    }

    public function view($id)
    {
        $result = $this->db->get_where($this->tableName, array($this->tableId => $id));
        if ($result->num_rows() > 0) {
            return $result->row_array();
        } else {
            return false;
        }
    }

    public function getData()
    {
        $this->db->order_by($this->tableId, "DESC");
        $result = $this->db->get($this->tableName);
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }
    //Get Report
    public function get_report($accountType, $dateFrom = "", $dateTo = ""){
        $this->db->select('*');
        $this->db->from('journal_entries');
		$this->db->where('account_types.account_type_name',$accountType);
		
		if (!empty($dateFrom) && !empty($dateTo)) {
			$this->db->where('journal_entries.journal_entry_date >=',$dateFrom);
			$this->db->where('journal_entries.journal_entry_date <=',$dateTo);
		} else{
			$this->db->where('journal_entries.journal_entry_date >=', (date('Y').'-1-1'));
			$this->db->where('journal_entries.journal_entry_date <=', date('Y-m-d'));
		}
        
        $this->db->join('gl_accounts', 'gl_accounts.gl_account_id = journal_entries.gl_account_id');
        $this->db->join('account_types', 'account_types.account_type_id = gl_accounts.account_type_id');
        $result =  $this->db->get();
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }
}