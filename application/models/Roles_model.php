<?php


class Roles_model extends CI_Model
{

    public $tableName = "roles";
    public $tableId = "role_id";
    

    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function add($data)
    {
        $this->db->insert($this->tableName, $data);
        return $this->db->insert_id();
    }

    public function edit($data, $id)
    {
        $this->db->where($this->tableId, $id);
        $this->db->update($this->tableName, $data);
    }

    public function delete($id)
    {
        return $this->db->delete($this->tableName, array($this->tableId => $id));
    }

    public function view($id)
    {
        $result = $this->db->get_where($this->tableName, array($this->tableId => $id));
        if ($result->num_rows() > 0) {
            return $result->row_array();
        } else {
            return false;
        }
    }

    public function getData()
    {
        $this->db->order_by($this->tableId, "ASC");
        if ($this->session->userdata('role_id') == 1) {
            $this->db->where('role_id !=','1');
            $this->db->where('role_id !=','6');
        }else{
            $this->db->where('role_id !=','6');
        }
        $result = $this->db->get($this->tableName);
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }
    //Get Module List
    public function getData_navigation($id){
        if(!empty($id)){
            $this->db->where('navigation_id', $id);
        }
        $this->db->order_by('navigation_id', "DESC");
        $result = $this->db->get('navigations');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }
    //Get Role Naviation
    public function get_role_navigation($role_id){
        $this->db->select('*');
        $this->db->from('role_navigations');
        $this->db->where('role_navigations.role_id',$role_id);
        $this->db->join('navigations', 'navigations.navigation_id = role_navigations.navigation_id');
        $result =  $this->db->get();
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }
	
	//Get User Naviations
    public function getUserNavigation($userId, $isParent = 1){
        $this->db->select('navigations.*');
        $this->db->from('role_navigations');
        $this->db->where('user_roles.user_id', $userId);
		if ($isParent == 1) {
	        $this->db->where('navigations.parent_id IS NULL');
		} else{
			$this->db->where('navigations.parent_id IS NOT NULL');
		}
		$this->db->join('user_roles', 'user_roles.role_id = role_navigations.role_id');
        $this->db->join('navigations', 'navigations.navigation_id = role_navigations.navigation_id');
        $result =  $this->db->get();
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }
	
	public function getNavigationByIds($ids = [])
	{
		$result = $this->db->from('navigations')->where_in('navigation_id', $ids)->get();
		
		if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
	}
}