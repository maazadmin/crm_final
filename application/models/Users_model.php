<?php
class Users_model extends CI_Model{

    public $tableName = "users";
    public $tableUser = "username";
    public $tableEmail = "email";
    public $tableId = "user_id";

    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function checkExist($userName)
    {
        $result = $this->db->get_where($this->tableName, array($this->tableUser => $userName));
        if($result->num_rows() > 0)
        {
            return $result->first_row();
        }else{
            return false;
        }
    }

    public function checkAtt($id,$date)
    {   
        $this->db->where('user_id',$id);
        $this->db->where('date',$date);
        $result = $this->db->get('attendance_hours');
        // echo $this->db->last_query();
        // exit;
        if($result->num_rows() > 0)
        {
            return true;
        }else{
            return false;
        }
        
    }

    public function checkTime_out($id,$date)
    {   
        $this->db->where('user_id',$id);
        $this->db->where('date',$date);
        $this->db->where('timeout',NULL);
        $result = $this->db->get('attendance_hours');
        if($result->num_rows() > 0)
        {
            return true;
        }else{
            return false;
        }
        
    }

    public function editAtt($data, $id,$date)
    {
        $this->db->where('attendance_hour_id',$this->session->userdata('att_id'));
        $this->db->where('user_id', $id);
        $this->db->where('date',$date);
       return $this->db->update('attendance_hours', $data);
       echo $this->db->last_query();
       exit;

    }
      public function checkExist_email($email)
    {
        $result = $this->db->get_where($this->tableName, array($this->tableEmail => $email));
        if($result->num_rows() > 0)
        {
            return $result->first_row();
        }else{
            return false;
        }
    }
       public function checkExist_username($username)
    {
        $result = $this->db->get_where($this->tableName, array($this->tableUser => $username));
        if($result->num_rows() > 0)
        {
            return $result->first_row();
        }else{
            return false;
        }
    }

       public function hash($string) {
        return hash ( 'sha512', $string . config_item ( 'encryption_key' ) );
    }

    public function logout() {
        $now = date('Y-m-d H:i:s');
        $login_info['last_logout']  = $now;
        $this->db->set($login_info);
        $this->db->where('id', $this->session->userdata('id'));
        $this->db->update('login');
        $this->session->sess_destroy();
    }

    public function add($data)
    {
        $this->db->insert($this->tableName, $data);
        return $this->db->insert_id();
    }

    public function edit($data, $id)
    {
        $this->db->where($this->tableId, $id);
       return $this->db->update($this->tableName, $data);

    }

    public function delete($id)
    {
        return $this->db->delete($this->tableName, array($this->tableId => $id));
    }

    public function view($id)
    {
        $result = $this->db->get_where($this->tableName, array($this->tableId => $id));
        if ($result->num_rows() > 0) {
            return $result->row_array();
        } else {
            return false;
        }
    }

    public function role_view($id)
    {
        $result = $this->db->get_where('user_roles', array('user_id' => $id));
       
            return $result->result();
       
    }

    public function getData()
    {


        $this->db->order_by($this->tableId, "DESC");

        $result = $this->db->get($this->tableName);
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function getDataAdmin()
    {
        $this->db->order_by($this->tableId, "DESC");
        $this->db->where('role_id','1');
        $result = $this->db->get('user_roles');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

// public function get_role_idd($user_id){


//             $this->db->select('');
//             $this->db->from('user_roles');
//             $this->db->where('user_roles.user_id', $user_id);
//              $this->db->join('roles', 'roles.role_id = user_roles.role_id');
//               $result = $this->db->get();


// }
    //Get User Roles
    public function get_user_roles($user_id){

        $this->db->select('*');
        $this->db->from('user_roles');
        $this->db->where('user_roles.user_id', $user_id);
        $this->db->join('roles', 'roles.role_id = user_roles.role_id');
        $result = $this->db->get();
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function get_relation_pax($table,$column,$reference,$input)
    {
        $this->db->select($column);
        $this->db->where($reference,$input);
        $result =  $this->db->get($table)->result();
        return $result[0]->$column;
    }

}