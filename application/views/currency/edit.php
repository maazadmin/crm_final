<!-- Main Content -->
<div class="page-wrapper">
    <div class="container-fluid">

        <!-- Title -->
        <div class="row heading-bg">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h5 class="txt-dark"><?= $heading ?></h5>
            </div>

            <!-- Breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="index.html">Dashboard</a></li>
                    <li><a href="active"><span><?= $heading ?></span></a></li>

                </ol>
            </div>
            <!-- /Breadcrumb -->

        </div>
        <!-- /Title -->

        <!-- Row -->
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default card-view">
                    <div class="panel-heading">
                        <div class="pull-left">
                            <h6 class="panel-title txt-dark"><?php echo $title; ?></h6>
                        </div>
                      
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body">
                              <div class="">
                            <a href="<?= site_url($this->uri->segment(1)) ?>" class="btn btn-success btn-sm">View
                                List</a>
                        </div>
                        <br>
                            <p class="text-muted"><?php echo $description; ?>.</p>

                            <div class="row">
                                <div class="col-sm-12 col-xs-12">
                                    <div class="form-wrap">
                                        <?php
                                        //Show message
                                        echo $this->session->flashdata("msg");
                                        ?>
        <form method="post" class="ac-type-form" enctype="multipart/form-data"
    action="<?= site_url($this->uri->segment(1) . '/edit/' . (($item !== false) ? $item[$pmKey] : "")) ?>" data-toggle="validator" role="form">
                                            <div class="form-group col-md-4">
                                                <label class="control-label mb-10" for="exampleInputuname_1">Currency
                                                    Name
                                                </label>
                                                <input type="text" class="form-control"
                                                       name="currency_name"
                                                       value="<?php echo ($item !== false) ? $item['currency_name'] : ""; ?>"
                                                       placeholder="Currency Name" required>
                                            </div>
                                                                                <div class="form-group col-md-4">
 <label class="control-label mb-10" for="exampleInputuname_1">Select Region
                                                </label>
                                                    <select class="form-control" name="region_id" id="nav_id">
     <option value="">Select Region</option>
                                                <?php foreach ($region_dropdownn as $row1) : 
if ($row1['region_id'] == $item['region_id']) {
                                                                $selected = "selected";
                                                            } else {
                                                                $selected = "";
                                                            }
                                                    ?>

         <option <?= $selected  ?> value="<?= $row1['region_id']?>"><?= $row1['region_name']?></option>
             <?php endforeach; ?>
</select>
</div>
                                               <div class="form-group col-md-4">
                                                <label class="control-label mb-10" for="exampleInputuname_1">Currency 
                                                    Sysmbol
                                                </label>
                                                <input type="text" class="form-control date-picker"
                                                       name="currency_symbol" placeholder="Shift Start"   value="<?php echo ($item !== false) ? $item['currency_symbol'] : ""; ?>" 
                                                        required>
                                            </div>

                                            



                                            <div class="form-group col-md-10">


                                                <button type="submit" class="btn btn-success mr-10" name="btn_submit">
                                                    Submit
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
                <!-- /Row -->

            </div>
            <!-- /Main Content -->
