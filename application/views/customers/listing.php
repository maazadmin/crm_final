<!-- Main Content -->
<div class="page-wrapper">
    <div class="container-fluid">

        <!-- Title -->
        <div class="row heading-bg">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h5 class="txt-dark"><?= $heading?></h5>
            </div>

            <!-- Breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="index.html">Dashboard</a></li>
                    <li><a href="active"><span><?= $heading?></span></a></li>

                </ol>
            </div>
            <!-- /Breadcrumb -->

        </div>
        <!-- /Title -->

        <!-- Row -->
        <div class="row">
            <!-- Basic Table -->
            <div class="col-sm-12">
                <div class="panel panel-default card-view">
                    <div class="panel-heading">
                        <div class="pull-left">
                            <h6 class="panel-title txt-dark"><?php echo $title;?></h6>
                        </div>
                        <div class="pull-right">
                           <a href="<?= site_url($this->uri->segment(1).'/add')?>" class="btn btn-success btn-sm">Add New</a>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body">
                            <p class="text-muted"><?php echo $description;?>.</p>
                            <div class="table-wrap mt-40">
                                <div class="table-responsive">
                                    <table class="table mb-0 table-striped  table-hover" id="customers_table">
                                        <thead>
                                        <tr>
                                            <th>S.NO</th>
                                            <?php
                                            //Set Table Headers
                                            foreach($cols as $value)
                                            {
                                                echo "<th>".$value."</th>";
                                            }
                                            ?>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        //Set Data in table
                                        if($items!==false) {
                                            $i = 0;
                                            foreach ($items as $item) {
                                                $i += 1;
                                                ?>
                                                <tr>
                                                    <td><?= $i ?></td>
                                                    <?php
                                                    foreach ($cols as $key => $value) {
                                                        if ($key == "path") {
                                                            $item[$key] = "<a href='" . base_url() . $item[$key] . "' target='_blank'>View File</a>";
                                                        }
                                                        if($key == "view_detail"){
                                                            $item[$key] = "<a href='" . site_url($this->uri->segment(1).'/view_detail/' . $item[$pmKey]) . "' target='_blank'>View Detail</a>";
                                                        }
                                                        echo "<td>" . $item[$key] . "</td>";
                                                    }
                                                    ?>
                                                    <td>
                                                        <a href="<?= site_url($this->uri->segment(1).'/edit/' . $item[$pmKey]) ?>"
                                                           class="btn btn-primary"><i class="ion-compose"></i> Edit</a>
                                                        <a href="<?= site_url($this->uri->segment(1).'/delete/' . $item[$pmKey]) ?>"
                                                           class="btn btn-danger" onClick="return confirm('Are you sure?');"><i
                                                                class="ion-close"></i> Delete</a>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Basic Table -->
        </div>
        <!-- /Row -->



    <!-- /Main Content -->