
    <?php
    if($this->session->flashdata())
    {
        foreach($this->session->flashdata() as $key => $value):
            if($key == 'update' || $key == 'saved')
            {
                $alert_class = 'alert-success';
            }else
            {
                $alert_class = 'alert-danger';
            }
            ?>
            <div class="alert alert-block <?php echo $alert_class; ?>">
                <button type="button" class="close" data-dismiss="alert">
                    <i class="icon-remove"></i>
                </button>

                <p>
                    <strong>
                        <i class="icon-ok"></i>
                        <?php echo ucwords(strtolower($key)); ?> !
                    </strong>
                    <?php echo $value; ?>

                </p>


            </div>
        <?php
        endforeach;
    }  ?>


   <!-- Main Content -->
        <div class="page-wrapper">
            <div class="container-fluid pt-25">
                <!-- Row -->
                <div class="row">
                    <div class="col-lg-4 col-sm-12">
                        <div class="panel panel-default card-view">
                            <div class="panel-heading">
                                <div class="pull-left">
                                    <h6 class="panel-title txt-dark">Account</h6>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="panel-wrapper collapse in">
                                <div class="panel-body pb-0">
                                    <div class="row">
                                        <div class="col-sm-6 mb-15">
                                            <span class="skill-head mb-20">Ledger Setting</span>
											<span id="pie_chart_1" class="easypiechart skill-circle" data-percent="86">
												<span class="percent head-font">86</span>
											</span>
                                        </div>
                                        <div class="col-sm-6 mb-15">
                                            <span class="skill-head mb-20">Journal Entries</span>
											<span id="pie_chart_2" class="easypiechart skill-circle" data-percent="46">
												<span class="percent head-font"><?php echo count($count_journal_entries);?></span>
											</span>
                                        </div>
                                       <!-- <div class="col-sm-4 mb-15">
                                            <span class="skill-head mb-20">Nightlife</span>
											<span id="pie_chart_3" class="easypiechart skill-circle" data-percent="90">
												<span class="percent head-font">90</span>
											</span>
                                        </div>-->
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6 mb-15">
                                            <span class="skill-head mb-20">Account Control</span>
											<span id="pie_chart_3" class="easypiechart skill-circle" data-percent="<?php echo count($count_gl_account);?>">
												<span class="percent head-font"><?php echo count($count_gl_account);?></span>
											</span>
                                        </div>
                                        <div class="col-sm-6 mb-15">
                                            <span class="skill-head mb-20">Account Type</span>
											<span id="pie_chart_4" class="easypiechart skill-circle" data-percent="<?php echo count($count_ac_type);?>">
												<span class="percent head-font"><?php echo count($count_ac_type);?></span>
											</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-12">
                        <div class="panel panel-default card-view">
                            <div class="panel-heading">
                                <div class="pull-left">
                                    <h6 class="panel-title txt-dark">Sales Statistics</h6>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="panel-wrapper collapse in">
                                <div class="panel-body pb-0">
                                    <div class="row">
                                        <div class="col-sm-4 mb-15">
                                            <span class="skill-head mb-20">Users</span>
											<span id="pie_chart_7" class="easypiechart skill-circle skill-circle-fill" data-percent="<?php echo count($count_user);?>">
												<span class="percent head-font"><?php echo count($count_user);?></span>
											</span>
                                        </div>
                                        <div class="col-sm-4 mb-15">
                                            <span class="skill-head mb-20">Roles</span>
											<span id="pie_chart_8" class="easypiechart skill-circle skill-circle-fill" data-percent="<?php echo count($count_roles);?>">
												<span class="percent head-font"><?php echo count($count_roles);?></span>
											</span>
                                        </div>
                                        <div class="col-sm-4 mb-15">
                                            <span class="skill-head mb-20">Department</span>
											<span id="pie_chart_9" class="easypiechart skill-circle skill-circle-fill" data-percent="<?php echo count($count_department);?>">
												<span class="percent head-font"><?php echo count($count_department);?></span>
											</span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4 mb-15">
                                            <span class="skill-head mb-20">Warehouses</span>
											<span id="pie_chart_10" class="easypiechart skill-circle skill-circle-fill" data-percent="<?php echo count($count_warehouses);?>">
												<span class="percent head-font"><?php echo count($count_warehouses);?></span>
											</span>
                                        </div>
                                        <div class="col-sm-4 mb-15">
                                            <span class="skill-head mb-20">Purchase Invoices</span>
											<span id="pie_chart_11" class="easypiechart skill-circle skill-circle-fill" data-percent="<?php echo count($count_purchase_invoices);?>">
												<span class="percent head-font"><?php echo count($count_purchase_invoices);?></span>
											</span>
                                        </div>
                                        <div class="col-sm-4 mb-15">
                                            <span class="skill-head mb-20">Purchase Order</span>
											<span id="pie_chart_12" class="easypiechart skill-circle skill-circle-fill" data-percent="<?php echo count($count_purchase_order);?>">
												<span class="percent head-font"><?php echo count($count_purchase_order);?></span>
											</span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4 mb-15">
                                            <span class="skill-head mb-20">Product</span>
											<span id="pie_chart_8" class="easypiechart skill-circle skill-circle-fill" data-percent="<?php echo count($count_product);?>">
												<span class="percent head-font"><?php echo count($count_product);?></span>
											</span>
                                        </div>
                                        <div class="col-sm-4 mb-15">
                                            <span class="skill-head mb-20">Supplier</span>
											<span id="pie_chart_6" class="easypiechart skill-circle skill-circle-fill" data-percent="<?php echo count($count_supplier);?>">
												<span class="percent head-font"><?php echo count($count_supplier);?></span>
											</span>
                                        </div>
                                        <div class="col-sm-4 mb-15">
                                            <span class="skill-head mb-20">Invoices</span>
											<span id="pie_chart_5" class="easypiechart skill-circle skill-circle-fill" data-percent="<?php echo count($count_getInvoice);?>">
												<span class="percent head-font"><?php echo count($count_getInvoice);?></span>
											</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2 col-sm-12">
                        <div class="panel panel-default card-view">
                            <div class="panel-heading">
                                <div class="pull-left">
                                    <h6 class="panel-title txt-dark">User Detail</h6>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="panel-wrapper collapse in">
                                <div class="panel-body pb-0">
                                    <div class="row">
                                        <span class="skill-head mb-20">User Name</span>
                                        <span class="skill-head mb-20"><h4><?= $this->session->userdata('fullname'); ?></h4></span>
                                    </div>
                                    <div class="row">
                                        <span class="skill-head mb-20">User Type</span>
                                        <span class="skill-head mb-20"><h4><?= $this->session->userdata('role_name'); ?></h4></span>
                                    </div>
                                        <div class="row">
                                    <?php if ($this->session->userdata('role_id') != '0'): ?>
                                        <span class="skill-head mb-20">Admin Name</span>
                                        <span class="skill-head mb-20"><h4><?= $this->session->userdata('admin_name'); ?></h4></span>
                                    <?php endif ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /Row -->


