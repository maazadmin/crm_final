 <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public_html/Booking Invoice_files/style.css">


<!-- saved from url=(0069)http://skytechdemo.com/skynet_final/Booking/invoice_detail/ereq34rfdf -->
<html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    
    
    <title>Invoice</title>
    
    <link rel="stylesheet" type="text/css" href="./Booking Invoice_files/style.css">
    


</head>

<body>
<style type="text/css">
.btn_back
{
    
    border-width: 4px;
    font-size: 13px;
    padding: 4px 9px;
    line-height: 1.39;
    float: left;
    border-width: 4px;
    font-size: 13px;
    padding: 4px 9px;
    line-height: 1.39;
    background-color: #abbac3!important;
    border-color: #abbac3;
    color: #fff;
    /* margin-top: 10px; */
    padding: 10px;
    cursor: pointer;
    text-decoration:none !important;
}
.btn_print
{
    border-width: 4px;
    font-size: 13px;
    padding: 4px 9px;
    line-height: 1.39;
    float: right;
    border-width: 4px;
    font-size: 13px;
    padding: 4px 9px;
    line-height: 1.39;
       background-color: #87b87f!important;
    border-color: #87b87f;
    color: #fff;
    /* margin-top: 10px; */
    padding: 10px;
    cursor: pointer;
    text-decoration:none !important;
}

</style>
<div style="margin:10px 15px 0px 15px;">
<a href="<?= base_url('Deal'); ?>" class="btn_back" name="" type="submit">
                        <i class="icon-ok bigger-110"></i>
                        Back
                    </a>
                    
                    <a class="btn_print" onclick="printDiv(&#39;page-wrap&#39;)" name="" type="submit">
                        <i class="icon-ok bigger-110"></i>
                        Print
                    </a>
                    <div style="clear:both;"></div>
</div>

    <div id="page-wrap">

        <textarea id="header" style="display:none;">INVOICE</textarea>
        
        <div id="identity" style="width: 100%;height: 100px !important;">
    
<div style="clear:both"></div>  
        
        </div>
        
        
        
        <div id="customer">

            <!--<textarea id="customer-title">Widget Corp.
c/o Steve Widget</textarea>-->
<div id="printDiv">
<table id="meta" style="float:left;">
                <tbody>
                <!-- <tr style="">
                    <td style="text-align: center;" colspan="2">SHIPPER</td>
                </tr> -->
                <tr>
                    <td class="meta-head">First Name</td>
                    <td align="left"><div style="text-align: left;" class="due"><?= $items[0]['first_name']; ?></div></td>
                </tr>
                <tr>

                    <td class="meta-head">Middle Initial</td>
                    <td align="left"><div style="text-align: left;" class="due"><?= $items[0]['middle_initial']; ?></div></td>
                </tr>
                <tr>
                    <td class="meta-head">Last Name</td>
                    <td align="left"><div style="text-align: left;" class="due"><?= $items[0]['last_name']; ?></div></td>
                </tr>
                <tr>
                    <td class="meta-head">Street Address</td>
                    <td align="left"><div style="text-align: left;" class="due"><?= $items[0]['street_address']; ?></div></td>
                </tr>
                <tr>
                    <td class="meta-head">Zip Code</td>
                    <td align="left"><div style="text-align: left;" class="due"><?= $items[0]['zip_code']; ?></div></td>
                </tr>
                <tr>
                    <td class="meta-head">State Provice Code</td>
                    <td align="left"><div style="text-align: left;" class="due"><?= $items[0]['state_province_code']; ?></div></td>
                </tr>
                <tr>
                    <td class="meta-head">Country</td>
                    <td align="left"><div style="text-align: left;" class="due"><?= $items[0]['country']; ?></div></td>
                </tr>
                <tr>
                    <td class="meta-head">Home Phone</td>
                    <td align="left"><div style="text-align: left;" class="due"><?= $items[0]['home_phone']; ?></div></td>
                </tr>
                <tr>
                    <td class="meta-head">Work Phone</td>
                    <td align="left"><div style="text-align: left;" class="due"><?= $items[0]['work_phone']; ?></div></td>
                </tr>
                <tr>
                    <td class="meta-head">Mobile Phone</td>
                    <td align="left"><div style="text-align: left;" class="due"><?= $items[0]['mobile_phone']; ?></div></td>
                </tr>
                <tr>
                    <td class="meta-head">Email Address</td>
                    <td align="left"><div style="text-align: left;" class="due"><?= $items[0]['email_address']; ?></div></td>
                </tr>
            </tbody></table>
            <table id="meta">
                <tbody>
                <!-- <tr>
                    <td style="text-align: center;" colspan="2">CONSIGNEE</td>
                </tr> -->
                
                <tr>
                    <td class="meta-head">Product</td>
                    <td colspan="3" align="left"><div style="text-align: left;" class="due"><?= $items[0]['payment_product']; ?></div></td>
                </tr>
                <tr>
                    <td class="meta-head">Mode Of Payment</td>
                    <td colspan="3" align="left"><div style="text-align: left;" class="due">
                        <?php if($items[0]['payment_mode'] == '1')  {echo 'Cod';} ?>
                        <?php if($items[0]['payment_mode'] == '2')  {echo 'CC';} ?>
                        <?php if($items[0]['payment_mode'] == '3')  {echo 'Package Price';} ?>
                        <?php if($items[0]['payment_mode'] == '4')  {echo 'Online';} ?>
                        <?php if($items[0]['payment_mode'] == '5')  {echo 'Check';} ?>
                    </div></td>
                </tr>
                <tr>
                    <td class="meta-head">Package Price</td>
                    <td colspan="3" align="left"><div style="text-align: left;" class="due"><?= $items[0]['package_price']; ?></div></td>
                </tr>
                <tr>
                    <td class="meta-head">Special Instruction</td>
                    <td colspan="3" align="left"><div style="text-align: left;" class="due"><?= $items[0]['special_instruction']; ?></div></td>
                </tr>
                <tr>
                    <td class="meta-head">Billing Address</td>
                    <td colspan="3" align="left"><div style="text-align: left;" class="due"><?= $items[0]['billing_address']; ?></div></td>
                </tr>
                <tr>
                    <td class="meta-head">Amount Total</td>
                    <td colspan="3" align="left"><div style="text-align: left;" class="due"><?= $total_charged_amount; ?></div></td>
                </tr>
                <?php

                  if(!empty($option_detail)){
                  foreach ($option_detail as $optionvalue) {
                    $total_charged_amount += $optionvalue['annual_intrest_amount'];
                  ?>

                
                 <tr>
                    <td class="meta-head">Payment Option</td>
                    <td align="left"><div style="text-align: left;" class="due"><?= $optionvalue['account_number'] ?></div></td>
                    <td align="left"><div style="text-align: left;" class="due"><?= $optionvalue['exp_date'] ?></div></td>
                    <td align="left"><div style="text-align: left;" class="due"><?= $optionvalue['cvc'] ?></div></td>
                </tr>
                
                <tr>
                    <td class="meta-head">Charge Amount</td>
                    <td colspan="3" align="left"><div style="text-align: left;" class="due"><?= $optionvalue['annual_intrest_amount']; ?></div></td>
                </tr>

                <?php
                  }
                 }
                ?>

                <tr>
                    <td class="meta-head">Transaction ID</td>
                    <td colspan="3" align="left"><div style="text-align: left;" class="due"><?= $items[0]['transaction_id']; ?></div></td>
                </tr>
                <tr>
                    <td class="meta-head">Descriptor</td>
                    <td colspan="3" align="left"><div style="text-align: left;" class="due"><?= $items[0]['descriptor']; ?></div></td>
                </tr>
                <tr>
                    <td class="meta-head">Date And Time</td>
                    <td colspan="3" align="left"><div style="text-align: left;" class="due"><?= $items[0]['dateandtime']; ?></div></td>
                </tr>

            </tbody></table>
        <div style="clear:both"></div>  
        <div class="mt-90 row">
            <div class="mt-90 col-md-12">
                 <p>I authorize the amount of $_____________ to the credit card provided herein. I agree to pay for this service/product in accordance with the issuing bank </p>
            </div>
        </div>

         <div class="mt-90 row">
            <div class="mt-50 col-md-12">
                <div class="col-md-1">
                 <input type="checkbox" name="">
                </div>
                <div class="col-md-11 text-left">
                    <p>By signing this form I agree with the terms & conditions </p>
                </div>
            </div>
        </div>

        <div class="mt-90 row">
            <div class="mt-50 col-md-12">
                <div class="col-md-6">
                    <div class="row">
                        <label style="margin-top: 10px" class="control-label mb-10" for="exampleInputuname_1">Fullname&nbsp;: ______________________________</label>
                    </div>
                    <div class="row">
                        <label style="margin-top: 10px" class="control-label mb-10" for="exampleInputuname_1">Signature&nbsp;: ______________________________</label>
                    </div>
                    <div class="row">
                        <label style="margin-top: 10px" class="control-label mb-10" for="exampleInputuname_1">Date&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: ______________________________</label>
                    </div>
                </div>
                <div class="col-md-6"></div>
            </div>
        </div>
        </div>  
        </div>
        
        
        
            
    
    </div>
    
    <script type="text/javascript">
    function printDiv(divName) {
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;

            document.body.innerHTML = printContents;

            window.print();

            document.body.innerHTML = originalContents;
        }
</script>



</body></html> 


                      