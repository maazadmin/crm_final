<!-- Main Content -->
<div class="page-wrapper">
    <div class="container-fluid">

        <!-- Title -->
        <div class="row heading-bg">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h5 class="txt-dark"><?= $heading?></h5>
            </div>

            <!-- Breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="index.html">Dashboard</a></li>
                    <li><a href="active"><span><?= $heading?></span></a></li>

                </ol>
            </div>
            <!-- /Breadcrumb -->

        </div>
        <!-- /Title -->

        <!-- Row -->
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default card-view">
                    <div class="panel-heading">
                        <div class="pull-left">
                            <h6 class="panel-title txt-dark"><?php echo $title; ?></h6>
                        </div>
                        <div class="pull-right">
                            <a href="<?php echo base_url('job'); ?>" class="btn btn-success btn-sm">View List</a>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body">
                            <p class="text-muted"><?php echo $description; ?>.</p>

                            <div class="row">
                                <div class="col-sm-12 col-xs-12">
                                    <div class="form-wrap">
                                        <?php
                                        //Show message
                                        echo $this->session->flashdata("msg");
                                        ?>
                                        <form  method="post" enctype="multipart/form-data"
                                               action="<?= site_url($this->uri->segment(1) . '/add') ?>" data-toggle="validator" role="form">
                                            <div class="form-group col-md-4">
                                                <label class="control-label mb-10"
                                                       for="exampleInputuname_1">Job Ref#</label>

                                                <input type="text" class="form-control" name="job_ref"
                                                       placeholder="Job Ref#" required>
                                            </div>

                                            <div class="form-group col-md-4">
                                                <label class="control-label mb-10"
                                                       for="exampleInputEmail_1">Date</label>

                                                <input type="date" class="form-control" name="date"
                                                       placeholder="Date">
                                            </div>

                                            <div class="form-group col-md-4">
                                                <label class="control-label mb-10"
                                                       for="exampleInputEmail_1">Description</label>

                                                <textarea class="form-control" placeholder="Description" name="description"></textarea>
                                            </div>

                                            <div class="form-group col-md-4">
                                                <label class="control-label mb-10"
                                                       for="exampleInputEmail_1">Short Description</label>

                                                <textarea class="form-control" placeholder="Short Description" name="short_description"></textarea>
                                            </div>

                                            <div class="form-group col-md-4">
                                                <label class="control-label mb-10"
                                                       for="exampleInputEmail_1">Note Invoice On Print</label>
                                                <textarea class="form-control" placeholder="Note Invoice On Print" name="note_invoice"></textarea>
                                            </div>

                                            <div class="form-group col-md-4">
                                                <label class="control-label mb-10"
                                                       for="exampleInputEmail_1">Remarks</label>
                                                <textarea class="form-control" placeholder="Remarks" name="remarks"></textarea>
                                            </div>

                                            <div class="form-group col-md-4">
                                                <label class="control-label mb-10"
                                                       for="exampleInputEmail_1">Bare @US$ Per Sqm</label>

                                                <input type="number" class="form-control" name="bare"
                                                       placeholder="Bare @US$ Per Sqm">
                                            </div>

                                            <div class="form-group col-md-4">
                                                <label class="control-label mb-10"
                                                       for="exampleInputEmail_1">Shell @US$ Per Sqm</label>

                                                <input type="number" class="form-control" name="shell"
                                                       placeholder="Shell @US$ Per Sqm">
                                            </div>

                                            <div class="form-group col-md-4">
                                                <label class="control-label mb-10"
                                                       for="exampleInputEmail_1">US$ PKR</label>

                                                <input type="number" class="form-control" name="us_pkr"
                                                       placeholder="US$ PKR">
                                            </div>

                                            <div class="form-group col-md-4">
                                                <label class="control-label mb-10"
                                                       for="exampleInputEmail_1">Held On</label>

                                                <input type="date" class="form-control" name="held_on"
                                                       placeholder="Held On">
                                            </div>

                                            <div class="form-group col-md-4">
                                                <label class="control-label mb-10"
                                                       for="exampleInputEmail_1">Status</label>

                                               <select class="form-control" name="status">
                                                   <option>Open</option>
                                               </select>
                                            </div>

                                            <div class="form-group col-md-10">


                                                <button type="submit" class="btn btn-success mr-10" name="btn_submit">
                                                    Submit
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- /Row -->



    <!-- /Main Content -->