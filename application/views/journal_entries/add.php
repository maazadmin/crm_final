<!-- Main Content -->
<div class="page-wrapper">
    <div class="container-fluid">

        <!-- Title -->
        <div class="row heading-bg">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h5 class="txt-dark"><?= $heading ?></h5>
            </div>

            <!-- Breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="index.html">Dashboard</a></li>
                    <li><a href="active"><span><?= $heading ?></span></a></li>

                </ol>
            </div>
            <!-- /Breadcrumb -->

        </div>
        <!-- /Title -->

        <!-- Row -->
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default card-view">
                    <div class="panel-heading">
                        <div class="pull-left">
                            <h6 class="panel-title txt-dark"><?php echo $title; ?></h6>
                        </div>
                        <div class="pull-right">
                            <a href="<?= site_url($this->uri->segment(1)) ?>" class="btn btn-success btn-sm">View
                                List</a>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body">
                            <p class="text-muted"><?php echo $description; ?>.</p>

                            <div class="row">
                                <div class="col-sm-12 col-xs-12">
                                    <div class="form-wrap">
                                        <?php
                                        //Show message
                                        echo $this->session->flashdata("msg");
                                        ?>
                                        <form method="post" class="ac-type-form" enctype="multipart/form-data"
                                              action="<?= site_url($this->uri->segment(1) . '/add') ?>"
                                              data-toggle="validator" role="form">
                                            <div class="row">
                                                <div class="form-group col-md-4">
                                                    <label class="control-label mb-10" for="exampleInputuname_1">Entry Date</label>

                                                    <input type="date" class="form-control"
                                                           name="entry_date" placeholder="Account Type" required>
                                                </div>

                                                <div class="form-group col-md-4">
                                                    <label class="control-label mb-10" for="exampleInputuname_1">Reference</label>

                                                    <input type="text" class="form-control"
                                                           name="reference" placeholder="Reference" >
                                                </div>

                                                <div class="form-group col-md-4">
                                                    <label class="control-label mb-10" for="exampleInputuname_1">Narration</label>

                                                    <textarea class="form-control" name="narration"></textarea>
                                                </div>
                                            </div>
                                            
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <table class="table table-responsive">
                                                        <tr>
                                                            <th>Account</th>
                                                            <th>Debit</th>
                                                            <th>Credit</th>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <select class="form-control  select2-select-00" name="account1">
                                                                    <option value="">Select Account</option>
                                                                    <?php foreach($ac_list as $row): ?>
                                                                    <option value="<?=$row['gl_account_id']?>"><?= $row['gl_account_name']?></option>
                                                                    <?php endforeach; ?>
                                                                </select>
                                                                </select>
                                                            </td>
                                                            <td>
                                                                <input type="number" value="0" class="form-control"name="debit_amount1" id="debit_amount1" min="0">
                                                            </td>
                                                            <td>
                                                                <input type="number" value="0" class="form-control"name="credit_amount1" id="credit_amount1" min="0" >

                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <select class="form-control select2-select-00" name="account2">
                                                                    <option value="">Select Account</option>
                                                                    <?php foreach($ac_list as $row): ?>
                                                                        <option value="<?=$row['gl_account_id']?>"><?= $row['gl_account_name']?></option>
                                                                    <?php endforeach; ?>
                                                                </select>
                                                            </td>
                                                            <td>
                                                                <input type="number" value="0" class="form-control" name="debit_amount2" id="debit_amount2" min="0">
                                                            </td>
                                                            <td>
                                                                <input type="number" value="0" class="form-control" name="credit_amount2" id="credit_amount2" min="0">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>


                                            <div class="form-group col-md-10">


                                                <button type="submit" class="btn btn-success mr-10" name="btn_submit">
                                                    Submit
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
                <!-- /Row -->

            </div>
            <!-- /Main Content -->
            <!-- <script>
                 //Form Validation A/C Type

                 $('.ac-type-form').validate(
                     {

                         errorElement: 'div',
                         errorClass: 'help-block',
                         focusInvalid: false,
                         rules: {
                             //Personal Info

                             account_type: {required: true},

                         },

                         messages: {

                             account_type: " Please enter Account Type",


                         },
                         invalidHandler: function (event, validator) { //display error alert on form submit
                             $('.alert-danger', $('.add_new_ajeer')).show();
                         },

                         highlight: function (e) {
                             $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
                         },

                         success: function (e) {
                             $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
                             $(e).remove();
                         },
                         errorPlacement: function (error, element) {
                             error.insertAfter(element.parent());
                         },

                     });
             </script>-->