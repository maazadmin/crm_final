   <script type="text/javascript">
    $(document).ready(function() {
        $('#product-drop').multiselect({
            enableFiltering: true,
            includeSelectAllOption: true,
            maxHeight: 400,
            minWidth:400,
            dropUp: true
        });
    });
     $(document).ready(function() {
        $('#campaign-drop').multiselect({
            enableFiltering: true,
            includeSelectAllOption: true,
            maxHeight: 400,
            dropUp: true
        });
    });
       $(document).ready(function() {
        $('#shift-drop').multiselect({
            enableFiltering: true,
            includeSelectAllOption: true,
            maxHeight: 400,
            dropUp: true
        });
    });
    
</script>
<script>

    //PrintDiv
    function printDiv(divName) {

        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;

        document.body.innerHTML = printContents;

        window.print();

        document.body.innerHTML = originalContents;
        window.close();
    }


    $(document).ready(function () {

        //Account Type Table
        var table = $('#ac_type_table').DataTable({
            responsive: true
        });

        //Gl Account Table
        var table = $('#gl_account_table').DataTable({
            responsive: true
        });
        //Customers Table
        var table = $('#customers_table').DataTable({
            responsive: true
        });
        //Job Order Table
        var table = $('#job_order_table').DataTable({
            responsive: true
        });

        //Suppliers Table
        var table = $('#suppliers_table').DataTable({
            responsive: true
        });
        //Product Table
        var table = $('#product_table').DataTable({
            responsive: true
        });

        //Purchase Invoice Table
        var table = $('#purchase_invoice_table').DataTable({
            responsive: true
        });
        //
        // new $.fn.dataTable.FixedHeader(table);

    });


    $('.qty_new').keyup(function () {
        var qty = $(this).val();
        var item_id = $(this).attr('data_id');
        var unit_price = $('#unit_price_new_' + item_id).val();
        $('#amount_new_' + item_id).val(qty * unit_price);
    });

    //Calculate Purchase Item
    $('.unit_price_new').keyup(function () {
        var unit_price = $(this).val();
        var item_id = $(this).attr('data_id');
        var qty = $('#qty_new_' + item_id).val();
        $('#amount_new_' + item_id).val(qty * unit_price);
    });


    //Calculate Purchase Item
    $('#qty').keyup(function () {
        var qty = $(this).val();
        var unit_price = $('#unit_price').val();
        $('#amount').val(qty * unit_price);
    });

    //Calculate Purchase Item
    $('#unit_price').keyup(function () {
        var unit_price = $(this).val();
        var qty = $('#qty').val();
        $('#amount').val(qty * unit_price);
    });


    // ADD Mutiple Values Product
    $('#insert').click(function () {


        $("#ccinfo").removeClass("hidden");
        $("#ccinfo input[type=text] input[type=checkbox]").addClass("form-group");
        $("#cctable").css("table-layout", "fixed").width("100%");

        var product_id = $('#product_id').val();
        var p_name = $('#p_name').val();
        var description = $('#description').val();
        var unit_price = $('#unit_price').val();
        var qty = $('#qty').val();
        var amount = $('#amount').val();

        var markup = "<tr><td><input type='checkbox'  class='record' name='record' data_amount='" + amount + "'></td><td><input type='hidden' class='form-control'  name='product_id_new[]' value='" + product_id + "' ><input type='hidden'   name='check_product_id_new' value='" + product_id + "' ><input type='text' class='form-control' readonly name='product_name_new[]' value='" + p_name + "'></td><td><input type='text' class='form-control' readonly name='desc_new[]' value='" + description + "'></td><td><input type='text' class='form-control' readonly name='unit_price_new[]' value='" + unit_price + "'></td><td><input type='text' class='form-control' readonly name='qty_new[]' value='" + qty + "'></td><td><input type='text' class='form-control' readonly name='amount_new[]' value='" + amount + "'></td></tr>";


        $("#cctable tbody").append(markup);

        var subtotal = Number($('#total_amount').val()) + Number(amount);
        $('#total_amount').val(subtotal);

        $('#product_id').val("");
        $('#p_name').val("");
        $('#description').val("");
        $('#unit_price').val("");
        $('#qty').val("1");
        $('#amount').val("");

    });

    // Find and remove selected table rows
    $(".delete-row").click(function () {
        $("#cctable tbody").find('input[name="record"]').each(function () {
            if ($(this).is(":checked")) {
                var item_id = $(this).attr('data_id');
                var amount = $(this).attr('data_amount');
                var r = confirm("Are You Sure !");
                if (r == true) {
                    //alert('Yes');
                    var json = {};
                    json['item_id'] = item_id;
                    var request = $.ajax({
                        url: "<?php echo base_url('Purchase_orders/delete_purchase_item'); ?>",
                        type: "POST",
                        data: json,
                        dataType: "json",

                        success: function (_return) {

                        }
                    });

                }

                var subtotal = Number($('#total_amount').val()) - Number(amount);
                $('#total_amount').val(subtotal);
                $(this).parents("tr").remove();

            }
        });


    });

//check email exists
    $('#email').blur(function(){


            var json = {};
            json['email'] = $(this).val();
          $.ajax(
                {
                    url: "<?php echo base_url('Users/email_exists'); ?>",
                    type: "POST",
                    data: json,
                    dataType: "json",

                    success: function (_return) {
                       
                       if(_return == 1){


                            $('.email_result').html('Email Already Exists');
                             $('#email').val(' ');
                          
                    
                       }
                       else{
                        $('.email_result').html('');
                       }
                      

                       

                      

                    }


    });
            });

// $('.myform :campaign_chk').change(function() {
//     // this will contain a reference to the checkbox   
//     if (this.checked) {
//        alert('checked');
//     } else {
//         // the checkbox is now no longer checked
//     }
// });

$('#campaign_chk').change(function(){
            if($(this).prop("checked")){

                $('#campaign_id').attr("disabled", true);

    }
    else{

                $('#campaign_id').attr("disabled", false);


    }

});
$('#product_chk').change(function(){

    if($(this).prop("checked")){

                $('#product_id').attr("disabled", true);

    }
    else{

                $('#product_id').attr("disabled", false);


    }

            

});

    //check email exists
    $('#username').blur(function(){


            var json = {};
            json['username'] = $(this).val();
          $.ajax(
                {
                    url: "<?php echo base_url('Users/username_exists'); ?>",
                    type: "POST",
                    data: json,
                    dataType: "json",

                    success: function (_return) {
                       
                       if(_return == 1){


                            $('.username_result').html('Username Already Exists');
                             $('#username').val(' ');
                       //      if($('#username').val(' ')){
                       //    $('.username_result').html('');
                       // }

                       }
                       else{
                        $('.username_result').html('');
                       }
                      

                       

                      

                    }


    });
            });

    //Get Product Name BY ID
    $('#product_id').change(function () {

        var valid = false;

        $('input[name=check_product_id_new]').each(function (event) {
            if ($(this).val() == $('#product_id').val()) {
                valid = true;
            }
        });
        if (valid == true) {
            //alert('Error');
            $('#product_id').val("");
            $.toast({
                heading: 'Product Already Exists!',
                text: 'Product Already Exists For this Product!',
                showHideTransition: 'fade',
                icon: 'error'
            })

        }
        else {

            var json = {};
            json['product_id'] = $(this).val();
            var request = $.ajax(
                {
                    url: "<?php echo base_url('Purchase_orders/get_product_id'); ?>",
                    type: "POST",
                    data: json,
                    dataType: "json",

                    success: function (_return) {
                        $('#p_name').val(_return['product_name']);
                        $('#unit_price').val(_return['product_price']);
                        //$(".city_get").change();

                    }

                });

        }
    });


    // ADD Mutiple Values For Invoice
    $('#insert_invoice').click(function () {


        $("#ccinfo").removeClass("hidden");
        $("#ccinfo input[type=text] input[type=checkbox]").addClass("form-group");
        $("#cctable").css("table-layout", "fixed").width("100%");

        var product_id = $('#product_id').val();
        var p_name = $('#p_name').val();
        var description = $('#description').val();
        var unit_price = $('#unit_price').val();
        var qty = $('#qty').val();
        var amount = $('#amount').val();

        var markup = "<tr><td><input type='checkbox'  class='record' name='record' data_amount='" + amount + "'></td><td><input type='hidden' class='form-control'  name='product_id_new[]' value='" + product_id + "' ><input type='hidden'   name='check_product_id_new' value='" + product_id + "' ><input type='text' class='form-control' readonly name='product_name_new[]' value='" + p_name + "'></td><td><input type='text' class='form-control' readonly name='desc_new[]' value='" + description + "'></td><td><input type='text' class='form-control' readonly name='unit_price_new[]' value='" + unit_price + "'></td><td><input type='text' class='form-control' readonly name='qty_new[]' value='" + qty + "'></td><td><input type='text' class='form-control' readonly name='amount_new[]' value='" + amount + "'></td></tr>";


        $("#cctable tbody").append(markup);

        var subtotal = Number($('#total_amount').val()) + Number(amount);
        $('#total_amount').val(subtotal);

        $('#product_id').val("");
        $('#p_name').val("");
        $('#description').val("");
        $('#unit_price').val("");
        $('#qty').val("1");
        $('#amount').val("");

    });

    //Remove Invoice Item
    // Find and remove selected table rows
    $(".delete-row-invoice").click(function () {
        $("#cctable tbody").find('input[name="record"]').each(function () {
            if ($(this).is(":checked")) {
                $(this).parents("tr").remove();
            }
        });


    });


    //Get Supplier Name BY ID
    $('#supplier_id').change(function () {

        var valid = false;

        $('input[name=check_supplier_new]').each(function (event) {
            if ($(this).val() == $('#supplier_id').val()) {
                valid = true;
            }
        });
        if (valid == true) {
            //alert('Error');
            $('#supplier_id').val("");
            $.toast({
                heading: 'Supplier Already Exists!',
                text: 'Supplier Already Exists For this Supplier!',
                showHideTransition: 'fade',
                icon: 'error'
            })

        }
        else {

            var json = {};
            json['supplier_id'] = $(this).val();
            var request = $.ajax(
                {
                    url: "<?php echo base_url('Products/get_supplier_id'); ?>",
                    type: "POST",
                    data: json,
                    dataType: "json",

                    success: function (_return) {
                        $('#supplier_name').val(_return['title']);

                    }

                });

        }
    });


    // ADD Mutiple Values Supplier For Products
    $('#insert_supplier').click(function () {


        $("#ccinfo_supplier").removeClass("hidden");
        $("#ccinfo_supplier input[type=text] input[type=checkbox]").addClass("form-group");
        $("#supplier_table").css("table-layout", "fixed").width("100%");

        var supplier_id = $('#supplier_id').val();
        var supplier_name = $('#supplier_name').val();
        var supplier_price = $('#supplier_price').val();


        var markup = "<tr><td><input type='checkbox'  class='record' name='record' data_amount='" + supplier_price + "'></td><td><input type='hidden' class='form-control'  name='supplier_id_new[]' value='" + supplier_id + "' ><input type='hidden'   name='check_supplier_new' value='" + supplier_id + "' ><input type='text' class='form-control' readonly name='supplier_name_new[]' value='" + supplier_name + "'></td><td><input type='text' class='form-control' readonly name='supplier_price_new[]' value='" + supplier_price + "'></td><tr>";


        $("#supplier_table tbody").append(markup);

        $('#supplier_id').val("");
        $('#supplier_name').val("");
        $('#supplier_price').val("0");

    });

    //Supplier Table Row
    // Find and remove selected table rows
    $(".delete-row-supplier").click(function () {
        $("#supplier_table tbody").find('input[name="record"]').each(function () {
            if ($(this).is(":checked")) {
                var product_supplier_id = $(this).attr('data_id');
                var r = confirm("Are You Sure !");
                if (r == true) {
                    //alert('Yes');
                    var json = {};
                    json['product_supplier_id'] = product_supplier_id;
                    var request = $.ajax({
                        url: "<?php echo base_url('Products/delete_product_supplier'); ?>",
                        type: "POST",
                        data: json,
                        dataType: "json",

                        success: function (_return) {

                        }
                    });

                    $(this).parents("tr").remove();
                }


            }
        });


    });

    // Add Loan Info
    $('#insert_loan').click(function () {


        var select_loan_type = $('#select_loan_type').val();
        var contact = $('#contact').val();
        var start_date = $('#start_date').val();
        var end_date = $('#end_date').val();
        var min_payment = $('#min_payment').val();
        var last_payment = $('#last_payment').val();
        var next_payment = $('#next_payment').val();
        var annual_interest = $('#annual_interest').val();
        var paid_off_duration = $('#paid_off_duration').val();
        var cc1 = $('#cc1').val();
        var cc2 = $('#cc2').val();
        var cc3 = $('#cc3').val();
        var cc4 = $('#cc4').val();
        var cc = cc1+'-'+cc2+'-'+cc3+'-'+cc4;


        var lender_name = $('#lender_name').val();
        var account_number1 = $('#account_number1').val();
        var total_owe = $('#total_owe').val();
        var available_balance = $('#available_balance').val();
        var apr = $('#apr').val();
        var cvc1 = $('#cvc1').val();
        var month = $('#month').val();
        var year = $('#year').val();
        var exp = month+'/'+year;

        if (select_loan_type != "" && contact != "" && lender_name != "" && cc1 != "" && cc2 != "" && cc3 != "" && cc4 != "" && month != "" && year != "" && apr != "" && total_owe != "" && available_balance != "" && min_payment != "" && last_payment != "") 
        {
             $("#ccinfo_loan").removeClass("hidden");
        $("#ccinfo_loan input[type=text] input[type=checkbox]").addClass("form-group");
        $("#loan_table").css("table-layout", "fixed").width("100%");



        var markup = "<tr><td><input type='checkbox'  class='record' name='record' data_amount='" + account_number1 + "'></td><input type='hidden' class='form-control'  name='select_loan_type_new[]' value='" + select_loan_type + "' ><input type='hidden' class='form-control'  name='contact_new[]' value='" + contact + "' ><input type='hidden' class='form-control'  name='start_date_new[]' value='" + start_date + "' ><input type='hidden' class='form-control'  name='end_date_new[]' value='" + end_date + "' ><input type='hidden' class='form-control'  name='min_payment_new[]' value='" + min_payment + "' ><input type='hidden' class='form-control'  name='last_payment_new[]' value='" + last_payment + "' ><input type='hidden' class='form-control'  name='next_payment_new[]' value='" + next_payment + "' ><input type='hidden' class='form-control'  name='annual_interest_new[]' value='" + annual_interest + "' ><input type='hidden' class='form-control'  name='paid_off_duration_new[]' value='" + paid_off_duration + "' ><input type='hidden' class='form-control'  name='cc_new[]' value='" + cc + "' ><td><input type='text' class='form-control' readonly name='lender_name_new[]' value='" + lender_name + "'></td><td><input type='text' class='form-control' readonly name='account_number1_new[]' value='" + account_number1 + "'></td><td><input type='text' class='form-control' readonly name='exp_new[]' value='" + exp + "'></td><td><input type='text' class='form-control' readonly name='cvc1_new[]' value='" + cvc1 + "'></td><td><input type='text' class='form-control' readonly name='total_owe_new[]' value='" + total_owe + "'></td><td><input type='text' class='form-control' readonly name='available_balance_new[]' value='" + available_balance + "'></td><td><input type='text' class='form-control' readonly name='apr_new[]' value='" + apr + "'></td><tr>";


        $("#loan_table tbody").append(markup);

        $('#select_loan_type').val("");
        $('#contact').val("");
        $('#start_date').val("");
        $('#end_date').val("");
        $('#min_payment').val("");
        $('#last_payment').val("");
        $('#next_payment').val("");
        $('#annual_interest').val("");
        $('#paid_off_duration').val("");
        $('#cc1').val("");
        $('#cc2').val("");
        $('#cc3').val("");
        $('#cc4').val("");

        $('#lender_name').val("");
        $('#account_number1').val("");
        $('#total_owe').val("");
        $('#available_balance').val("");
        $('#apr').val("");
        $('#cvc1').val("");
        $('#month').val("");
        $('#year').val("");
        }else
        {
            $('#error_loan_div').removeClass('hidden');
            $('#error_loan').html('Please fill all required fields');
        }

       

    });



    //loan Table Row
    // Find and remove selected table rows
    $(".delete-row-loan").click(function () {
        $("#loan_table tbody").find('input[name="record"]').each(function () {
            if ($(this).is(":checked")) {
                    $(this).parents("tr").remove();
            }
        });


    });
    //Get Warehouse Name BY ID
    $('#warehouse_id').change(function () {

        var valid = false;

        $('input[name=check_warehouse_new]').each(function (event) {
            if ($(this).val() == $('#warehouse_id').val()) {
                valid = true;
            }
        });
        if (valid == true) {
            //alert('Error');
            $('#warehouse_id').val("");
            $.toast({
                heading: 'Warehouse Already Exists!',
                text: 'Warehouse Already Exists For this Warehouse!',
                showHideTransition: 'fade',
                icon: 'error'
            })

        }
        else {

            var json = {};
            json['warehouse_id'] = $(this).val();
            var request = $.ajax(
                {
                    url: "<?php echo base_url('Products/get_warehouse_id'); ?>",
                    type: "POST",
                    data: json,
                    dataType: "json",

                    success: function (_return) {
                        $('#warehouse_name').val(_return['warehouse_name']);

                    }

                });

        }
    });

    // ADD Mutiple Values Warehouse For Products
    $('#insert_warehouse').click(function () {

        $("#ccinfo_warehouse").removeClass("hidden");
        $("#ccinfo_warehouse input[type=text] input[type=checkbox]").addClass("form-group");
        $("#warehouse_table").css("table-layout", "fixed").width("100%");

        var warehouse_id = $('#warehouse_id').val();
        var warehouse_name = $('#warehouse_name').val();
        var warehouse_qty = $('#warehouse_qty').val();


        var markup = "<tr><td><input type='checkbox'  class='record' name='record' data_amount='" + warehouse_qty + "'></td><td><input type='hidden' class='form-control'  name='warehouse_id_new[]' value='" + warehouse_id + "' ><input type='hidden'   name='check_warehouse_new' value='" + warehouse_id + "' ><input type='text' class='form-control' readonly name='warehouse_name_new[]' value='" + warehouse_name + "'></td><td><input type='text' class='form-control' readonly name='warehouse_qty_new[]' value='" + warehouse_qty + "'></td><tr>";


        $("#warehouse_table tbody").append(markup);

        $('#warehouse_id').val("");
        $('#warehouse_name').val("");
        $('#warehouse_qty').val("1");

    });


    //Warehouse Table Row
    // Find and remove selected table rows
    $(".delete-row-warehouse").click(function () {
        $("#warehouse_table tbody").find('input[name="record"]').each(function () {
            if ($(this).is(":checked")) {

                var product_locations_id = $(this).attr('data_id');
                var r = confirm("Are You Sure !");
                if (r == true) {
                    //alert('Yes');
                    var json = {};
                    json['product_locations_id'] = product_locations_id;
                    var request = $.ajax({
                        url: "<?php echo base_url('Products/delete_product_location'); ?>",
                        type: "POST",
                        data: json,
                        dataType: "json",

                        success: function (_return) {

                        }
                    });

                    $(this).parents("tr").remove();
                }

            }
        });

    });

    //Get Product Name BY ID
    $('#role_id').change(function () {

        var valid = false;

        $('input[name=check_role_id_new]').each(function (event) {
            if ($(this).val() == $('#role_id').val()) {
                valid = true;
            }
        });
        if (valid == true) {
            //alert('Error');
            $('#role_id').val("");
            $.toast({
                heading: 'Role Already Exists!',
                text: 'Role Already Exists For this Role!',
                showHideTransition: 'fade',
                icon: 'error'
            })

        }
        else {

            var json = {};
            json['role_id'] = $(this).val();
            var request = $.ajax(
                {
                    url: "<?php echo base_url('Users/get_role_id'); ?>",
                    type: "POST",
                    data: json,
                    dataType: "json",

                    success: function (_return) {
                        $('#role_name').val(_return['role_name']);

                    }

                });

        }
    });

    // ADD Mutiple Values Roles
    $('#insert_role').click(function () {


        $("#role-table").removeClass("hidden");
        $("#role-table input[type=text] input[type=checkbox]").addClass("form-group");
        $("#role-table").css("table-layout", "fixed").width("100%");

        var role_id = $('#role_id').val();
        var role_name = $('#role_name').val();

        var markup = "<tr><td><input type='checkbox'  class='record' name='record'  ></td><td><input type='text' readonly class='form-control'  name='role_id_new[]' value='" + role_id + "' ><input type='hidden'   name='check_role_id_new' value='" + role_id + "' ></td><td><input type='text' class='form-control' readonly name='role_name_new[]' value='" + role_name + "'></td></tr>";


        $("#role-table tbody").append(markup);

        $('#role_id').val("");
        $('#role_name').val("");

    });

    //Supplier Table Row
    // Find and remove selected table rows
    $(".delete-row-roles").click(function () {
        $("#role-table tbody").find('input[name="record"]').each(function () {
            if ($(this).is(":checked")) {

                var user_role_id = $(this).attr('data_id');
                var r = confirm("Are You Sure !");
                if (r == true) {
                    //alert('Yes');
                    var json = {};
                    json['user_role_id'] = user_role_id;
                    var request = $.ajax({
                        url: "<?php echo base_url('Users/delete_user_roles'); ?>",
                        type: "POST",
                        data: json,
                        dataType: "json",

                        success: function (_return) {

                        }
                    });

                    $(this).parents("tr").remove();
                }

            }
        });

    });


    //Get Navigation Name BY ID
    $('#nav_id').change(function () {

        var valid = false;

        $('input[name=check_nav_id_new]').each(function (event) {
            if ($(this).val() == $('#nav_id').val()) {
                valid = true;
            }
        });
        if (valid == true) {
            //alert('Error');
            $('#nav_id').val("");
            $.toast({
                heading: 'Module Already Exists!',
                text: 'Module Already Exists For this Module!',
                showHideTransition: 'fade',
                icon: 'error'
            })

        }
        else {

            var json = {};
            json['nav_id'] = $(this).val();
            var request = $.ajax(
                {
                    url: "<?php echo base_url('Roles/get_nav_id'); ?>",
                    type: "POST",
                    data: json,
                    dataType: "json",

                    success: function (_return) {
                        $('#nav_name').val(_return['nav_name']);

                    }

                });

        }
    });

    // ADD Mutiple Values Navigaiton/Module
    $('#insert_nav').click(function () {


        $("#nav-table").removeClass("hidden");
        $("#nav-table input[type=text] input[type=checkbox]").addClass("form-group");
        $("#nav-table").css("table-layout", "fixed").width("100%");

        var nav_id = $('#nav_id').val();
        var nav_name = $('#nav_name').val();

        var markup = "<tr><td><input type='checkbox'  class='record' name='record'  ></td><td><input type='text' readonly class='form-control'  name='nav_id_new[]' value='" + nav_id + "' ><input type='hidden'   name='check_nav_id_new' value='" + nav_id + "' ></td><td><input type='text' class='form-control' readonly name='nav_name_new[]' value='" + nav_name + "'></td></tr>";

        $("#nav-table tbody").append(markup);

        $('#nav_id').val("");
        $('#nav_name').val("");

    });

    // Find and remove selected table rows
    $(".delete-row-nav").click(function () {

        $("#nav-table tbody").find('input[name="record"]').each(function () {
            if ($(this).is(":checked")) {

                var role_navigation_id = $(this).attr('data_id');

                var r = confirm("Are You Sure !");
                if (r == true) {
                    //alert('Yes');
                    var json = {};
                    json['role_navigation_id'] = role_navigation_id;
                    var request = $.ajax({
                        url: "<?php echo base_url('Roles/delete_roles_nav'); ?>",
                        type: "POST",
                        data: json,
                        dataType: "json",

                        success: function (_return) {

                        }
                    });

                    $(this).parents("tr").remove();
                }

            }
        });

    });

    ///Disable Textbox Journal Entries
    //Check Value Empty Textbox
    $('#debit_amount1').change(function(){
        var value = $(this).val();
        if(value > 0){
            $('#debit_amount2').prop('disabled',true);
            $('#credit_amount1').prop('disabled',true);
        }else{
            $('#debit_amount2').prop('disabled',false);
            $('#credit_amount1').prop('disabled',false);
        }
    });
    //Check Value Empty Textbox
    $('#credit_amount1').change(function(){
        var value = $(this).val();
        if(value > 0){
            $('#debit_amount1').prop('disabled',true);
            $('#credit_amount2').prop('disabled',true);
        }else{
            $('#debit_amount1').prop('disabled',false);
            $('#credit_amount2').prop('disabled',false);
        }
    });

    //Check Value Empty Textbox
    $('#debit_amount2').change(function(){
        var value = $(this).val();
        if(value > 0){
            $('#credit_amount2').prop('disabled',true);
            $('#debit_amount1').prop('disabled',true);
        }else{
            $('#credit_amount2').prop('disabled',false);
            $('#debit_amount1').prop('disabled',false);
        }
    });

    //Check Value Empty Textbox
    $('#credit_amount2').change(function(){
        var value = $(this).val();
        if(value > 0){
            $('#debit_amount2').prop('disabled',true);
            $('#credit_amount1').prop('disabled',true);
        }else{
            $('#debit_amount2').prop('disabled',false);
            $('#credit_amount1').prop('disabled',false);
        }
    });

    //Check Checked Box Is Check

    $('#change_password').change(function(){
        if($(this).prop("checked") == true){
            $('#pass').prop('required',true);
            $('#confirm_pass').prop('required',true);
            $('#pass').prop('disabled',false);
            $('#confirm_pass').prop('disabled',false);
            $('#pass').val('');
            $('#confirm_pass').val('');

        }
        else{
            $('#pass').prop('required',false);
            $('#confirm_pass').prop('required',false);
            $('#pass').prop('disabled',true);
            $('#confirm_pass').prop('disabled',true);
            $('#pass').val('');
            $('#confirm_pass').val('');

        }
    });

      $('.datetimepicker2').datetimepicker({
            format: 'LT',
            useCurrent: false,
            icons: {
                    time: "fa fa-clock-o",
                    date: "fa fa-calendar",
                    up: "fa fa-arrow-up",
                    down: "fa fa-arrow-down"
                },
        });
      $('#datetimepicker3').datetimepicker({
            format: 'LT',
            useCurrent: false,
            icons: {
                    time: "fa fa-clock-o",
                    date: "fa fa-calendar",
                    up: "fa fa-arrow-up",
                    down: "fa fa-arrow-down"
                },
        });
        /* Datetimepicker Init*/
    $('.datetimepicker1').datetimepicker({
        format:'YYYY-MM-DD',

        useCurrent: false,
        icons: {
            time: "fa fa-clock-o",
            date: "fa fa-calendar",
            up: "fa fa-arrow-up",
            down: "fa fa-arrow-down"
        },
    }).on('dp.show', function() {
        if($(this).data("DateTimePicker").date() === null)
            $(this).data("DateTimePicker").date(moment());
    });


      $('.final_month').datetimepicker({
        format:'MM',

        useCurrent: false,
        icons: {
            time: "fa fa-clock-o",
            date: "fa fa-calendar",
            up: "fa fa-arrow-up",
            down: "fa fa-arrow-down"
        },
    }).on('dp.show', function() {
        if($(this).data("DateTimePicker").date() === null)
            $(this).data("DateTimePicker").date(moment());
    });
      $('.final_year').datetimepicker({
        format:'YYYY',

        useCurrent: false,
        icons: {
            time: "fa fa-clock-o",
            date: "fa fa-calendar",
            up: "fa fa-arrow-up",
            down: "fa fa-arrow-down"
        },
    }).on('dp.show', function() {
        if($(this).data("DateTimePicker").date() === null)
            $(this).data("DateTimePicker").date(moment());
    });
    $('.dateandtime').datetimepicker({
        format:'YYYY-MM-DD hh-mm-ss',

        useCurrent: false,
        icons: {
            time: "fa fa-clock-o",
            date: "fa fa-calendar",
            up: "fa fa-arrow-up",
            down: "fa fa-arrow-down"
        },
    }).on('dp.show', function() {
        if($(this).data("DateTimePicker").date() === null)
            $(this).data("DateTimePicker").date(moment());
    });



// var packageCounter = $("#package-master").find(".pkg-counter").text();
//  $('body').on('click','.package-adder',function(e){

//         e.preventDefault();

//         var pkgClone = $("#package-master").clone().attr('id', 'package-' + (++packageCounter) );

//         pkgClone.find(".pkg-counter").text(packageCounter);

//         $("#pkg-count").val(packageCounter);

//         pkgClone.find("input").val("");

//         pkgClone.find('textarea').val('');
        
//         pkgClone.find('.info-package').data('pkgr',packageCounter);

//         pkgClone.appendTo( $( "#pkg-wrapper tbody" ) );

//         pkgClone.removeClass('hidden');
//         //show remove button
//         pkgClone.find(".remove-package").removeClass("hidden");
//         //pkgClone.find("#add-pkg").addClass("hidden");

//     });


//      $("#pkg-wrapper").on("click",".remove-package", function(){

//         var removeObj = $(this).closest(".package-input");
        
//         if(removeObj.attr('id') !== 'package-master')
//         {

//             removeObj.remove();

//             --packageCounter;
            
//             $("#pkg-count").val(packageCounter);
//         }
//         //$( "input.calculate-weight" ).trigger( "focusout" );

//         //$( "input.calculate-chargeable-weight" ).trigger( "focusout" );

//     });

     
</script>
