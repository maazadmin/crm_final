
</div>

<!-- Footer -->
<footer class="footer container-fluid pl-30 pr-30">
    <div class="row">
        <div class="col-sm-12">
            <p>2019 &copy; ERP. Developed by <a href='http://skytechsol.com/' target='_blank'>Sky Tech Sol</a></p>
        </div>
    </div>
</footer>
<!-- /Footer -->

</div>
<!-- /Main Content -->

</div>
<!-- /#wrapper -->

<!-- JavaScript -->

<!-- jQuery -->
<script src="<?php echo base_url(); ?>public_html/backend/vendors/bower_components/jquery/dist/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script
    src="<?php echo base_url(); ?>public_html/backend/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- Data table JavaScript -->
<script
    src="<?php echo base_url(); ?>public_html/backend/vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>

<!-- Slimscroll JavaScript -->
<script src="<?php echo base_url(); ?>public_html/backend/dist/js/jquery.slimscroll.js"></script>

<!-- Progressbar Animation JavaScript -->
<script
    src="<?php echo base_url(); ?>public_html/backend/vendors/bower_components/waypoints/lib/jquery.waypoints.min.js"></script>
<script
    src="<?php echo base_url(); ?>public_html/backend/vendors/bower_components/jquery.counterup/jquery.counterup.min.js"></script>

<!-- Fancy Dropdown JS -->
<script src="<?php echo base_url(); ?>public_html/backend/dist/js/dropdown-bootstrap-extended.js"></script>

<!-- Sparkline JavaScript -->
<script
    src="<?php echo base_url(); ?>public_html/backend/vendors/jquery.sparkline/dist/jquery.sparkline.min.js"></script>

<!-- Owl JavaScript -->
<script
    src="<?php echo base_url(); ?>public_html/backend/vendors/bower_components/owl.carousel/dist/owl.carousel.min.js"></script>

<!-- Switchery JavaScript -->
<script
    src="<?php echo base_url(); ?>public_html/backend/vendors/bower_components/switchery/dist/switchery.min.js"></script>

<!-- Bootstrap Switch JavaScript -->
<script
    src="<?php echo base_url(); ?>public_html/backend/vendors/bower_components/bootstrap-switch/dist/js/bootstrap-switch.min.js"></script>


<!-- EChartJS JavaScript -->
<script
    src="<?php echo base_url(); ?>public_html/backend/vendors/bower_components/echarts/dist/echarts-en.min.js"></script>
<script src="<?php echo base_url(); ?>public_html/backend/vendors/echarts-liquidfill.min.js"></script>

<!-- Toast JavaScript -->
<script src="<?php echo base_url(); ?>public_html/backend/vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.js"></script>

<!-- Init JavaScript -->
<script src="<?php echo base_url(); ?>public_html/backend/dist/js/toast-data.js"></script>
<script src="<?php echo base_url(); ?>public_html/backend/dist/js/init.js"></script>
<script src="<?php echo base_url(); ?>public_html/backend/dist/js/dashboard-data.js"></script>

<!-- Validation JavaScript -->
<!--<script src="<?php /*echo base_url(); */ ?>public_html/backend/dist/js/jquery.validate.min.js"></script>-->

<!-- Dropsown Search -->
<script src="<?php echo base_url('public_html/backend/dist/select2/select2.min.js') ?>"></script>
<script src="<?php echo base_url('public_html/backend/dist/select2/form.js') ?>"></script>

<!-- Counter -->
<script src="<?php echo base_url();?>public_html/backend/vendors/bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js" ></script>
<script src="<?php echo base_url();?>public_html/backend/dist/js/skills-counter-data.js"></script>
<!-- DataTable JavaScript-->
<!--<script src="<?php /*echo base_url('public_html/backend/dist/js/dataTables.bootstrap.min.js') */?>"></script>
<script src="<?php /*echo base_url('public_html/backend/dist/js/dataTables.fixedHeader.min.js') */?>"></script>
<script src="<?php /*echo base_url('public_html/backend/dist/js/dataTables.responsive.min.js') */?>"></script>
<script src="<?php /*echo base_url('public_html/backend/dist/js/jquery.dataTables.min.js') */?>"></script>
<script src="<?php /*echo base_url('public_html/backend/dist/js/jquery-3.3.1.js') */?>"></script>
<script src="<?php /*echo base_url('public_html/backend/dist/js/responsive.bootstrap.min.js') */?>"></script>-->



<!-- Bootstrap Core JavaScript -->

<script
    src="<?php echo base_url('public_html/backend/vendors/bower_components/bootstrap-validator/dist/validator.min.js'); ?>"></script>

<script>

    jQuery(function ($) {
        $('.select2-select-00').select2({width: '100%'}).on("select2:close", function () {
            $(this).focus();
        });
    });

</script>

</body>

</html>




