<?php
header('Access-Control-Allow-Origin: *');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <title>ERP</title>
    <!-- Favicon -->
    <?php /*if($system_info[0]->system_image != "")
{*/ ?><!--
<link rel="shortcut icon" href="<?php /*echo base_url();*/ ?>/public_html/frontend/image/system/<?php /*echo $system_info[0]->system_image; */ ?>" type="image/x-icon" />
<link rel="icon" href="<?php /*echo base_url();*/ ?>/public_html/frontend/image/system/<?php /*echo $system_info[0]->system_image; */ ?>" type="image/x-icon" />
--><?php /*
}*/ ?>
    <meta name="description" content=""/>
    <meta name="keywords" content=""/>
    <meta name="author" content=""/>

    <!-- Bootstrap Datetimepicker CSS -->
    <link href="<?php echo base_url(); ?>public_html/backend/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css"/>

    <!-- Bootstrap Daterangepicker CSS -->
    <link href="<?php echo base_url(); ?>public_html/backend/vendors/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" type="text/css"/>
    <!-- vector map CSS -->
    <link
        href="<?php echo base_url(); ?>public_html/backend/vendors/bower_components/jasny-bootstrap/dist/css/jasny-bootstrap.min.css"
        rel="stylesheet" type="text/css"/>

    <!-- Data table CSS -->
    <link
        href="<?php echo base_url(); ?>public_html/backend/vendors/bower_components/datatables/media/css/jquery.dataTables.min.css"
        rel="stylesheet" type="text/css"/>

    <!-- Toast CSS -->
    <link
        href="<?php echo base_url(); ?>public_html/backend/vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.css"
        rel="stylesheet" type="text/css">


    <!-- Custom CSS -->
    <link href="<?php echo base_url(); ?>public_html/backend/dist/css/style.css" rel="stylesheet" type="text/css">


    <!-- select2 CSS -->
    <link href="<?php echo base_url(); ?>public_html/backend/vendors/bower_components/select2/dist/css/select2.min.css"
          rel="stylesheet" type="text/css"/>
    <!-- bootstrap-select CSS -->
    <link
        href="<?php echo base_url(); ?>public_html/backend/vendors/bower_components/bootstrap-select/dist/css/bootstrap-select.min.css"
        rel="stylesheet" type="text/css"/>
    <!-- multi-select CSS -->
    <link href="<?php echo base_url(); ?>public_html/backend/vendors/bower_components/multiselect/css/multi-select.css"
          rel="stylesheet" type="text/css"/>

    <!--  DataTable CSS-->
    <!--<link href="<?php /*echo base_url();*/ ?>public_html/backend/dist/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css"/>-->

    <!--<link href="<?php /*echo base_url();*/ ?>public_html/backend/dist/css/responsive.bootstrap.min.css" rel="stylesheet" type="text/css"/>-->
    <!-- Bootstrap Switches CSS -->
    <link
        href="<?php echo base_url(); ?>public_html/backend/vendors/bower_components/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css"
        rel="stylesheet" type="text/css"/>
    <!-- switchery CSS -->
    <link href="<?php echo base_url(); ?>public_html/backend/vendors/bower_components/switchery/dist/switchery.min.css"
          rel="stylesheet" type="text/css"/>
    <style>
       .table th {
           background-color: grey !important;
           color: white !important;
       }
    </style>



</head>

<body>
<!-- Preloader -->
<!-- <div class="preloader-it">
    <div class="la-anim-1"></div>
</div> -->
<!-- /Preloader -->
<div class="wrapper theme-1-active pimary-color-blue slide-nav-toggle">


    <!-- Top Menu Items -->
    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="mobile-only-brand pull-left">
            <div class="nav-header pull-left">
                <div class="logo-wrap">
                    <a href="<?php echo base_url(); ?>">
                        <img class="brand-img" src="<?php echo base_url(); ?>public_html/backend/dist/system.jpg"
                             style="height: 28px;" alt="brand"/>
                        <span class="brand-text">ERP</span>
                    </a>
                </div>
            </div>
            <a id="toggle_nav_btn" class="toggle-left-nav-btn inline-block ml-20 pull-left"
               href="javascript:void(0);"><i class="zmdi zmdi-menu"></i></a>
            <a id="toggle_mobile_search" data-toggle="collapse" data-target="#search_form" class="mobile-only-view"
               href="javascript:void(0);"><i class="zmdi zmdi-search"></i></a>
            <a id="toggle_mobile_nav" class="mobile-only-view" href="javascript:void(0);"><i class="zmdi zmdi-more"></i></a>
            <!--<form id="search_form" role="search" class="top-nav-search collapse pull-left">
                <div class="input-group">
                    <input type="text" name="example-input1-group2" class="form-control" placeholder="Search">
                    <span class="input-group-btn">
                    <button type="button" class="btn  btn-default"  data-target="#search_form" data-toggle="collapse" aria-label="Close" aria-expanded="true"><i class="zmdi zmdi-search"></i></button>
                    </span>
                </div>
            </form>-->
        </div>
        <div id="mobile_only_nav" class="mobile-only-nav pull-right">
            <ul class="nav navbar-right top-nav pull-right">

                <li class="dropdown auth-drp">
                    <a href="#" class="dropdown-toggle pr-0" data-toggle="dropdown"><img
                            src="<?php echo base_url(); ?>public_html/backend/img/user1.png" alt="user_auth"
                            class="user-auth-img img-circle"/><span class="user-online-status"></span></a>
                    <ul class="dropdown-menu user-auth-dropdown" data-dropdown-in="flipInX"
                        data-dropdown-out="flipOutX">

                        <li>
                            <a href="<?php echo base_url('Login/logout'); ?>"><i class="zmdi zmdi-power"></i><span>Log Out</span></a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
    <!-- /Top Menu Items -->
    <!-- Left Sidebar Menu -->
    <div class="fixed-sidebar-left">
        <ul class="nav navbar-nav side-nav nicescroll-bar">
            <li class="navigation-header">
                <span>Main</span>
                <i class="zmdi zmdi-more"></i>
            </li>
            <!--<li>
                <a class="" href="javascript:void(0);" data-toggle="collapse" data-target="#dashboard_dr">
                    <div class="pull-left"><i class="zmdi zmdi-landscape mr-20"></i><span class="right-nav-text">Accounts</span>
                    </div>
                    <div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div>
                    <div class="clearfix"></div>
                </a>
                <ul id="dashboard_dr" class="collapse collapse-level-1">
                    <li>
                        <a class="active-page" href="#">Account Type</a>
                    </li>
                </ul>
            </li>-->
            <li>
                <a class="active" href="<?php echo base_url('Dashboard');?>"><div class="pull-left"><i class="zmdi zmdi zmdi-landscape mr-20"></i><span class="right-nav-text">Dashboard</span></div><div class="clearfix"></div></a>
            </li>
            <li>
                <a class="" href="<?php echo base_url('Users/my_account');?>"><div class="pull-left"><i class="zmdi zmdi zmdi-landscape mr-20"></i><span class="right-nav-text">My Account</span></div><div class="clearfix"></div></a>
            </li>
            <?php

            if (count($navigations) > 0) {
                foreach ($navigations['parent_nav'] as $key => $item) {
                    if ($childNav['nav_name'] == 'Dashboard') {
                        continue;
                    }
                    ?>
                    <li>
                        <a class="<?= $key == 0 ? '' : '' ?>" href="javascript:void(0);" data-toggle="collapse"
                           data-target="#navlink<?= $item['navigation_id'] ?>">
                            <div class="pull-left"><i class="zmdi <?= $item['nav_icon'] ?> mr-20"></i><span
                                    class="right-nav-text"><?= $item['nav_name'] ?></span></div>
                            <div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div>
                            <div class="clearfix"></div>
                        </a>
                        <ul id="navlink<?= $item['navigation_id'] ?>" class="collapse in collapse-level-1">
                            <?php
                            if (isset($navigations['child_nav'][$item['navigation_id']])) {
                                foreach ($navigations['child_nav'][$item['navigation_id']] as $childNav) {
                                    ?>
                                    <li>
                                        <a class="<?= $childNav['nav_link'] == $this->uri->segment(1) ? 'active-page' : '' ?>"
                                           href="<?php echo base_url($childNav['nav_link']); ?>"><?= $childNav['nav_name'] ?></a>
                                    </li>
                                    <?php
                                }
                            }
                            ?>
                        </ul>
                    </li>
                    <?php
                }
            }
            ?>
            <!--<li>
					<a class="active" href="javascript:void(0);" data-toggle="collapse" data-target="#dashboard_dr"><div class="pull-left"><i class="zmdi zmdi-landscape mr-20"></i><span class="right-nav-text">Accounts</span></div><div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div><div class="clearfix"></div></a>
					<ul id="dashboard_dr" class="collapse collapse-level-1">
						<li>
							<a class="active-page" href="<?php /*echo base_url('account_types');*/ ?>">Account Type</a>
						</li>
						<li>
							<a class="active-page" href="<?php /*echo base_url('gl_accounts');*/ ?>">Account Control</a>
						</li>
						<li>
							<a class="active-page" href="<?php /*echo base_url('customers');*/ ?>">Customers</a>
						</li>
						<li>
							<a class="active-page" href="<?php /*echo base_url('job');*/ ?>">Job Order</a>
						</li>
						<li>
							<a class="active-page" href="<?php /*echo base_url('job/invoice_listing');*/ ?>">Invoice</a>
						</li>
						<li>
							<a class="active-page" href="<?php /*echo base_url('suppliers');*/ ?>">Suppliers</a>
						</li>
						<li>
							<a class="active-page" href="<?php /*echo base_url('purchase_orders');*/ ?>">Purchase Orders</a>
						</li>
						<li>
							<a class="active-page" href="<?php /*echo base_url('products');*/ ?>">Products</a>
						</li>
						<li>
							<a class="active-page" href="<?php /*echo base_url('purchase_invoices');*/ ?>">Purchase invoices</a>
						</li>
						<li>
							<a class="active-page" href="<?php /*echo base_url('Warehouses');*/ ?>">Warehouses</a>
						</li>
						<li>
							<a class="active-page" href="<?php /*echo base_url('Departments');*/ ?>">Departments</a>
						</li>
						<li>
							<a class="active-page" href="<?php /*echo base_url('Roles');*/ ?>">Roles</a>
						</li>
						<li>
							<a class="active-page" href="<?php /*echo base_url('Users');*/ ?>">Users</a>
						</li>
						<li>
							<a class="active-page" href="<?php /*echo base_url('Journal_entries');*/ ?>">Journal Entries</a>
						</li>

					</ul>
				</li>
				<li>
					<a class="active" href="javascript:void(0);" data-toggle="collapse" data-target="#report_dr"><div class="pull-left"><i class="zmdi zmdi-landscape mr-20"></i><span class="right-nav-text">Reports</span></div><div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div><div class="clearfix"></div></a>
					<ul id="report_dr" class="collapse collapse-level-1">
						<li>
							<a class="active-page" href="<?php /*echo base_url('Report/balance_sheet');*/ ?>">Balance sheet</a>
						</li>
						<li>
							<a class="active-page" href="<?php /*echo base_url('Report/bank_book');*/ ?>">Bank Book</a>
						</li>
						<li>
							<a class="active-page" href="<?php /*echo base_url('Report/income_statment');*/ ?>">P/L Statement</a>
						</li>
						<li>
							<a class="active-page" href="<?php /*echo base_url('Report/cash_book');*/ ?>">Cash Book</a>
						</li>
						<li>
							<a class="active-page" href="<?php /*echo base_url('Report/ledger');*/ ?>">View Ledger</a>
						</li>
					</ul>
				</li>
				<li>
					<a class="active" href="javascript:void(0);" data-toggle="collapse" data-target="#setting_dr"><div class="pull-left"><i class="zmdi zmdi-landscape mr-20"></i><span class="right-nav-text">Setting</span></div><div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div><div class="clearfix"></div></a>
					<ul id="setting_dr" class="collapse collapse-level-1">
						<li>
							<a class="active-page" href="<?php /*echo base_url('Settings/ledger_settings');*/ ?>">Ledger Setting</a>
						</li>
					</ul>
				</li>-->


        </ul>
    </div>
    <!-- /Left Sidebar Menu -->