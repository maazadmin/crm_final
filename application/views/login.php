   <!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		 <title></title>
		 <!-- Favicon -->
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<meta name="author" content=""/>
		
		<!-- vector map CSS -->
		<link href="<?php echo base_url();?>public_html/backend/vendors/bower_components/jasny-bootstrap/dist/css/jasny-bootstrap.min.css" rel="stylesheet" type="text/css"/>
		
		    <!-- Data table CSS -->
    <link href="<?php echo base_url();?>public_html/backend/vendors/bower_components/datatables/media/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
    
    <!-- Toast CSS -->
    <link href="<?php echo base_url();?>public_html/backend/vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.css" rel="stylesheet" type="text/css">
        
  
		<!-- Custom CSS -->
		<link href="<?php echo base_url();?>public_html/backend/dist/css/style.css" rel="stylesheet" type="text/css">

		
		<!-- select2 CSS -->
		<link href="<?php echo base_url();?>public_html/backend/vendors/bower_components/select2/dist/css/select2.min.css" rel="stylesheet" type="text/css"/>
	<!-- bootstrap-select CSS -->
		<link href="<?php echo base_url();?>public_html/backend/vendors/bower_components/bootstrap-select/dist/css/bootstrap-select.min.css" rel="stylesheet" type="text/css"/>
	<!-- multi-select CSS -->
		<link href="<?php echo base_url();?>public_html/backend/vendors/bower_components/multiselect/css/multi-select.css" rel="stylesheet" type="text/css"/>
	</head>

	<style>
	.help-block
	{
	font-size: 12px;
    display: block;
    margin-top: 5px;
    font-weight: normal;
    color: #F44336;
	
	}
	
	.form-group 
	{
		margin-bottom:5px;
	}
	</style>
		<body>
		<!--Preloader-->
		<!--<div class="preloader-it">
			<div class="la-anim-1"></div>
		</div>-->
		<!--/Preloader-->
		
		<div class="wrapper pa-0">
			<header class="sp-header">
				<div class="sp-logo-wrap pull-left">
				
				
				<?php /* if($system_info[0]->system_image != "")
{
	
} */?>

					<a href="#">
					<!--	<img class="brand-img mr-10" src="dist/system.jpg" style="height: 28px;" alt="brand"/>-->
						<span class="brand-text">CRM</span>
					</a>
					
					
				</div>
				<div class="form-group mb-0 pull-right">
					<!--<span class="inline-block pr-10">Don't have an account?</span>
					<a class="inline-block btn btn-primary  btn-rounded btn-outline" href="signup.html">Sign Up</a>-->
				</div>
				<div class="clearfix"></div>
			</header>
			
			<!-- Main Content -->
			<div class="page-wrapper pa-0 ma-0 auth-page">
				<div class="container-fluid">
					<!-- Row -->
					<div class="table-struct full-width full-height">
						<div class="table-cell vertical-align-middle auth-form-wrap">
							<div class="auth-form  ml-auto mr-auto no-float">
								<div class="row">
									<div class="col-sm-12 col-xs-12">
										<div class="mb-30">
											<h3 class="text-center txt-dark mb-10">Sign in to CRM</h3>
											<h6 class="text-center nonecase-font txt-grey">Enter your details below</h6>
										</div>	
										<div class="form-wrap">
										<?php
    if($this->session->flashdata())
    {
        foreach($this->session->flashdata() as $key => $value):
            if($key == 'update' || $key == 'saved')
            {
                $alert_class = 'bg-green';
            }else
            {
                $alert_class = 'bg-red';
            }
            ?>
			<div class="alert <?php echo $alert_class; ?> alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
				</button>
				<p>
                    <strong>
                        <i class="icon-ok"></i>
                        <?php echo ucwords(strtolower($key)); ?> !
                    </strong>
                    <?php echo $value; ?>

                </p>
            </div>
        <?php
        endforeach;
    }  ?>
	
											  
				<form method="post" class="form-validate-login" action="<?php echo base_url().'Login/check_user'; ?>" />
						<div class="form-group">
							<label class="control-label mb-10" for="username">Username</label>
							<input type="text" name="username" class="form-control"  id="username" placeholder="Username">
												</div>
						<div class="form-group">
							<label class="pull-left control-label mb-10" for="password">Password</label>
							<!--<a class="capitalize-font txt-primary block mb-10 pull-right font-12" href="forgot-password.html">forgot password ?</a>-->
							<div class="clearfix"></div>
							<input type="password" class="form-control"  name="password" id="password" placeholder="Password">
						</div>
												
						<div class="form-group">
							<div class="checkbox checkbox-primary pr-10 pull-left">
							<input id="checkbox_2" type="checkbox">
							<label for="checkbox_2"> Keep me logged in</label>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="form-group text-center">
							<button type="submit" class="btn btn-primary  btn-rounded">sign in</button>
						</div>
				</form>											
											<div class="row m-t-15 m-b--20">
                        <div class="col-xs-12">
						   <p style="margin: 0;
    vertical-align: bottom;
    text-align: center;
    padding: 0;" ><b>Powered by </b><a href="http://www.skytechsol.com/" target="_blank"><img src="<?php echo base_url();?>/public_html/backend/skytechsol.jpg" style="height:50px;" /></a></p>
                          
						</div>
					</div>	
					
					
										</div>
									</div>	
								</div>
							</div>
						</div>
					</div>
					<!-- /Row -->	
				</div>
				
			</div>
			<!-- /Main Content -->
		
		</div>
		
		<!-- <?php
		        $this->load->view('Template/footer');
		        ?> -->
		        <!-- Footer -->
			<footer class="footer container-fluid pl-30 pr-30">
				<div class="row">
					<div class="col-sm-12">
						<p>2018 &copy; CRM. Developed by <a href='http://skytechsol.com/' target='_blank' >Sky Tech Sol</a></p>
					</div>
				</div>
			</footer>
			<!-- /Footer -->
			
		</div>
        <!-- /Main Content -->

    </div>
    <!-- /#wrapper -->
	
	<!-- JavaScript -->
	
    <!-- jQuery -->
    <script src="<?php echo base_url();?>public_html/backend/vendors/bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url();?>public_html/backend/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    
	<!-- Data table JavaScript -->
	<script src="<?php echo base_url();?>public_html/backend/vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
	
	<!-- Slimscroll JavaScript -->
	<script src="<?php echo base_url();?>public_html/backend/dist/js/jquery.slimscroll.js"></script>
	
	<!-- Progressbar Animation JavaScript -->
	<script src="<?php echo base_url();?>public_html/backend/vendors/bower_components/waypoints/lib/jquery.waypoints.min.js"></script>
	<script src="<?php echo base_url();?>public_html/backend/vendors/bower_components/jquery.counterup/jquery.counterup.min.js"></script>
	
	<!-- Fancy Dropdown JS -->
	<script src="<?php echo base_url();?>public_html/backend/dist/js/dropdown-bootstrap-extended.js"></script>
	
	<!-- Sparkline JavaScript -->
	<script src="<?php echo base_url();?>public_html/backend/vendors/jquery.sparkline/dist/jquery.sparkline.min.js"></script>
	
	<!-- Owl JavaScript -->
	<script src="<?php echo base_url();?>public_html/backend/vendors/bower_components/owl.carousel/dist/owl.carousel.min.js"></script>
	
	<!-- Switchery JavaScript -->
	<script src="<?php echo base_url();?>public_html/backend/vendors/bower_components/switchery/dist/switchery.min.js"></script>
	
	<!-- EChartJS JavaScript -->
	<script src="<?php echo base_url();?>public_html/backend/vendors/bower_components/echarts/dist/echarts-en.min.js"></script>
	<script src="<?php echo base_url();?>public_html/backend/vendors/echarts-liquidfill.min.js"></script>
	
	<!-- Toast JavaScript -->
	<script src="<?php echo base_url();?>public_html/backend/vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.js"></script>
	
	<!-- Init JavaScript -->
	<script src="<?php echo base_url();?>public_html/backend/dist/js/init.js"></script>
	<script src="<?php echo base_url();?>public_html/backend/dist/js/dashboard-data.js"></script>
</body>

</html>


	
		

		
		



<!-- inline scripts related to this page -->
<?php
if(file_exists('application/modules/'.$controller.'/views/js.php') == true)
{
    $this->load->view($controller.'/js');  
}  



?>
 <!-- Validation Plugin Js -->
    <script src="<?php echo base_url(); ?>public_html/backend/plugins/jquery-validation/jquery.validate.js"></script>


		<script type="text/javascript">
    jQuery(function($) {
		
		  $('.form-validate-login').validate({

            errorElement: 'div',
            errorClass: 'help-block',
            focusInvalid: false,
            rules:
            {
                username: {
                    required: true
                },
                password: {
                    required: true
                }
            },
            messages: {
                username: " Please enter username",
                password: "   Please enter password"
            },
            invalidHandler: function (event, validator) { //display error alert on form submit
                $('.alert-danger', $('.form-validate')).show();
            },

            highlight: function (e) {
                $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
            },

            success: function (e) {
                $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
                $(e).remove();
            },
            errorPlacement: function (error, element) {
                error.insertAfter(element.parent());
            },

        });
        // Direct change value in data base on for changing status

		
		
		// Direct change value in data base on for changing status
        setTimeout(function(){
            $('.alert-dismissible').remove();
            }, 2000);  
        setTimeout(function(){
            $('.alert-dismissible').remove();
            }, 2000);
			
			
		
	});
	
	</script>
		
	</body>
</html>
