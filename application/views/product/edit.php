<!-- Main Content -->
<div class="page-wrapper">
    <div class="container-fluid">

        <!-- Title -->
        <div class="row heading-bg">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h5 class="txt-dark"><?= $heading ?></h5>
            </div>

            <!-- Breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="index.html">Dashboard</a></li>
                    <li><a href="active"><span><?= $heading ?></span></a></li>

                </ol>
            </div>
            <!-- /Breadcrumb -->

        </div>
        <!-- /Title -->

        <!-- Row -->
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default card-view">
                    <div class="panel-heading">
                        <div class="pull-left">
                            <h6 class="panel-title txt-dark"><?php echo $title; ?></h6>
                        </div>
                       
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body">
                             <div class="">
                            <a href="<?= site_url($this->uri->segment(1)) ?>" class="btn btn-success btn-sm">View
                                List</a>
                        </div>
                        <br>
                            <p class="text-muted"><?php echo $description; ?>.</p>

                            <div class="row">
                                <div class="col-sm-12 col-xs-12">
                                    <div class="form-wrap">
                                        <?php
                                        //Show message
                                        echo $this->session->flashdata("msg");
                                        ?>
                                        <form method="post" class="ac-type-form" enctype="multipart/form-data"
                                              action="<?= site_url($this->uri->segment(1) . '/edit/' . (($item !== false) ? $item[$pmKey] : "")) ?>"
                                              data-toggle="validator" role="form">
                                               <div class="form-group col-md-4">
                                                <label class="control-label mb-10" for="exampleInputuname_1">Product 
                                                    Name
                                                </label>
                                                <input type="text" class="form-control"
                                                       name="product_name" placeholder="Product Name" 
                                                       value="<?php echo ($item !== false) ? $item['product_name'] : ""; ?>" required>
                                            </div>
                                        <div class="form-group col-md-4">
                                                    <label class="control-label mb-10"
                                                           for="exampleInputEmail_1">Description</label>
                                                    <textarea class="form-control" name="description">
                                                        <?php echo ($item !== false) ? $item['product_description'] : ""; ?>
                                                    </textarea>
                                                </div>

                                          <div class="form-group col-md-4">
                                                <label class="control-label mb-10" for="exampleInputuname_1">Price 
                                                    
                                                </label>
                                                <input type="text" class="form-control"
                                                       name="product_price" value="<?php echo ($item !== false) ? $item['sellprice'] : ""; ?>" placeholder="Price" required>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group col-md-4">
 <label class="control-label mb-10" for="exampleInputuname_1">Select Currency
                                                </label>
                                                    <select class="form-control" name="currency_id" id="nav_id">

     <option value="">Select Currency</option>
                                                <?php foreach ($currency_dropdownn as $row1) : 
if ($row1['currency_id'] == $item['currency_id']) {
                                                                $selected = "selected";
                                                            } else {
                                                                $selected = "";
                                                            }
                                                    ?>

         <option  <?= $selected; ?> value="<?= $row1['currency_id'];?>">
            <?= $row1['currency_name'];?> </option>
             <?php endforeach; ?>
</select>
</div>
                                             

                                            



                                            <div class="form-group col-md-10">


                                                <button type="submit" class="btn btn-success mr-10" name="btn_submit">
                                                    Submit
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
                <!-- /Row -->

            </div>
            <!-- /Main Content -->
