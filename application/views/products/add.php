<!-- Main Content -->
<div class="page-wrapper">
    <div class="container-fluid">

        <!-- Title -->
        <div class="row heading-bg">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h5 class="txt-dark"><?= $heading ?></h5>
            </div>

            <!-- Breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="index.html">Dashboard</a></li>
                    <li><a href="active"><span><?= $heading ?></span></a></li>

                </ol>
            </div>
            <!-- /Breadcrumb -->

        </div>
        <!-- /Title -->

        <!-- Row -->
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default card-view">
                    <div class="panel-heading">
                        <div class="pull-left">
                            <h6 class="panel-title txt-dark"><?php echo $title; ?></h6>
                        </div>
                        <div class="pull-right">
                            <a href="<?= site_url($this->uri->segment(1)) ?>" class="btn btn-success btn-sm">View
                                List</a>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body">
                            <p class="text-muted"><?php echo $description; ?>.</p>

                            <div class="row">
                                <div class="col-sm-12 col-xs-12">
                                    <div class="form-wrap">
                                        <?php
                                        //Show message
                                        echo $this->session->flashdata("msg");
                                        ?>


                                        <form method="post" enctype="multipart/form-data"
                                              action="<?= site_url($this->uri->segment(1) . '/add') ?>"
                                              data-toggle="validator" role="form">
                                            <div class="row">


                                                <div class="form-group col-md-4">
                                                    <label class="control-label mb-10"
                                                           for="exampleInputuname_1">Product Code</label>

                                                    <input type="text" class="form-control"
                                                           name="product_code"
                                                           placeholder="Product Code" required>
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label class="control-label mb-10"
                                                           for="exampleInputuname_1">Product Name</label>

                                                    <input type="text" class="form-control"
                                                           name="product_name"
                                                           placeholder="Product Name" required>
                                                </div>

                                                <div class="form-group col-md-4">
                                                    <label class="control-label mb-10"
                                                           for="exampleInputEmail_1">Description</label>
                                                    <textarea class="form-control" name="description"></textarea>
                                                </div>

                                            </div>

                                            <div class="row">
                                                <div class="form-group col-md-4">
                                                    <label class="control-label mb-10"
                                                           for="exampleInputEmail_1">Is Active</label>

                                                    <input type="checkbox" class="js-switch js-switch-1"  data-color="#e11d8e" name="is_active">
                                                </div>

                                                <div class="form-group col-md-4">
                                                    <label class="control-label mb-10"
                                                           for="exampleInputuname_1">Unit of measurement</label>

                                                    <select class="form-control" name="uom_id">
                                                        <option>Select Masurement</option>
                                                        <?php foreach ($unit_of_measurements_list as $row): ?>
                                                            <option
                                                                value="<?= $row['uom_id']; ?>"><?= $row['unit_name']; ?></option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>

                                                <div class="form-group col-md-4">
                                                    <label class="control-label mb-10"
                                                           for="exampleInputuname_1">Min stock quantity</label>

                                                    <input type="text" class="form-control"
                                                           name="min_stock_quantity"
                                                           placeholder="Min stock quantity">
                                                </div>

                                            </div>
                                            <div class="row">
                                                <div class="form-group col-md-4">
                                                    <label class="control-label mb-10"
                                                           for="exampleInputEmail_1">Max stock quantity</label>

                                                    <input type="text" class="form-control"
                                                           name="max_stock_quantity"
                                                           placeholder="Max stock quantity">
                                                </div>

                                                <div class="form-group col-md-4">
                                                    <label class="control-label mb-10"
                                                           for="exampleInputEmail_1">product Brand</label>

                                                    <select class="form-control" name="product_brand_id">
                                                        <option>Select Brand</option>
                                                        <?php foreach ($product_brands_list as $row1): ?>
                                                            <option
                                                                value="<?= $row1['product_brand_id']; ?>"><?= $row1['brand_name']; ?></option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>

                                                <div class="form-group col-md-4">
                                                    <label class="control-label mb-10"
                                                           for="exampleInputEmail_1">Sell price</label>

                                                    <input type="text" class="form-control"
                                                           name="sellprice"
                                                           placeholder="Sell price">
                                                </div>
                                            </div>
                                            <div class="row">

                                                <div class="form-group col-md-4">
                                                    <label class="control-label mb-10"
                                                           for="exampleInputEmail_1">Tax rate</label>

                                                    <select class="form-control" name="tax_rate_id">
                                                        <option>Select Tax rate</option>
                                                        <?php foreach ($tax_rates_list as $row2): ?>
                                                            <option
                                                                value="<?= $row2['tax_rate_id']; ?>"><?= $row2['tax_rate_name']; ?></option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>

                                                <!--<div class="form-group col-md-4">
                                                    <label class="control-label mb-10"
                                                           for="exampleInputEmail_1">Is Service</label>

                                                    <input type="checkbox" class="" name="is_service">
                                                </div>-->
                                            </div>

                                            <!-- Row -->
                                            <div class="row">
                                                <div class="col-md-8">
                                                    <div class="panel panel-default card-view">
                                                        <div class="panel-heading">
                                                            <div class="pull-left">
                                                                <!--<h6 class="panel-title txt-dark">one open</h6>-->
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                        <div class="panel-wrapper collapse in">
                                                            <div class="panel-body">
                                                                <div class="panel-group accordion-struct" id="accordion_1" role="tablist" aria-multiselectable="true">
                                                                    <div class="panel panel-default">
                                                                        <div class="panel-heading activestate" role="tab" id="heading_1">
                                                                            <a role="button" data-toggle="collapse" data-parent="#accordion_1" href="#collapse_1" aria-expanded="true">Supplier Information</a>
                                                                        </div>
                                                                        <div id="collapse_1" class="panel-collapse collapse in" role="tabpanel">
                                                                            <div class="panel-body pa-15">
                                                                                <div id="payment" class="panel-body">
                                                                                    <div class="col-md-12">
                                                                                        <table class="table-responsive table-condensed">
                                                                                            <thead>
                                                                                            <tr>
                                                                                                <th>Supplier</th>
                                                                                                <th>Price</th>

                                                                                            </tr>
                                                                                            </thead>
                                                                                            <tbody>
                                                                                            <tr>


                                                                                                <td>
                                                                                                    <select class="form-control"
                                                                                                            name="supplier_id" id="supplier_id">
                                                                                                        <option value="">Select Supplier</option>
                                                                                                        <?php foreach ($supplier_list as $row) : ?>

                                                                                                            <option
                                                                                                                value="<?= $row['supplier_id']; ?>"><?= $row['title']; ?></option>
                                                                                                        <?php endforeach; ?>
                                                                                                    </select>

                                                                                                    <!--  <input type="text" id="size" name="size" style=""  class="form-control"  placeholder="Registration No" > -->
                                                                                                    <input type="hidden" id="supplier_name"
                                                                                                           name="supplier_name">

                                                                                                </td>
                                                                                                <td><input type="number" class="form-control"
                                                                                                           id="supplier_price" name="supplier_price"
                                                                                                           placeholder="" value="0"></td>

                                                                                            </tr>

                                                                                            </tbody>

                                                                                        </table>
                                                                                        <div class="pull-right">
                                                                                            <a href="#" id="insert_supplier" class="btn btn-primary">Add
                                                                                                Supplier</a>
                                                                                        </div>
                                                                                        <div style="clear:both;"></div>

                                                                                        <div id="ccinfo_supplier"
                                                                                             class="col-md-12 <?php if ($row[0]->size_status == 1) {
                                                                                                 echo "";
                                                                                             } else {
                                                                                                 echo "hidden";
                                                                                             } ?>">

                                                                                            <table id="supplier_table"
                                                                                                   class="table-responsive table-condensed">
                                                                                                <thead>
                                                                                                <tr>
                                                                                                    <th style="width: 50px;">Action</th>

                                                                                                    <th>Supplier</th>
                                                                                                    <th>Price</th>
                                                                                                </tr>


                                                                                                </thead>
                                                                                                <tbody>

                                                                                                <?php
                                                                                                if ($row[0]->size_status > 0) {
                                                                                                    $size_list = $this->Mdl_product->get_size_list_by_product_id($row[0]->id);
                                                                                                    foreach ($size_list as $list):
                                                                                                        ?>
                                                                                                        <tr>

                                                                                                            <td>
                                                                                                                <!-- <input type='checkbox'  class='record' name='record'> -->
                                                                                                            </td>

                                                                                                            <td><input type='hidden'
                                                                                                                       class='form-control'
                                                                                                                       readonly
                                                                                                                       name='check_size_id_new'
                                                                                                                       value="<?php /*echo $list->size_id;*/
                                                                                                                       ?>">
                                                                                                                <input type='hidden'
                                                                                                                       class='form-control'
                                                                                                                       readonly
                                                                                                                       name='size_id_new[]'
                                                                                                                       value="<?php /*echo $list->size_id;*/
                                                                                                                       ?>">
                                                                                                                <input type='hidden'
                                                                                                                       class='form-control'
                                                                                                                       readonly
                                                                                                                       name='old_quantity_new[]'
                                                                                                                       value="<?php /*echo $list->quantity;*/
                                                                                                                       ?>">

                                                                                                                <input type='hidden'
                                                                                                                       class='form-control'
                                                                                                                       readonly
                                                                                                                       name='size_update_new[]'
                                                                                                                       value="<?php /*echo $list->id;*/
                                                                                                                       ?>">
                                                                                                                <input type='text'
                                                                                                                       class='form-control'
                                                                                                                       readonly
                                                                                                                       name='size_new[]'
                                                                                                                       value="<?php /*echo $list->size_name;*/
                                                                                                                       ?>">
                                                                                                            </td>

                                                                                                            <td>
                                                                                                                <input type='number'
                                                                                                                       class='form-control'
                                                                                                                       name='quantity_new[]'
                                                                                                                       value="<?php /*echo $list->quantity;*/
                                                                                                                       ?>">
                                                                                                            </td>
                                                                                                            <td>
                                                                                                                <input type='number'
                                                                                                                       class='form-control'
                                                                                                                       name='sorting_order_new[]'
                                                                                                                       value="<?php /*echo $list->sorting_order;*/
                                                                                                                       ?>">
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <?php
                                                                                                    endforeach;
                                                                                                } ?>
                                                                                                </tbody>
                                                                                            </table>
                                                                                            <button type="button" id="delete-row"
                                                                                                    class="delete-row-supplier btn btn-danger btn-xs">
                                                                                                Delete Record
                                                                                            </button>
                                                                                        </div>

                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="panel panel-default">
                                                                        <div class="panel-heading" role="tab" id="heading_2">
                                                                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion_1" href="#collapse_2" aria-expanded="false">Warehouces/Store Information </a>
                                                                        </div>
                                                                        <div id="collapse_2" class="panel-collapse collapse" role="tabpanel">
                                                                            <div class="panel-body pa-15">
                                                                                <div id="payment" class="panel-body">
                                                                                    <div class="col-md-12">
                                                                                        <table class="table-responsive table-condensed">
                                                                                            <thead>
                                                                                            <tr>
                                                                                                <th>Warehouse</th>
                                                                                                <th>Qty</th>

                                                                                            </tr>
                                                                                            </thead>
                                                                                            <tbody>
                                                                                            <tr>


                                                                                                <td>
                                                                                                    <select class="form-control"
                                                                                                            name="warehouse_id" id="warehouse_id">
                                                                                                        <option value="">Select Warehouse</option>
                                                                                                        <?php foreach ($warehouses_list as $row) : ?>

                                                                                                            <option
                                                                                                                value="<?= $row['warehouse_id']; ?>"><?= $row['warehouse_name']; ?></option>
                                                                                                        <?php endforeach; ?>
                                                                                                    </select>

                                                                                                    <input type="hidden" id="warehouse_name" >

                                                                                                </td>
                                                                                                <td><input type="number" class="form-control"
                                                                                                           id="warehouse_qty"  value="1"></td>

                                                                                            </tr>

                                                                                            </tbody>

                                                                                        </table>
                                                                                        <div class="pull-right">
                                                                                            <a href="#" id="insert_warehouse" class="btn btn-primary">Add
                                                                                                Warehouse</a>
                                                                                        </div>
                                                                                        <div style="clear:both;"></div>

                                                                                        <div id="ccinfo_warehouse"
                                                                                             class="col-md-12 <?php if ($row[0]->size_status == 1) {
                                                                                                 echo "";
                                                                                             } else {
                                                                                                 echo "hidden";
                                                                                             } ?>">

                                                                                            <table id="warehouse_table"
                                                                                                   class="table-responsive table-condensed">
                                                                                                <thead>
                                                                                                <tr>
                                                                                                    <th style="width: 50px;">Action</th>

                                                                                                    <th>Warehouse</th>
                                                                                                    <th>Qty</th>
                                                                                                </tr>


                                                                                                </thead>
                                                                                                <tbody>

                                                                                                <?php
                                                                                                if ($row[0]->size_status > 0) {
                                                                                                    $size_list = $this->Mdl_product->get_size_list_by_product_id($row[0]->id);
                                                                                                    foreach ($size_list as $list):
                                                                                                        ?>
                                                                                                        <tr>

                                                                                                            <td>
                                                                                                                <!-- <input type='checkbox'  class='record' name='record'> -->
                                                                                                            </td>

                                                                                                            <td><input type='hidden'
                                                                                                                       class='form-control'
                                                                                                                       readonly
                                                                                                                       name='check_size_id_new'
                                                                                                                       value="<?php /*echo $list->size_id;*/
                                                                                                                       ?>">
                                                                                                                <input type='hidden'
                                                                                                                       class='form-control'
                                                                                                                       readonly
                                                                                                                       name='size_id_new[]'
                                                                                                                       value="<?php /*echo $list->size_id;*/
                                                                                                                       ?>">
                                                                                                                <input type='hidden'
                                                                                                                       class='form-control'
                                                                                                                       readonly
                                                                                                                       name='old_quantity_new[]'
                                                                                                                       value="<?php /*echo $list->quantity;*/
                                                                                                                       ?>">

                                                                                                                <input type='hidden'
                                                                                                                       class='form-control'
                                                                                                                       readonly
                                                                                                                       name='size_update_new[]'
                                                                                                                       value="<?php /*echo $list->id;*/
                                                                                                                       ?>">
                                                                                                                <input type='text'
                                                                                                                       class='form-control'
                                                                                                                       readonly
                                                                                                                       name='size_new[]'
                                                                                                                       value="<?php /*echo $list->size_name;*/
                                                                                                                       ?>">
                                                                                                            </td>

                                                                                                            <td>
                                                                                                                <input type='number'
                                                                                                                       class='form-control'
                                                                                                                       name='quantity_new[]'
                                                                                                                       value="<?php /*echo $list->quantity;*/
                                                                                                                       ?>">
                                                                                                            </td>
                                                                                                            <td>
                                                                                                                <input type='number'
                                                                                                                       class='form-control'
                                                                                                                       name='sorting_order_new[]'
                                                                                                                       value="<?php /*echo $list->sorting_order;*/
                                                                                                                       ?>">
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <?php
                                                                                                    endforeach;
                                                                                                } ?>
                                                                                                </tbody>
                                                                                            </table>
                                                                                            <button type="button" id="delete-row"
                                                                                                    class="delete-row-warehouse btn btn-danger btn-xs">
                                                                                                Delete Record
                                                                                            </button>
                                                                                        </div>

                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /Row -->


                                            <div class="form-group col-md-10">


                                                <button type="submit" class="btn btn-success mr-10" name="btn_submit">
                                                    Submit
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
                <!-- /Row -->

            </div>
            <!-- /Main Content -->