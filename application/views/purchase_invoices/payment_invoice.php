<!-- Main Content -->
<div class="page-wrapper">
    <div class="container-fluid">

        <!-- Title -->
        <div class="row heading-bg">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h5 class="txt-dark"><?= $heading ?></h5>
            </div>

            <!-- Breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="index.html">Dashboard</a></li>
                    <li><a href="active"><span><?= $heading ?></span></a></li>

                </ol>
            </div>
            <!-- /Breadcrumb -->

        </div>
        <!-- /Title -->

        <!-- Row -->
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default card-view">
                    <div class="panel-heading">
                        <div class="pull-left">
                            <h6 class="panel-title txt-dark"><?php echo $title; ?></h6>
                        </div>
                        <div class="pull-right">
                            <a href="<?php echo base_url('job'); ?>" class="btn btn-success btn-sm">View List</a>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body">
                            <p class="text-muted"><?php echo $description; ?>.</p>

                            <div class="row">
                                <div class="col-sm-12 col-xs-12">
                                    <div class="form-wrap">
                                        <?php
                                        //Show message
                                        echo $this->session->flashdata("msg");
                                        ?>
                                        <form method="post" enctype="multipart/form-data"
                                              action="<?= site_url($this->uri->segment(1) . '/payment_invoice') ?>"
                                              data-toggle="validator" role="form">
                                            <input type="hidden" value="<?= $this->uri->segment(3);?>" name="purchase_invoice_id">
                                            <div class="form-group col-md-4">
                                                <label class="control-label mb-10"
                                                       for="exampleInputuname_1">Inv No</label>

                                                <input type="text" class="form-control" name="inv_no"
                                                       placeholder="Inv No" readonly value="<?= $items[0]['inv_no'];?>">
                                            </div>

                                            <div class="form-group col-md-4">
                                                <label class="control-label mb-10"
                                                       for="exampleInputEmail_1">Subtotal</label>

                                                <input type="text" class="form-control" name="subtotal"
                                                       placeholder="Subtotal" readonly value="<?= $items[0]['sub_total'];?>">
                                            </div>

                                            <div class="form-group col-md-4">
                                                <label class="control-label mb-10"
                                                       for="exampleInputEmail_1">Discount</label>

                                                <input type="text" class="form-control" placeholder="Discount"
                                                          name="discount" readonly value="<?= $items[0]['discount'];?>">
                                            </div>

                                            <div class="form-group col-md-4">
                                                <label class="control-label mb-10"
                                                       for="exampleInputEmail_1">Payment Date</label>

                                                <input type="date" class="form-control"
                                                       name="payment_date"/>
                                            </div>

                                            <div class="form-group col-md-4">
                                                <label class="control-label mb-10"
                                                       for="exampleInputEmail_1">Payment Method</label>
                                                <select class="form-control" name="payment_method">
                                                    <option value="">Select Payment Method</option>
                                                    <option value="Cash">Cash</option>
                                                    <option value="Bank Transfer">Bank Transfer</option>
                                                    <option value="Cheque">Cheque</option>
                                                    <option value="Debit Card">Debit Card</option>
                                                    <option value="Credit Card">Credit Card</option>
                                                </select>
                                            </div>

                                            <div class="form-group col-md-4">
                                                <label class="control-label mb-10"
                                                       for="exampleInputEmail_1">Payment Amount</label>

                                                <input type="number" class="form-control" placeholder="Payment Amount"
                                                       name="payment_amount"/>
                                            </div>

                                            <div class="form-group col-md-4">
                                                <label class="control-label mb-10"
                                                       for="exampleInputEmail_1">Notes</label>

                                                <textarea class="form-control" name="notes"></textarea>
                                            </div>


                                            <div class="form-group col-md-10">


                                                <button type="submit" class="btn btn-success mr-10" name="btn_submit">
                                                    Submit
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12 col-xs-12">
                                    <div class="table-wrap mt-40">
                                        <div class="table-responsive">
                                            <table class="table mb-0" id="">
                                                <thead>
                                                <tr>
                                                    <th>Invoice No</th>
                                                    <th>Payment Date</th>
                                                    <th>Payment Method</th>
                                                    <th>Payment Amount</th>
                                                    <th>Note</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php foreach($payment_ivoice_list as $row):?>
                                                    <tr>
                                                        <td><?=$row['inv_no']?></td>
                                                        <td><?=$row['payment_date']?></td>
                                                        <td><?=$row['payment_method']?></td>
                                                        <td><?=$row['payment_amount']?></td>
                                                        <td><?=$row['note']?></td>
                                                    </tr>
                                                <?php endforeach; ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- /Row -->


        <!-- /Main Content -->