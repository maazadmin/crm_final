<!DOCTYPE html>
<html>
<head>
    <title></title>

    <style type="text/css">

        * { margin: 0; padding: 0; }
        body { font: 14px/1.4 Georgia, serif; }
        #page-wrap { width: 800px; margin: 0 auto; }

        textarea { border: 0; font: 14px Georgia, Serif; overflow: hidden; resize: none; }
        table { border-collapse: collapse; }
        table td, table th { border: 1px solid black; padding: 5px; }

        #header {     height: 15px;
            width: 100%;
            margin: 1px 0; background: #222; text-align: center; color: white; font: bold 15px Helvetica, Sans-Serif; text-decoration: uppercase; letter-spacing: 20px; padding: 8px 0px; }

        #address { width: 250px; height: 95px; float: left; }
        #customer { overflow: hidden; }

        .logo { text-align: right; float: right; position: relative; margin-top: 5px; border: 1px solid #fff; max-width: 540px; max-height: 100px; overflow: hidden; }
        #logo:hover, #logo.edit { border: 1px solid #000; margin-top: 0px; max-height: 125px; }
        #logoctr { display: none; }
        #logo:hover #logoctr, #logo.edit #logoctr { display: block; text-align: right; line-height: 25px; background: #eee; padding: 0 5px; }
        #logohelp { text-align: left; display: none; font-style: italic; padding: 10px 5px;}
        #logohelp input { margin-bottom: 5px; }
        .edit #logohelp { display: block; }
        .edit #save-logo, .edit #cancel-logo { display: inline; }
        .edit #image, #save-logo, #cancel-logo, .edit #change-logo, .edit #delete-logo { display: none; }
        #customer-title { font-size: 20px; font-weight: bold; float: left; }

        #meta { margin-top: 1px; width: 300px; float: right; }
        #meta td { text-align: right;  }
        #meta td.meta-head { text-align: left; background: #eee; }
        #meta td textarea { width: 100%; height: 20px; text-align: right; }

        #items { width: 100%; margin: 5px 0 0 0; border: 1px solid black; }
        #items th { background: #eee; }
        #items textarea { width: 80px; height: 40px; }
        #items tr.item-row td { border: 0; vertical-align: top; }
        #items td.description { width: 300px; }
        #items td.item-name { width: 175px; }
        #items td.description textarea, #items td.item-name textarea { width: 100%; }
        #items td.total-line { border-right: 0; text-align: right; }
        #items td.total-value { border-left: 0; padding: 10px; }
        #items td.total-value textarea { height: 20px; background: none; }
        #items td.balance { background: #eee; }
        #items td.blank { border: 0; }

        #terms { text-align: center; margin: 3px 0 0 0; }
        #terms h5 { text-transform: uppercase; font: 13px Helvetica, Sans-Serif; letter-spacing: 10px; border-bottom: 1px solid black; padding: 0 0 8px 0; margin: 0 0 8px 0; }
        #terms textarea { width: 100%; text-align: center;}

        textarea:hover, textarea:focus, #items td.total-value textarea:hover, #items td.total-value textarea:focus, .delete:hover { /* background-color:#EEFF88; */ }

        .delete-wpr { position: relative; }
        .delete { display: block; color: #000; text-decoration: none; position: absolute; background: #EEEEEE; font-weight: bold; padding: 0px 3px; border: 1px solid; top: -6px; left: -22px; font-family: Verdana; font-size: 12px; }

        #hiderow,
        .delete {
            display: none;
        }





        .center {
            margin: auto;
            width: 40%;
            border: 3px solid green;
            padding: 10px;
        }
        .remove-border
        {

            border: none;
        }
        .btn_back
        {

            border-width: 4px;
            font-size: 13px;
            padding: 4px 9px;
            line-height: 1.39;
            float: left;
            border-width: 4px;
            font-size: 13px;
            padding: 4px 9px;
            line-height: 1.39;
            background-color: #abbac3!important;
            border-color: #abbac3;
            color: #fff;
            /* margin-top: 10px; */
            padding: 10px;
            cursor: pointer;
            text-decoration:none !important;
        }
        .btn_print
        {
            border-width: 4px;
            font-size: 13px;
            padding: 4px 9px;
            line-height: 1.39;
            float: right;
            border-width: 4px;
            font-size: 13px;
            padding: 4px 9px;
            line-height: 1.39;
            background-color: #87b87f!important;
            border-color: #87b87f;
            color: #fff;
            /* margin-top: 10px; */
            padding: 10px;
            cursor: pointer;
            text-decoration:none !important;
        }

    </style>
</head>
<body>
<div Style="margin:10px 15px 0px 15px;" >
    <a href="<?php echo base_url(); ?>purchase_invoices" class="btn_back" name="" type="submit">
        <i class="icon-ok bigger-110"></i>
        Back
    </a>

    <a class="btn_print" onclick="printDiv('page-wrap1')" name="" type="submit">
        <i class="icon-ok bigger-110"></i>
        Print
    </a>
    <div style="clear:both;"></div>
</div>
<br>
<div id="page-wrap1">


    <div style="width: 100%; ">
        <div>
            <h1 style=" font-family: 'Times New Roman', Times, serif; text-align: center; padding: .2in .1in; color: #438eb9;">Purchase invoices </h1>
            <p style="text-align: center;padding-top: -40px;width: 50%;
    margin-left: 25%;"><?php echo $address;?>Suite # A - 147, 1st Floor, Bhayani Shopping Centre, Near 5 Star Chowrangi, Opp. Farooq-e-Azam Masjid, North Nazimabad, Karachi, Pakistan.</p>
        </div>
    </div>


    <div class="wrapper ap" style=" margin: 10px auto;
    padding: .2in .1in;
    min-height: 8in;
    max-width: 8in;
   
    background-color: #FFF;
    font-size: 13px;
    font-family: 'Times New Roman', Times, serif;
    padding-bottom: 0px;">



        <!-- <table style="width: 100%">
               <tr>
                 <td style="background:#438eb9;" class="panel-heading">
                   <h3 style="margin:0 10px;padding:0;color:#fff;" class="panel-title">Your Item Name:</h3>
                 </td>
               </tr>
             </table>  -->
        <div class="panel panel-primary" style="">

            <div style="font-family: 'Lucida Sans Unicode', 'Lucida Grande', sans-serif; font-size: medium;">
                <div style="width: 100%">
                    <div class="panel-body legal" style="margin-top:0px;float: left;width: 50%">
                        <strong>Inv No: </strong><?php echo $items_new2[0]['inv_no'];?>
                    </div>

                    <div class="panel-body legal" style="width: 50%;text-align: right;float: left;">
                        <strong>Date: </strong><?php echo date('Y-m-d');?>
                    </div>
                </div>




                <div class="panel-body legal" style="width: 100%;float: left;">
                    <strong>Supplier: </strong><?php echo $items_new2[0]['title'];?>
                </div>

                <div class="panel-body legal" style="width: 100%;float: left;">
                    <strong>Purchase order reference: </strong><?php echo $items_new2[0]['purchase_order_reference'];?>
                </div>
            </div>
        </div>

        <table class="detailtable" style="width:100%; margin-top:20px; text-align: center;">
            <thead>
            <tr style="padding:5px; font-size:16px; font-family:'Times New Roman';">
                <th>S.NO#</th>
                <th class="col-md-2">Product Name: </th>
                <th>Product Description:</th>
                <th>Order Quantity:</th>
                <th>Unit Price:</th>
                <th>Amount:</th>
            </tr>
            <tbody>
            <?php
            $i = 0;
            $subtotal = 0;
            $total_qty = 0;
            foreach ($items as $item) {
            $i += 1;
                $subtotal += $item['line_total'];
                $total_qty += $item['product_quantity'];
            ?>

                <tr style=" padding:5px; font-size:14px; font-family:'Times New Roman'; color:#111; border-bottom:2px solid #5F5F5F; text-align: center;">
                    <td class="col-md-2"><?php echo $i;?></td>
                    <td> <?php echo $item['product_name'];?></td>
                    <td> <?php echo $item['product_description'];?> </td>
                    <td> <?php echo $item['product_quantity'];?>	</td>
                    <td> <?php echo $item['unit_price'];?></td>
                    <td> <?php echo $item['line_total'];?>	</td>
                </tr>
            <?php } ?>
            <tr style=" padding:5px; font-size:14px; font-family:'Times New Roman'; color:#111; border-bottom:2px solid #5F5F5F; text-align: center;">
                <td class="col-md-2"></td>
                <td> 	</td>
                <td> <B>Total</B> </td>
                <td> <?php echo $total_qty;?>	</td>
                <td> </td>
                <td> <?php echo $subtotal;?></td>
            </tr>



            </tbody>
        </table>

        <br><br>
        <div class="panel panel-primary" style=" text-align: right;">
            <!-- <div style="background:#438eb9; text-align: right;" class="panel-heading">
                <h3 style="margin:0 10px;padding:0;color:#fff; " class="panel-title">Total:</h3>
             </div> -->
            <div style="font-family: 'Lucida Sans Unicode', 'Lucida Grande', sans-serif; font-size: medium;">
                <!-- <div class="panel-body legal" style="margin-top:10px;">
                    <strong>Total Amount: </strong>2000
                </div>
                                <br/> -->
                <!--<div class="panel-body legal" style="margin-top:10px;text-align: left;">
                    <strong>Final Balance: </strong><?php /*echo $final_balance;*/?>
                </div>-->
                <div class="panel-body legal" style="margin-top:10px;">
                    <strong>Signature: </strong>_____________________
                </div>

            </div>
        </div>
    </div>

</div>
</body>
</html>
<script type="text/javascript">
    function printDiv(divName) {
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;

        document.body.innerHTML = printContents;

        window.print();

        document.body.innerHTML = originalContents;
    }
</script>