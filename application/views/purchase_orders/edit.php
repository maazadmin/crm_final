<!-- Main Content -->
<div class="page-wrapper">
    <div class="container-fluid">

        <!-- Title -->
        <div class="row heading-bg">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h5 class="txt-dark"><?= $heading ?></h5>
            </div>

            <!-- Breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="index.html">Dashboard</a></li>
                    <li><a href="active"><span><?= $heading ?></span></a></li>

                </ol>
            </div>
            <!-- /Breadcrumb -->

        </div>
        <!-- /Title -->

        <!-- Row -->
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default card-view">
                    <div class="panel-heading">
                        <div class="pull-left">
                            <h6 class="panel-title txt-dark"><?php echo $title; ?></h6>
                        </div>
                        <div class="pull-right">
                            <a href="<?= site_url($this->uri->segment(1)) ?>" class="btn btn-success btn-sm">View
                                List</a>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body">
                            <p class="text-muted"><?php echo $description; ?>.</p>

                            <div class="row">
                                <div class="col-sm-12 col-xs-12">
                                    <div class="form-wrap">
                                        <?php
                                        //Show message
                                        echo $this->session->flashdata("msg");
                                        ?>


                                        <form method="post" enctype="multipart/form-data"
                                              action="<?= site_url($this->uri->segment(1) . '/edit/' . (($item !== false) ? $item[$pmKey] : "")) ?>"
                                              data-toggle="validator" role="form">
                                            <div class="row">
                                                <div class="form-group col-md-4">
                                                    <label class="control-label mb-10"
                                                           for="exampleInputuname_1">PO #</label>

                                                    <input type="text" class="form-control"
                                                           name="purchase_order_reference"
                                                           placeholder="Purchase Order Reference" required
                                                           value="<?php echo ($item !== false) ? $item['purchase_order_reference'] : ""; ?>">
                                                </div>

                                                <div class="form-group col-md-4">
                                                    <label class="control-label mb-10"
                                                           for="exampleInputEmail_1">Supplier</label>

                                                    <select class="form-control" name="supplier">
                                                        <option>Select Supplier</option>
                                                        <?php foreach ($suppliers_list as $row) :
                                                            if ($row['supplier_id'] == $item['supplier_id']) {
                                                                $selected = "selected";
                                                            } else {
                                                                $selected = "";
                                                            }
                                                            ?>

                                                            <option
                                                                value="<?= $row['supplier_id']; ?>" <?= $selected; ?>><?= $row['title']; ?></option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                                <!-- <h1><?php /*echo ($item!== false)?$item['created_at']: ""; */ ?></h1>-->

                                                <div class="form-group col-md-4">
                                                    <label class="control-label mb-10"
                                                           for="exampleInputEmail_1">PO Date</label>

                                                    <input type="date" class="form-control" name="po_date"
                                                           value="<?php echo ($item !== false) ? $item['created_at'] : ""; ?>">
                                                </div>

                                            </div>

                                            <div class="row">

                                                <!--<div class="form-group col-md-4">
                                                    <label class="control-label mb-10"
                                                           for="exampleInputEmail_1">Delivery address</label>

                                                    <textarea class="form-control" name="delivery_address"></textarea>
                                                </div>-->

                                                <div class="form-group col-md-4">
                                                    <label class="control-label mb-10"
                                                           for="exampleInputEmail_1">Job Ref #</label>

                                                    <select class="form-control" name="job_order_id">
                                                        <option value="">Select Job Ref</option>
                                                        <?php foreach ($job_list as $row) :
                                                            if ($row['job_id'] == $item['job_order_id']) {
                                                                $selected = "selected";
                                                            } else {
                                                                $selected = "";
                                                            }
                                                            ?>

                                                            <option
                                                                value="<?= $row['job_id']; ?>" <?= $selected; ?>><?= $row['job_ref']; ?></option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>

                                            </div>


                                            <div class="container">
                                                <div class="panel panel-default" id="div_add_size">

                                                    <div class="panel-heading">
                                                        <a data-toggle="collapse" data-target="#deal" href="#">Item
                                                            Details</a>
                                                    </div>
                                                    <div id="deal" class="panel-body">
                                                        <div id="payment" class="panel-body">
                                                            <div class="col-md-12">
                                                                <table class="table-responsive table-condensed">
                                                                    <thead>
                                                                    <tr>
                                                                        <th>Product</th>
                                                                        <th>Description</th>
                                                                        <th>Unit Price</th>
                                                                        <th>Qty</th>
                                                                        <th>Amount</th>
                                                                    </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                    <tr>


                                                                        <td>
                                                                            <select class="form-control"
                                                                                    name="product_id" id="product_id">
                                                                                <option value="">Select Product</option>
                                                                                <?php foreach ($product_list as $row) : ?>

                                                                                    <option
                                                                                        value="<?= $row['product_id']; ?>"><?= $row['product_name']; ?></option>
                                                                                <?php endforeach; ?>
                                                                            </select>

                                                                            <!--  <input type="text" id="size" name="size" style=""  class="form-control"  placeholder="Registration No" > -->
                                                                            <input type="hidden" id="p_name"
                                                                                   name="p_name">

                                                                        </td>
                                                                        <td><input type="text" class="form-control"
                                                                                   id="description" name="description"
                                                                                   placeholder=""></td>
                                                                        <td><input type="number" class="form-control"
                                                                                   id="unit_price" name="unit_price"
                                                                                   placeholder="" readonly></td>
                                                                        <td><input type="number" class="form-control"
                                                                                   id="qty" name="qty" placeholder=""
                                                                                   value="1">
                                                                        </td>
                                                                        <td><input type="number" class="form-control"
                                                                                   readonly id="amount" name="amount"
                                                                                   placeholder=""></td>
                                                                    </tr>

                                                                    </tbody>

                                                                </table>
                                                                <div class="pull-right">
                                                                    <a href="#" id="insert" class="btn btn-primary">Add
                                                                        Item</a>
                                                                    <!--  <a href="#" id="test" class="btn btn-primary"> Test</a> -->
                                                                </div>
                                                                <div style="clear:both;"></div>

                                                                <div id="ccinfo"
                                                                     class="col-md-12 <?php if (!empty($item_list)) {
                                                                         echo "";
                                                                     } else {
                                                                         echo "hidden";
                                                                     } ?>">

                                                                    <table id="cctable"
                                                                           class="table-responsive table-condensed">
                                                                        <thead>
                                                                        <tr>
                                                                            <th style="width: 50px;">Action</th>

                                                                            <th>Product</th>
                                                                            <th>Description</th>
                                                                            <th>Unit Price</th>
                                                                            <th>Qty</th>
                                                                            <th>Amount</th>
                                                                        </tr>


                                                                        </thead>
                                                                        <tbody>

                                                                        <?php
                                                                        if (!empty($item_list)) {
                                                                            // $size_list = $this->Mdl_product->get_size_list_by_product_id($row[0]->id);
                                                                            foreach ($item_list as $list):
                                                                                ?>
                                                                                <tr>
                                                                                    <td><input type="checkbox"
                                                                                               class="record"
                                                                                               name="record" data_id="<?= $list['purchase_order_item_id']; ?>">
                                                                                    </td>
                                                                                    <td><input type="hidden"
                                                                                               name="product_id_new[]"
                                                                                               value="<?= $list['product_id']; ?>">
                                                                                        <input type="hidden"
                                                                                               name="item_id[]"
                                                                                               value="<?= $list['purchase_order_item_id']; ?>">
                                                                                        <input type="hidden"
                                                                                               name="check_product_id_new"
                                                                                               value="<?= $list['product_id']; ?>">
                                                                                        <input
                                                                                            type="text"
                                                                                            class="form-control"
                                                                                            readonly
                                                                                            name="product_name_new[]"
                                                                                            value="<?= $list['product_name']; ?>">
                                                                                    </td>
                                                                                    <td><input type="text"
                                                                                               class="form-control"
                                                                                               name="desc_new[]"
                                                                                               value="<?= $list['item_description']; ?>">
                                                                                    </td>
                                                                                    <td><input type="text"
                                                                                               class="form-control unit_price_new"

                                                                                               name="unit_price_new[]"
                                                                                               value="<?= $list['product_unit_price']; ?>"
                                                                                               data_id="<?= $list['purchase_order_item_id']; ?>"
                                                                                               id="unit_price_new_<?= $list['purchase_order_item_id'] ?>">
                                                                                    </td>
                                                                                    <td><input type="text"
                                                                                               class="form-control qty_new"
                                                                                               name="qty_new[]"
                                                                                               value="<?= $list['ordered_quantity']; ?>"
                                                                                               data_id="<?= $list['purchase_order_item_id']; ?>"
                                                                                               id="qty_new_<?= $list['purchase_order_item_id'] ?>">
                                                                                    </td>
                                                                                    <td><input type="text"
                                                                                               class="form-control amount_new"

                                                                                               name="amount_new[]"
                                                                                               value="<?= $list['line_item_total_price']; ?>"
                                                                                               data_id="<?= $list['purchase_order_item_id']; ?>"
                                                                                               id="amount_new_<?= $list['purchase_order_item_id'] ?>"
                                                                                               readonly>
                                                                                    </td>
                                                                                </tr>
                                                                                <?php
                                                                            endforeach;
                                                                        } ?>
                                                                        </tbody>
                                                                    </table>
                                                                    <button type="button" id="delete-row"
                                                                            class="delete-row btn btn-danger btn-xs">
                                                                        Delete Record
                                                                    </button>
                                                                </div>

                                                            </div>
                                                        </div>


                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="form-group col-md-4">
                                                    <label class="control-label mb-10"
                                                           for="exampleInputuname_1">Remarks</label>

                                                    <textarea class="form-control"
                                                              name="remarks"><?php echo ($item !== false) ? $item['remarks'] : ""; ?></textarea>
                                                </div>

                                                <div class="form-group col-md-4">
                                                    <label class="control-label mb-10"
                                                           for="exampleInputuname_1">Tax</label>

                                                    <input type="text" class="form-control" name="tax"
                                                           value="<?php echo ($item !== false) ? $item['tax'] : ""; ?>">
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label class="control-label mb-10"
                                                           for="exampleInputuname_1">Shipping Method</label>

                                                    <input type="text" class="form-control" name="shipping_method"
                                                           value="<?php echo ($item !== false) ? $item['shipping_method'] : ""; ?>">
                                                </div>

                                            </div>
                                            <div class="row">
                                                <div class="form-group col-md-4">
                                                    <label class="control-label mb-10"
                                                           for="exampleInputuname_1">Payment Terms</label>

                                                    <input type="text" class="form-control" name="payment_terms"
                                                           value="<?php echo ($item !== false) ? $item['payment_terms'] : ""; ?>">
                                                </div>

                                                <div class="form-group col-md-4">
                                                    <label class="control-label mb-10"
                                                           for="exampleInputuname_1">Delivery Date</label>

                                                    <input type="date" class="form-control" name="delivery_date"
                                                           value="<?php echo ($item !== false) ? $item['deliverydate'] : ""; ?>">
                                                </div>

                                                <div class="form-group col-md-4">
                                                    <label class="control-label mb-10"
                                                           for="exampleInputuname_1">Total Amount</label>

                                                    <input type="text" name="total_amount" id="total_amount" class="form-control" value="<?php echo ($item !== false) ? $item['total_amount'] : ""; ?>" readonly>
                                                </div>
                                            </div>

                                            <div class="form-group col-md-10">


                                                <button type="submit" class="btn btn-success mr-10" name="btn_submit">
                                                    Submit
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
                <!-- /Row -->

            </div>
            <!-- /Main Content -->
