<!-- Main Content -->
<div class="page-wrapper">
    <div class="container-fluid">

        <!-- Title -->
        <div class="row heading-bg">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h5 class="txt-dark"><?= $heading ?></h5>
            </div>

            <!-- Breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="index.html">Dashboard</a></li>
                    <li><a href="active"><span><?= $heading ?></span></a></li>

                </ol>
            </div>
            <!-- /Breadcrumb -->

        </div>
        <!-- /Title -->

        <!-- Row -->
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default card-view">
                    <div class="panel-heading">
                        <div class="pull-left">
                            <h6 class="panel-title txt-dark"><?php echo $title; ?></h6>
                        </div>
                        <!--<div class="pull-right">
                            <a href="<?/*= site_url($this->uri->segment(1)) */?>" class="btn btn-success btn-sm">View
                                List</a>
                        </div>-->
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body">
                            <p class="text-muted"><?php echo $description; ?>.</p>

                            <div class="row">
                                <div class="col-sm-12 col-xs-12">
                                    <div class="form-wrap">
                                        <?php
                                        //Show message
                                        echo $this->session->flashdata("msg");
                                        ?>
                                        <form method="post" class="ac-type-form" enctype="multipart/form-data"
                                              action="<?=base_url()."/Report/balance_sheet"?>" data-toggle="validator" role="form">
                                            <div class="row">
                                                <div class="form-group col-md-4">
                                                    <label class="control-label mb-10">Date From</label>
                                                    <input type="date" class="form-control" name="date_from">
                                                </div>
												<div class="form-group col-md-4">
                                                    <label class="control-label mb-10">Date To</label>
                                                    <input type="date" class="form-control" name="date_to">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group col-md-10">
                                                    <button type="submit" class="btn btn-success mr-10" name="btn_submit">
                                                        Generate Report
                                                    </button>
                                                </div>
                                            </div>


                                        </form>

                                        <div>
                                            <button type="button" class="btn btn-primary hide2"
                                                    onclick="printDiv('printDiv');">PRINT
                                            </button>
                                            <br/><br/>

                                            <div id="printDiv">
                                                <div class="row" style="text-align: center; font-weight: bold;">
                                                    <br>
                                                    <strong>
                                                        Balance Sheet
                                                    </strong>
                                                    <br>
                                                </div>
                                                <div class="row">
                                                    <div class="container">
                                                        <b style="font-weight: 700">Assets</b>
                                                        <table class="table table-striped table-bordered">
                                                            <thead>
                                                            <tr>
                                                                <th style="text-align:left"></th>
                                                                <th style="text-align:right"></th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <?php
                                                            $total_assets = 0;
                                                            foreach($asset_report as $row_asset):
                                                                $total_assets += $row_asset['amount'];
                                                                ?>
                                                            <tr >
                                                                <td style="width: 65%;padding-left: 40px;"><?php echo $row_asset['gl_account_name'];?></td>
                                                                <td><?php echo $row_asset['amount'];?></td>
                                                            </tr>
                                                            <?php endforeach; ?>

                                                            <tr style="border-top:2px solid #ddd">
                                                                <td style="text-align:left;font-weight: bold;">Total
                                                                    Assets
                                                                </td>
                                                                <td style="text-align:left; font-weight: bold;"
                                                                    ><?=$total_assets;?>
                                                                </td>
                                                            </tr>

                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="container">
                                                        <b style="font-weight: 700">Liabilities</b>
                                                        <table class="table table-striped table-bordered">
                                                            <thead>
                                                            <tr>
                                                                <th style="text-align:left"></th>
                                                                <th style="text-align:right"></th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <?php
                                                            $total_liabilities = 0;
                                                            foreach($liability_report as $row_liablility):
                                                                $total_liabilities += $row_liablility['amount'];
                                                                ?>
                                                                <tr>
                                                                    <td style="width: 65%;padding-left: 40px;"><?php echo $row_liablility['gl_account_name'];?></td>
                                                                    <td><?php echo $row_liablility['amount'];?></td>
                                                                </tr>
                                                            <?php endforeach; ?>
                                                            <tr style="border-top:2px solid #ddd">
                                                                <td style="text-align:left;font-weight: bold;width: 65%;">Total
                                                                    Liabilities
                                                                </td>
                                                                <td style="text-align:left; font-weight: bold;"
                                                                    ><?=$total_liabilities;?>
                                                                </td>
                                                            </tr>

                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="container">
                                                        <b style="font-weight: 700">Equity</b>
                                                        <table class="table table-striped table-bordered">
                                                            <thead>
                                                            <tr>
                                                                <th style="text-align:left"></th>
                                                                <th style="text-align:right"></th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <?php
                                                            $total_equity = 0;
                                                            foreach($equity_report as $row_equity):
                                                                $total_equity += $row_equity['amount'];
                                                                ?>
                                                                <tr>
                                                                    <td style="width: 65%;padding-left: 40px;"><?php echo $row_equity['gl_account_name'];?></td>
                                                                    <td><?php echo $row_equity['amount'];?></td>
                                                                </tr>
                                                            <?php endforeach; ?>
                                                            <tr style="border-top:2px solid #ddd">
                                                                <td style="text-align:left;font-weight: bold;width: 65%;">Total
                                                                    Liabilities & Equity
                                                                </td>
                                                                <td style="text-align:left; font-weight: bold;"
                                                                    ><?=$total_equity;?>
                                                                </td>
                                                            </tr>

                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
                <!-- /Row -->

            </div>
            <!-- /Main Content -->
            <!-- <script>
                 //Form Validation A/C Type

                 $('.ac-type-form').validate(
                     {

                         errorElement: 'div',
                         errorClass: 'help-block',
                         focusInvalid: false,
                         rules: {
                             //Personal Info

                             account_type: {required: true},

                         },

                         messages: {

                             account_type: " Please enter Account Type",


                         },
                         invalidHandler: function (event, validator) { //display error alert on form submit
                             $('.alert-danger', $('.add_new_ajeer')).show();
                         },

                         highlight: function (e) {
                             $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
                         },

                         success: function (e) {
                             $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
                             $(e).remove();
                         },
                         errorPlacement: function (error, element) {
                             error.insertAfter(element.parent());
                         },

                     });
             </script>-->