<!-- Main Content -->
<div class="page-wrapper">
    <div class="container-fluid">

        <!-- Title -->
        <div class="row heading-bg">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h5 class="txt-dark"><?= $heading ?></h5>
            </div>

            <!-- Breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="index.html">Dashboard</a></li>
                    <li><a href="active"><span><?= $heading ?></span></a></li>

                </ol>
            </div>
            <!-- /Breadcrumb -->

        </div>
        <!-- /Title -->

        <!-- Row -->
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default card-view">
                    <div class="panel-heading">
                        <div class="pull-left">
                            <h6 class="panel-title txt-dark"><?php echo $title; ?></h6>
                        </div>
                        <!--<div class="pull-right">
                            <a href="<?/*= site_url($this->uri->segment(1)) */?>" class="btn btn-success btn-sm">View
                                List</a>
                        </div>-->
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body">
                            <p class="text-muted"><?php echo $description; ?>.</p>

                            <div class="row">
                                <div class="col-sm-12 col-xs-12">
                                    <div class="form-wrap">
                                        <?php
                                        //Show message
                                        echo $this->session->flashdata("msg");
                                        ?>
                                        <form method="post" class="ac-type-form" enctype="multipart/form-data"
                                              action="#" data-toggle="validator" role="form">
                                            <div class="row">
                                                <div class="form-group col-md-4">
                                                    <label class="control-label mb-10" for="exampleInputuname_1">Select
                                                        Dates</label>

                                                    <input type="date" class="form-control" id="date"
                                                           name="account_type" placeholder="Account Type">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group col-md-10">


                                                    <button type="submit" class="btn btn-success mr-10"
                                                            name="btn_submit">
                                                        Get
                                                    </button>
                                                </div>
                                            </div>


                                        </form>

                                        <div>
                                            <button type="button" class="btn btn-primary hide2"
                                                    onclick="printDiv('printDiv');">PRINT
                                            </button>
                                            <br/><br/>

                                            <div id="printDiv">
                                                <div class="row" style="text-align: center; font-weight: bold;">
                                                    <br>
                                                    <strong>
                                                        Income Statement
                                                    </strong>
                                                    <br>
                                                </div>
                                                <div class="row">
                                                    <div class="container">
                                                        <b style="font-weight: 700">Income</b>
                                                        <table class="table table-striped table-bordered">
                                                            <thead>
                                                            <tr>
                                                                <th style="text-align:left"></th>
                                                                <th style="text-align:right"></th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>

                                                            <?php
                                                            $total_income = 0;
                                                            foreach($income_report as $row_income):
                                                                $total_income += $row_income['amount'];
                                                                ?>
                                                                <tr>
                                                                    <td style="width: 70%;padding-left: 40px;"><?php echo $row_income['gl_account_name'];?></td>
                                                                    <td><?php echo $row_income['amount'];?></td>
                                                                </tr>
                                                            <?php endforeach; ?>

                                                            <tr style="border-top:2px solid #ddd">
                                                                <td style="text-align:left;font-weight: bold;">Total
                                                                    Income
                                                                </td>
                                                                <td style="text-align:left; font-weight: bold;"
                                                                    ><?=$total_income;?>
                                                                </td>
                                                            </tr>

                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="container">
                                                        <b style="font-weight: 700">Expense</b>
                                                        <table class="table table-striped table-bordered">
                                                            <thead>
                                                            <tr>
                                                                <th style="text-align:left"></th>
                                                                <th style="text-align:right"></th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>

                                                            <?php
                                                            $total_expense = 0;
                                                            foreach($expense_report as $row_expense):
                                                                $total_expense += $row_expense['amount'];
                                                                ?>
                                                                <tr>
                                                                    <td style="width: 70%;padding-left: 40px;"><?php echo $row_expense['gl_account_name'];?></td>
                                                                    <td><?php echo $row_expense['amount'];?></td>
                                                                </tr>
                                                            <?php endforeach; ?>


                                                            <tr style="border-top:2px solid #ddd">
                                                                <td style="text-align:left;font-weight: bold;">Total
                                                                    Expense
                                                                </td>

                                                                <td style="text-align:left; font-weight: bold;"
                                                                    ><?=$total_expense;?>
                                                                </td>
                                                            </tr>

                                                           <!-- <tr style="border-top:2px solid #ddd">
                                                                <td style="text-align:left;font-weight: bold;">Net Income
                                                                </td>
                                                                <td style="text-align:left; font-weight: bold;"
                                                                    ><?/*=$total_income - $total_expense;*/?>
                                                                </td>
                                                            </tr>-->

                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="container">
                                                        <table class="table table-striped table-bordered">
                                                            <thead>
                                                            <tr>
                                                                <th style="text-align:left"></th>
                                                                <th style="text-align:right"></th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>


                                                           <!-- <tr>
                                                                <td></td>
                                                                <td></td>
                                                            </tr>-->


                                                            <tr style="border-top:2px solid #ddd">
                                                                <td style="text-align:left;font-weight: bold;width: 70%;">Net Income
                                                                </td>
                                                                <td style="text-align:left; font-weight: bold;"
                                                                    ><?=$total_income - $total_expense;?>
                                                                </td>
                                                            </tr>

                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
                <!-- /Row -->

            </div>
            <!-- /Main Content -->
            <!-- <script>
                 //Form Validation A/C Type

                 $('.ac-type-form').validate(
                     {

                         errorElement: 'div',
                         errorClass: 'help-block',
                         focusInvalid: false,
                         rules: {
                             //Personal Info

                             account_type: {required: true},

                         },

                         messages: {

                             account_type: " Please enter Account Type",


                         },
                         invalidHandler: function (event, validator) { //display error alert on form submit
                             $('.alert-danger', $('.add_new_ajeer')).show();
                         },

                         highlight: function (e) {
                             $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
                         },

                         success: function (e) {
                             $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
                             $(e).remove();
                         },
                         errorPlacement: function (error, element) {
                             error.insertAfter(element.parent());
                         },

                     });
             </script>-->