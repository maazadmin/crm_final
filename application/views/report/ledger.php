<!-- Main Content -->
<div class="page-wrapper">
    <div class="container-fluid">

        <!-- Title -->
        <div class="row heading-bg">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h5 class="txt-dark"><?= $heading ?></h5>
            </div>

            <!-- Breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="index.html">Dashboard</a></li>
                    <li><a href="active"><span><?= $heading ?></span></a></li>

                </ol>
            </div>
            <!-- /Breadcrumb -->

        </div>
        <!-- /Title -->

        <!-- Row -->
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default card-view">
                    <div class="panel-heading">
                        <div class="pull-left">
                            <h6 class="panel-title txt-dark"><?php echo $title; ?></h6>
                        </div>
                        <!--<div class="pull-right">
                            <a href="<?/*= site_url($this->uri->segment(1)) */?>" class="btn btn-success btn-sm">View
                                List</a>
                        </div>-->
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body">
                            <p class="text-muted"><?php echo $description; ?>.</p>

                            <div class="row">
                                <div class="col-sm-12 col-xs-12">
                                    <div class="form-wrap">
                                        <?php
                                        //Show message
                                        echo $this->session->flashdata("msg");
                                        ?>
                                        <form method="post" class="ac-type-form" enctype="multipart/form-data"
                                              action="#" data-toggle="validator" role="form">
                                            <div class="row">
                                                <div class="form-group col-md-4">
                                                    <label class="control-label mb-10" for="exampleInputuname_1">Account</label>

                                                   <select class="form-control">
                                                       <option value="">Select Account</option>
                                                   </select>
                                                </div>

                                                <div class="form-group col-md-4">
                                                    <label class="control-label mb-10" for="exampleInputuname_1">Select
                                                        Dates</label>

                                                    <input type="date" class="form-control" id="date"
                                                           name="account_type" placeholder="Account Type">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group col-md-10">


                                                    <button type="submit" class="btn btn-success mr-10"
                                                            name="btn_submit">
                                                        Get
                                                    </button>
                                                </div>
                                            </div>


                                        </form>

                                        <div>
                                            <button type="button" class="btn btn-primary hide2"
                                                    onclick="printDiv('printDiv');">PRINT
                                            </button>
                                            <br/><br/>

                                            <div id="printDiv">
                                                <div class="row" style="text-align: center; font-weight: bold;">
                                                    <br>
                                                   <!-- <strong>
                                                        Cash Ledger
                                                    </strong>-->
                                                    <br>
                                                </div>
                                                <div class="row">
                                                    <div class="container">

                                                        <table class="table table-striped table-bordered">
                                                            <thead>
                                                            <tr>
                                                                <th style="width: 117px;">Date</th>
                                                                <th style="width: 500px;">Particular</th>
                                                                <th style="width: 90px;">J.R</th>
                                                                <th >Debit</th>
                                                                <th >Credit</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>

                                                            <tr >
                                                                <td>1-2-2019</td>
                                                                <td>A/C</td>
                                                                <td></td>
                                                                <td>0.00</td>
                                                                <td>0.00</td>
                                                            </tr>
                                                            <tr>
                                                                <td></td>
                                                                <td style=" font-weight: bold;text-align: right;">Total</td>
                                                                <td ></td>
                                                                <td></td>
                                                                <td></td>
                                                            </tr>
                                                            <tr>
                                                                <td></td>
                                                                <td style="font-weight: bold;text-align: right;">By Balance b/d</td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                            </tr>

                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
                <!-- /Row -->

            </div>
            <!-- /Main Content -->
            <!-- <script>
                 //Form Validation A/C Type

                 $('.ac-type-form').validate(
                     {

                         errorElement: 'div',
                         errorClass: 'help-block',
                         focusInvalid: false,
                         rules: {
                             //Personal Info

                             account_type: {required: true},

                         },

                         messages: {

                             account_type: " Please enter Account Type",


                         },
                         invalidHandler: function (event, validator) { //display error alert on form submit
                             $('.alert-danger', $('.add_new_ajeer')).show();
                         },

                         highlight: function (e) {
                             $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
                         },

                         success: function (e) {
                             $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
                             $(e).remove();
                         },
                         errorPlacement: function (error, element) {
                             error.insertAfter(element.parent());
                         },

                     });
             </script>-->