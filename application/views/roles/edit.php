<!-- Main Content -->
<div class="page-wrapper">
    <div class="container-fluid">

        <!-- Title -->
        <div class="row heading-bg">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h5 class="txt-dark"><?= $heading ?></h5>
            </div>

            <!-- Breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="index.html">Dashboard</a></li>
                    <li><a href="active"><span><?= $heading ?></span></a></li>

                </ol>
            </div>
            <!-- /Breadcrumb -->

        </div>
        <!-- /Title -->

        <!-- Row -->
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default card-view">
                    <div class="panel-heading">
                        <div class="pull-left">
                            <h6 class="panel-title txt-dark"><?php echo $title; ?></h6>
                        </div>
                        <div class="pull-right">
                            <a href="<?= site_url($this->uri->segment(1)) ?>" class="btn btn-success btn-sm">View
                                List</a>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body">
                            <p class="text-muted"><?php echo $description; ?>.</p>

                            <div class="row">
                                <div class="col-sm-12 col-xs-12">
                                    <div class="form-wrap">
                                        <?php
                                        //Show message
                                        echo $this->session->flashdata("msg");
                                        ?>
                                        <form method="post" class="ac-type-form" enctype="multipart/form-data"
                                              action="<?= site_url($this->uri->segment(1) . '/edit/' . (($item !== false) ? $item[$pmKey] : "")) ?>"
                                              data-toggle="validator" role="form">
                                            <div class="row">
                                                <div class="form-group col-md-4">
                                                    <label class="control-label mb-10" for="exampleInputuname_1">Role
                                                        Name
                                                    </label>

                                                    <input type="text" class="form-control" id="role_name"
                                                           name="role_name" placeholder="Role Name" required
                                                           value="<?php echo ($item !== false) ? $item['role_name'] : ""; ?>">
                                                </div>


                                            </div>
                                            <div class="row">
                                                <div class="container">
                                                    <div class="panel panel-default" id="div_add_size">

                                                        <div class="panel-heading">
                                                            <a data-toggle="collapse" data-target="#deal" href="#">Module
                                                                List</a>
                                                        </div>
                                                        <div id="deal" class="panel-body">
                                                            <div id="payment" class="panel-body">
                                                                <div class="col-md-12">
                                                                    <table class="table-responsive table-condensed">
                                                                        <thead>
                                                                        <tr>
                                                                            <th>Module</th>
                                                                        </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                        <tr>

                                                                            <td>
                                                                                <select class="form-control"
                                                                                        name="nav_id" id="nav_id">
                                                                                    <option value="">Select Module
                                                                                    </option>
                                                                                    <?php foreach ($navigation_list as $row) : ?>

                                                                                        <option
                                                                                            value="<?= $row['navigation_id'] ?>"><?= $row['nav_name'] ?></option>
                                                                                    <?php endforeach; ?>
                                                                                </select>

                                                                                <input type="hidden" id="nav_name"
                                                                                       name="nav_name">

                                                                            </td>

                                                                        </tr>

                                                                        </tbody>

                                                                    </table>
                                                                    <div class="pull-right">
                                                                        <a href="#" id="insert_nav"
                                                                           class="btn btn-primary">Add
                                                                            Module</a>
                                                                        <!--  <a href="#" id="test" class="btn btn-primary"> Test</a> -->
                                                                    </div>
                                                                    <div style="clear:both;"></div>

                                                                    <div id="nav-table"
                                                                         class="col-md-12 <?php if (!empty($role_navigation)) {
                                                                             echo "";
                                                                         } else {
                                                                             echo "hidden";
                                                                         } ?>">

                                                                        <table id="cctable"
                                                                               class="table-responsive table-condensed">
                                                                            <thead>
                                                                            <tr>
                                                                                <th style="width: 50px;">Action</th>

                                                                                <th>Module ID</th>
                                                                                <th>Module Name</th>
                                                                            </tr>


                                                                            </thead>
                                                                            <tbody>

                                                                            <?php

                                                                            foreach ($role_navigation as $list):
                                                                                ?>
                                                                                <tr>
                                                                                    <td><input type="checkbox"
                                                                                               class="record"
                                                                                               name="record" data_id="<?=$list['role_navigation_id']?>"></td>
                                                                                    <td><input type="hidden"
                                                                                               name="role_navigation_id[]"
                                                                                                value="<?=$list['role_navigation_id']?>">
                                                                                        <input type="text" readonly=""
                                                                                               class="form-control"
                                                                                               name="nav_id_new[]"
                                                                                               value="<?= $list['navigation_id'] ?>"><input
                                                                                            type="hidden"
                                                                                            name="check_nav_id_new"
                                                                                            value="<?= $list['navigation_id'] ?>">
                                                                                    </td>
                                                                                    <td><input type="text"
                                                                                               class="form-control"
                                                                                               readonly=""
                                                                                               name="nav_name_new[]"
                                                                                               value="<?= $list['nav_name'] ?>">
                                                                                    </td>
                                                                                </tr>
                                                                                <?php
                                                                            endforeach;
                                                                            ?>
                                                                            </tbody>
                                                                        </table>
                                                                        <button type="button" id="delete-row-nav"
                                                                                class="delete-row-nav btn btn-danger btn-xs">
                                                                            Delete Record
                                                                        </button>
                                                                    </div>

                                                                </div>
                                                            </div>


                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="form-group col-md-10">


                                                <button type="submit" class="btn btn-success mr-10" name="btn_submit">
                                                    Submit
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
                <!-- /Row -->

            </div>
            <!-- /Main Content -->
