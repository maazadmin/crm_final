<!-- Main Content -->
<div class="page-wrapper">
    <div class="container-fluid">

        <!-- Title -->
        <div class="row heading-bg">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h5 class="txt-dark"><?= $heading ?></h5>
            </div>

            <!-- Breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="index.html">Dashboard</a></li>
                    <li><a href="active"><span><?= $heading ?></span></a></li>

                </ol>
            </div>
            <!-- /Breadcrumb -->

        </div>
        <!-- /Title -->

        <!-- Row -->
        <div class="row">
            <div class="col-lg-6 col-sm-12">
                <div class="panel panel-default card-view">
                    <div class="panel-heading">
                        <div class="pull-left">
                            <h6 class="panel-title txt-dark"><?php echo $title; ?></h6>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body">
                            <p class="text-muted"><?php echo $description; ?></p>

                            <div class="tab-struct custom-tab-1 mt-40">
                                <ul role="tablist" class="nav nav-tabs" id="myTabs_7">
                                    <li class="active" role="presentation"><a aria-expanded="true" data-toggle="tab"
                                                                              role="tab" id="home_tab_7"
                                                                              href="#purchase_tab">Purchases</a></li>
                                    <li role="presentation" class=""><a data-toggle="tab" id="profile_tab_7" role="tab"
                                                                        href="#purchase_return_tab"
                                                                        aria-expanded="false">Purchase Return</a></li>
                                    <li role="presentation" class=""><a data-toggle="tab" id="profile_tab_7" role="tab"
                                                                        href="#sales_tab"
                                                                        aria-expanded="false">Sales</a></li>
                                    <li role="presentation" class=""><a data-toggle="tab" id="profile_tab_7" role="tab"
                                                                        href="#sales_return_tab"
                                                                        aria-expanded="false">Sales Return</a></li>
                                    <li role="presentation" class=""><a data-toggle="tab" id="profile_tab_7" role="tab"
                                                                        href="#salary_tab"
                                                                        aria-expanded="false">Salary</a></li>

                                </ul>
                                <div class="tab-content" id="myTabContent_7">
                                    <!-- Purchase Tab -->
                                    <div id="purchase_tab" class="tab-pane fade active in" role="tabpanel">
                                        <div class="form-wrap">
                                            <?php
                                            //Show message
                                            echo $this->session->flashdata("msg");
                                            ?>
                                            <form method="post" class="ac-type-form" enctype="multipart/form-data"
                                                  action="<?= site_url($this->uri->segment(1) . '/ledger_settings') ?>"
                                                  data-toggle="validator" role="form">
                                                <input type="hidden" name="po_gl_enabled"
                                                       value="<?php echo $po_gl_enabled['setting_id']; ?>">
                                                <input type="hidden" name="po_credit_gl_id"
                                                       value="<?php echo $po_credit_gl_id['setting_id']; ?>">
                                                <input type="hidden" name="po_debit_gl_id"
                                                       value="<?php echo $po_debit_gl_id['setting_id']; ?>">
                                                <input type="hidden" name="po_gl_note"
                                                       value="<?php echo $po_gl_note['setting_id']; ?>">

                                                <!--<h1><?php /*echo $po_gl_note['setting_id']; */?></h1>-->

                                                <div class="row">
                                                    <div class="form-group col-md-4">
                                                        <label class="control-label mb-10"
                                                               for="exampleInputuname_1">Enable</label>

                                                        <select class="form-control" name="po_is_enable">
                                                            <option
                                                                value="1" <?php echo ($po_gl_enabled['value'] == 1) ? "selected" : ""; ?>>
                                                                Yes
                                                            </option>

                                                            <option
                                                                value="0" <?php echo ($po_gl_enabled['value'] == 0) ? "selected" : ""; ?>>
                                                                No
                                                            </option>
                                                        </select>

                                                    </div>

                                                    <div class="form-group col-md-4">
                                                        <label class="control-label mb-10"
                                                               for="exampleInputuname_1">Credit To</label>

                                                        <select class="form-control select2-select-00"
                                                                name="po_gl_credit_account">
                                                            <option value="">Select Account</option>
                                                            <?php foreach ($ac_list as $row): ?>
                                                                <option
                                                                    value="<?= $row['gl_account_id'] ?>" <?php echo ($po_credit_gl_id['value'] == $row['gl_account_id']) ? "selected" : ""; ?>><?= $row['gl_account_name'] ?></option>
                                                            <?php endforeach; ?>
                                                        </select>

                                                    </div>

                                                    <div class="form-group col-md-4">
                                                        <label class="control-label mb-10"
                                                               for="exampleInputuname_1">Debit To</label>

                                                        <select class="form-control select2-select-00"
                                                                name="po_gl_account_debit">
                                                            <option value="">Select Account</option>
                                                            <?php foreach ($ac_list as $row): ?>
                                                                <option
                                                                    value="<?= $row['gl_account_id'] ?>" <?php echo ($po_debit_gl_id['value'] == $row['gl_account_id']) ? "selected" : ""; ?>><?= $row['gl_account_name'] ?></option>
                                                            <?php endforeach; ?>
                                                        </select>

                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="form-group col-md-4">
                                                        <label class="control-label mb-10"
                                                               for="exampleInputuname_1">Naration</label>

                                                        <textarea class="form-control" name="po_notes"><?php echo $po_gl_note['value']; ?></textarea>

                                                    </div>
                                                </div>


                                                <div class="form-group col-md-10">


                                                    <button type="submit" class="btn btn-success mr-10"
                                                            name="btn_purchase_submit">
                                                        Save Changes
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>

                                    <!-- Purchase Return Tab -->
                                    <div id="purchase_return_tab" class="tab-pane fade" role="tabpanel">
                                        <div class="form-wrap">
                                            <?php
                                            //Show message
                                            echo $this->session->flashdata("msg");
                                            ?>
                                            <form method="post" class="ac-type-form" enctype="multipart/form-data"
                                                  action="<?= site_url($this->uri->segment(1) . '/ledger_settings') ?>"
                                                  data-toggle="validator" role="form">
                                                <div class="row">
                                                    <div class="form-group col-md-4">
                                                        <label class="control-label mb-10"
                                                               for="exampleInputuname_1">Enable</label>

                                                        <select class="form-control">
                                                            <option value="1">Yes</option>
                                                            <option value="0">No</option>
                                                        </select>

                                                    </div>

                                                    <div class="form-group col-md-4">
                                                        <label class="control-label mb-10"
                                                               for="exampleInputuname_1">Credit To</label>

                                                        <select class="form-control select2-select-00">
                                                            <option value="">Select Account</option>
                                                            <?php foreach ($ac_list as $row): ?>
                                                                <option
                                                                    value="<?= $row['gl_account_id'] ?>"><?= $row['gl_account_name'] ?></option>
                                                            <?php endforeach; ?>
                                                        </select>

                                                    </div>

                                                    <div class="form-group col-md-4">
                                                        <label class="control-label mb-10"
                                                               for="exampleInputuname_1">Debit To</label>

                                                        <select class="form-control select2-select-00">
                                                            <option value="">Select Account</option>
                                                            <?php foreach ($ac_list as $row): ?>
                                                                <option
                                                                    value="<?= $row['gl_account_id'] ?>"><?= $row['gl_account_name'] ?></option>
                                                            <?php endforeach; ?>
                                                        </select>

                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="form-group col-md-4">
                                                        <label class="control-label mb-10"
                                                               for="exampleInputuname_1">Naration</label>

                                                        <textarea class="form-control"></textarea>

                                                    </div>
                                                </div>


                                                <div class="form-group col-md-10">


                                                    <button type="submit" class="btn btn-success mr-10"
                                                            name="btn_submit">
                                                        Save Changes
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>

                                    <!-- Sales Tab -->
                                    <div id="sales_tab" class="tab-pane fade" role="tabpanel">
                                        <div class="form-wrap">
                                            <?php
                                            //Show message
                                            echo $this->session->flashdata("msg");
                                            ?>
                                            <form method="post" class="ac-type-form" enctype="multipart/form-data"
                                                  action="<?= site_url($this->uri->segment(1) . '/add') ?>"
                                                  data-toggle="validator" role="form">
                                                <div class="row">
                                                    <div class="form-group col-md-4">
                                                        <label class="control-label mb-10"
                                                               for="exampleInputuname_1">Enable</label>

                                                        <select class="form-control">
                                                            <option value="1">Yes</option>
                                                            <option value="0">No</option>
                                                        </select>

                                                    </div>

                                                    <div class="form-group col-md-4">
                                                        <label class="control-label mb-10"
                                                               for="exampleInputuname_1">Credit To</label>

                                                        <select class="form-control select2-select-00">
                                                            <option value="">Select Account</option>
                                                            <?php foreach ($ac_list as $row): ?>
                                                                <option
                                                                    value="<?= $row['gl_account_id'] ?>"><?= $row['gl_account_name'] ?></option>
                                                            <?php endforeach; ?>
                                                        </select>

                                                    </div>

                                                    <div class="form-group col-md-4">
                                                        <label class="control-label mb-10"
                                                               for="exampleInputuname_1">Debit To</label>

                                                        <select class="form-control select2-select-00">
                                                            <option value="">Select Account</option>
                                                            <?php foreach ($ac_list as $row): ?>
                                                                <option
                                                                    value="<?= $row['gl_account_id'] ?>"><?= $row['gl_account_name'] ?></option>
                                                            <?php endforeach; ?>
                                                        </select>

                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="form-group col-md-4">
                                                        <label class="control-label mb-10"
                                                               for="exampleInputuname_1">Naration</label>

                                                        <textarea class="form-control"></textarea>

                                                    </div>
                                                </div>


                                                <div class="form-group col-md-10">


                                                    <button type="submit" class="btn btn-success mr-10"
                                                            name="btn_submit">
                                                        Save Changes
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>

                                    <!-- Sales Return Tab -->
                                    <div id="sales_return_tab" class="tab-pane fade" role="tabpanel">
                                        <div class="form-wrap">
                                            <?php
                                            //Show message
                                            echo $this->session->flashdata("msg");
                                            ?>
                                            <form method="post" class="ac-type-form" enctype="multipart/form-data"
                                                  action="<?= site_url($this->uri->segment(1) . '/add') ?>"
                                                  data-toggle="validator" role="form">
                                                <div class="row">
                                                    <div class="form-group col-md-4">
                                                        <label class="control-label mb-10"
                                                               for="exampleInputuname_1">Enable</label>

                                                        <select class="form-control">
                                                            <option value="1">Yes</option>
                                                            <option value="0">No</option>
                                                        </select>

                                                    </div>

                                                    <div class="form-group col-md-4">
                                                        <label class="control-label mb-10"
                                                               for="exampleInputuname_1">Credit To</label>

                                                        <select class="form-control select2-select-00">
                                                            <option value="">Select Account</option>
                                                            <?php foreach ($ac_list as $row): ?>
                                                                <option
                                                                    value="<?= $row['gl_account_id'] ?>"><?= $row['gl_account_name'] ?></option>
                                                            <?php endforeach; ?>
                                                        </select>

                                                    </div>

                                                    <div class="form-group col-md-4">
                                                        <label class="control-label mb-10"
                                                               for="exampleInputuname_1">Debit To</label>

                                                        <select class="form-control select2-select-00">
                                                            <option value="">Select Account</option>
                                                            <?php foreach ($ac_list as $row): ?>
                                                                <option
                                                                    value="<?= $row['gl_account_id'] ?>"><?= $row['gl_account_name'] ?></option>
                                                            <?php endforeach; ?>
                                                        </select>

                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="form-group col-md-4">
                                                        <label class="control-label mb-10"
                                                               for="exampleInputuname_1">Naration</label>

                                                        <textarea class="form-control"></textarea>

                                                    </div>
                                                </div>


                                                <div class="form-group col-md-10">


                                                    <button type="submit" class="btn btn-success mr-10"
                                                            name="btn_submit">
                                                        Save Changes
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>

                                    <!-- Sales Return Tab -->
                                    <div id="salary_tab" class="tab-pane fade" role="tabpanel">
                                        <div class="form-wrap">
                                            <?php
                                            //Show message
                                            echo $this->session->flashdata("msg");
                                            ?>
                                            <form method="post" class="ac-type-form" enctype="multipart/form-data"
                                                  action="<?= site_url($this->uri->segment(1) . '/add') ?>"
                                                  data-toggle="validator" role="form">
                                                <div class="row">
                                                    <div class="form-group col-md-4">
                                                        <label class="control-label mb-10"
                                                               for="exampleInputuname_1">Enable</label>

                                                        <select class="form-control">
                                                            <option value="1">Yes</option>
                                                            <option value="0">No</option>
                                                        </select>

                                                    </div>

                                                    <div class="form-group col-md-4">
                                                        <label class="control-label mb-10"
                                                               for="exampleInputuname_1">Credit To</label>

                                                        <select class="form-control select2-select-00">
                                                            <option value="">Select Account</option>
                                                            <?php foreach ($ac_list as $row): ?>
                                                                <option
                                                                    value="<?= $row['gl_account_id'] ?>"><?= $row['gl_account_name'] ?></option>
                                                            <?php endforeach; ?>
                                                        </select>

                                                    </div>

                                                    <div class="form-group col-md-4">
                                                        <label class="control-label mb-10"
                                                               for="exampleInputuname_1">Debit To</label>

                                                        <select class="form-control select2-select-00">
                                                            <option value="">Select Account</option>
                                                            <?php foreach ($ac_list as $row): ?>
                                                                <option
                                                                    value="<?= $row['gl_account_id'] ?>"><?= $row['gl_account_name'] ?></option>
                                                            <?php endforeach; ?>
                                                        </select>

                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="form-group col-md-4">
                                                        <label class="control-label mb-10"
                                                               for="exampleInputuname_1">Naration</label>

                                                        <textarea class="form-control"></textarea>

                                                    </div>
                                                </div>


                                                <div class="form-group col-md-10">


                                                    <button type="submit" class="btn btn-success mr-10"
                                                            name="btn_submit">
                                                        Save Changes
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- /Row -->