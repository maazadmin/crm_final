 Main Content -->
<div class="page-wrapper">
    <div class="container-fluid">

        <!-- Title -->
        <div class="row heading-bg">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h5 class="txt-dark"><?= $heading ?></h5>
            </div>

            <!-- Breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="index.html">Dashboard</a></li>
                    <li><a href="active"><span><?= $heading ?></span></a></li>

                </ol>
            </div>
            <!-- /Breadcrumb -->

        </div>
        <!-- /Title -->

        <!-- Row -->
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default card-view">
                    <div class="panel-heading">
                        <div class="pull-left">
                            <h6 class="panel-title txt-dark"><?php echo $title; ?></h6>
                        </div>

                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body">
                             <div class="">
                            <a href="<?= site_url($this->uri->segment(1)) ?>" class="btn btn-success btn-sm">View
                                List</a>
                        </div>
                        <br>
                            <p class="text-muted"><?php echo $description; ?>.</p>

                            <div class="row">
                                <div class="col-sm-12 col-xs-12">
                                    <div class="form-wrap">
                                      
                                        <form method="post" class="ac-type-form" enctype="multipart/form-data"
                                              action="<?= site_url($this->uri->segment(1) . '/add') ?>"
                                              data-toggle="validator" role="form">
                                            <div class="form-group col-md-4">
                                                <label class="control-label mb-10" for="exampleInputuname_1">Shift 
                                                    Name
                                                </label>
                                                <input type="text" class="form-control"
                                                       name="shift_name" placeholder="Shift Name" required>
                                            </div>
                                               <div class="form-group col-md-4">
                                                        <div class="form-group">
                                                            <label class="control-label mb-10 text-left">Start Time</label>
                                                            <div class='input-group date' >
                                                                <input type='text' name="shift_start"  class="form-control datetimepicker2" 
                                                                placeholder="Select Start Time"
                                                                 />
                                                                <span class="input-group-addon">
                                                                    <span class=""></span>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                       <div class="form-group col-md-4">
                                                        <div class="form-group">
                                                            <label class="control-label mb-10 text-left">End Time</label>
                                                            <div class='input-group date '>
                                                                <input type='text' name="shift_end" class="form-control datetimepicker2"
                                                                placeholder="Select End Time" />
                                                                <span class="input-group-addon">
                                                                    <span class=""></span>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                            
                                           <!--    <div class="form-group col-md-4">
                                                <label class="control-label mb-10" for="exampleInputuname_1">Shift 
                                                    end
                                                </label>
                                                <input type="date" class="form-control"
                                                       name="shift_end" placeholder="Shift End" required>
                                            </div>
 -->
                                         



                                            <div class="form-group col-md-10">


                                                <button type="submit" class="btn btn-success mr-10" name="btn_submit">
                                                    Submit
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
                <!-- /Row -->

            </div>
            <!-- /Main Content -->
            <!-- <script>
                 //Form Validation A/C Type

                 $('.ac-type-form').validate(
                     {

                         errorElement: 'div',
                         errorClass: 'help-block',
                         focusInvalid: false,
                         rules: {
                             //Personal Info

                             account_type: {required: true},

                         },

                         messages: {

                             account_type: " Please enter Account Type",


                         },
                         invalidHandler: function (event, validator) { //display error alert on form submit
                             $('.alert-danger', $('.add_new_ajeer')).show();
                         },

                         highlight: function (e) {
                             $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
                         },

                         success: function (e) {
                             $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
                             $(e).remove();
                         },
                         errorPlacement: function (error, element) {
                             error.insertAfter(element.parent());
                         },

                     });
             </script>