<!-- Main Content -->
<div class="page-wrapper">
    <div class="container-fluid">

        <!-- Title -->
        <div class="row heading-bg">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h5 class="txt-dark"><?= $heading?></h5>
            </div>

            <!-- Breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="index.html">Dashboard</a></li>
                    <li><a href="active"><span><?= $heading?></span></a></li>

                </ol>
            </div>
            <!-- /Breadcrumb -->

        </div>
        <!-- /Title -->

        <!-- Row -->
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default card-view">
                    <div class="panel-heading">
                        <div class="pull-left">
                            <h6 class="panel-title txt-dark"><?php echo $title; ?></h6>
                        </div>
                        <div class="pull-right">
                            <a href="<?= site_url($this->uri->segment(1))?>" class="btn btn-success btn-sm">View
                                List</a>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body">
                            <p class="text-muted"><?php echo $description; ?>.</p>

                            <div class="row">
                                <div class="col-sm-12 col-xs-12">
                                    <div class="form-wrap">
                                        <?php
                                        //Show message
                                        echo $this->session->flashdata("msg");
                                        ?>
                                        <form method="post"  enctype="multipart/form-data"
                                              action="<?= site_url($this->uri->segment(1) . '/edit/' . (($item !== false) ? $item[$pmKey] : "")) ?>" data-toggle="validator" role="form">
                                            <div class="row">
                                                <div class="form-group col-md-4">
                                                    <label class="control-label mb-10"
                                                           for="exampleInputuname_1">Title</label>

                                                    <input type="text" class="form-control" name="title"
                                                           placeholder="Title" value="<?php echo ($item!== false)?$item['title']: ""; ?>" required>
                                                </div>

                                                <div class="form-group col-md-4">
                                                    <label class="control-label mb-10"
                                                           for="exampleInputEmail_1">Address</label>

                                                    <textarea class="form-control" name="address"
                                                              placeholder="Address"><?php echo ($item!== false)?$item['address']: ""; ?></textarea>
                                                </div>

                                                <div class="form-group col-md-4">
                                                    <label class="control-label mb-10"
                                                           for="exampleInputEmail_1">City</label>

                                                    <input type="text" class="form-control" name="city"
                                                           placeholder="City" value="<?php echo ($item!== false)?$item['city']: ""; ?>">
                                                </div>

                                            </div>

                                            <div class="row">
                                                <div class="form-group col-md-4">
                                                    <label class="control-label mb-10"
                                                           for="exampleInputEmail_1">Country</label>

                                                    <input type="text" class="form-control" name="country"
                                                           placeholder="Country" value="<?php echo ($item!== false)?$item['country']: ""; ?>">
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label class="control-label mb-10"
                                                           for="exampleInputEmail_1">Person</label>

                                                    <input type="text" class="form-control" name="person"
                                                           placeholder="Person" value="<?php echo ($item!== false)?$item['person']: ""; ?>">
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label class="control-label mb-10"
                                                           for="exampleInputEmail_1">Person Email</label>

                                                    <input type="text" class="form-control" name="person_email"
                                                           placeholder="Email" value="<?php echo ($item!== false)?$item['person_email']: ""; ?>">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="form-group col-md-4">
                                                    <label class="control-label mb-10"
                                                           for="exampleInputEmail_1">Telephone #</label>

                                                    <input type="text" class="form-control" name="telephone"
                                                           placeholder="Telephone" value="<?php echo ($item!== false)?$item['telephone']: ""; ?>">
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label class="control-label mb-10"
                                                           for="exampleInputEmail_1">Fax #</label>

                                                    <input type="text" class="form-control" name="fax"
                                                           placeholder="Fax #" value="<?php echo ($item!== false)?$item['fax']: ""; ?>">
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label class="control-label mb-10"
                                                           for="exampleInputEmail_1">Email</label>

                                                    <input type="text" class="form-control" name="email"
                                                           placeholder="Email" value="<?php echo ($item!==false)?$item['email']:"";?>" >
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="form-group col-md-4">
                                                    <label class="control-label mb-10"
                                                           for="exampleInputEmail_1">Cell #</label>

                                                    <input type="text" class="form-control" name="cell"
                                                           placeholder="cell" value="<?php echo ($item!==false)?$item['cell']:"";?>" >
                                                </div>
                                            </div>

                                            <div class="form-group col-md-10">


                                                <button type="submit" class="btn btn-success mr-10" name="btn_submit">
                                                    Submit
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
                <!-- /Row -->

            </div>
            <!-- /Main Content -->