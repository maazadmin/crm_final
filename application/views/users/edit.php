<!-- Main Content -->
<div class="page-wrapper">
<div class="container-fluid">

<!-- Title -->
<div class="row heading-bg">
<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
<h5 class="txt-dark"><?= $heading ?></h5>
</div>

<!-- Breadcrumb -->
<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
<ol class="breadcrumb">
<li><a href="index.html">Dashboard</a></li>
<li><a href="active"><span><?= $heading ?></span></a></li>

</ol>
</div>
<!-- /Breadcrumb -->

</div>
<!-- /Title -->

<!-- Row -->
<div class="row">
<div class="col-md-12">
<div class="panel panel-default card-view">
<div class="panel-heading">
<div class="pull-left">
<h6 class="panel-title txt-dark"><?php echo $title; ?></h6>
</div>

<div class="clearfix"></div>
</div>
<div class="panel-wrapper collapse in">
<div class="panel-body">
<div class="">
<a href="<?= site_url($this->uri->segment(1)) ?>" class="btn btn-success btn-sm">View
List</a>
</div>
<br>
<p class="text-muted"><?php echo $description; ?>.</p>

<div class="row">
<div class="col-sm-12 col-xs-12">
<div class="form-wrap">
    <?php
    //Show message
    echo $this->session->flashdata("msg");
    ?>
    <form method="post" class="ac-type-form" enctype="multipart/form-data"
          action="<?= site_url($this->uri->segment(1) . '/edit/' . (($item !== false) ? $item[$pmKey] : "")) ?>"
          data-toggle="validator" role="form">


        <div class="row">
            <input type="text" hidden="hidden" name="roles_id" value="<?php echo $role_data[0]->user_role_id; ?>">
                             <div class="form-group col-md-4">
                                                <label class="control-label mb-10" for="exampleInputuname_1">User Type
                                                </label>
<select class="form-control"
        name="user_type_id" id="role_id">
    <option value="">User Type</option>
    <?php foreach ($roles_list as $row) :
        if ($row['user_type_id'] == $item['role_id']) {
                                                                $selected = "selected";
                                                            } else {
                                                                $selected = "";
                                                            }
     ?>

        <option <?= $selected ?>
            value="<?= $row['role_id']; ?>"><?= $row['role_name']; ?></option>
    <?php endforeach; ?>
</select>
                                            </div>

                                                <div class="form-group col-md-4">
                                                <label class="control-label mb-10" for="exampleInputuname_1">Region 
                                                </label>
            <select class="form-control" name="region_id" id="region_id">
                         <option value="">Select Region</option>
                                  <?php foreach ($region_list as $row) : 
                                     if ($row['region_id'] == $item['region_id']) {
                                                                $selected = "selected";
                                                            } else {
                                                                $selected = "";
                                                            }
                                    ?>

                                 <option <?= $selected ?>
                                         value="<?= $row['region_id']; ?>"><?= $row['region_name']; ?></option>
                                     <?php endforeach; ?>
                                                                                </select>
                                            </div>

                                              <div class="form-group col-md-4">
                                                <label class="control-label mb-10" for="exampleInputuname_1">Campaign 
                                                </label>
                                                <div class="clearfix"></div>
                                             <!--    <div class="clearfix"></div> -->
   <select class="form-control" name="campaign_value[]" id="campaign-drop" multiple="multiple">
    <?php foreach ($campaign_list as $row) : ?>

                <option  value="<?= $row['campaign_id']; ?>"><?= $row['campaign_name']; ?></option>
                         <?php endforeach; ?>
</select>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-4">
                                                <label class="control-label mb-10" for="exampleInputuname_1">Currency 
                                                </label>
                    <select class="form-control" name="currency_id" id="currency_id">
                                            <option value="">Select Currency</option>
                         <?php foreach ($currency_list as $row) :
 if ($row['currency_id'] == $item['currency_id']) {
                                                                $selected = "selected";
                                                            } else {
                                                                $selected = "";
                                                            }

                          ?>

                <option <?= $selected ?>  value="<?= $row['currency_id']; ?>"><?= $row['currency_name']; ?></option>
                         <?php endforeach; ?>
                                                                                </select>
                                            </div>
                                              <div class="form-group col-md-4">
                                                <label class="control-label mb-10" for="exampleInputuname_1">Shift 
                                                </label>
                                                <div class="clearfix"></div>
                                                <select class="form-control" name="shift_value[]" id="shift-drop" multiple="multiple">
    <?php foreach ($shift_list as $row) : ?>

                <option  value="<?= $row['shift_id']; ?>"><?= $row['shift_name']; ?></option>
                         <?php endforeach; ?>
</select>
                
                                            </div>
                                            <div class="form-group col-md-4">
                                                <label class="control-label mb-10" for="exampleInputuname_1">Product 
                                                </label>
                                                <div class="clearfix"></div>
                                             <!--    <div class="clearfix"></div> -->
   <select class="form-control" name="product_value[]" id="product-drop" multiple="multiple">
    <?php foreach ($product_list as $row) : ?>

                <option  value="<?= $row['product_id']; ?>"><?= $row['product_name']; ?></option>
                         <?php endforeach; ?>
</select>
                                            </div>
                                        </div>
                                        <div class="row">

                                            <div class="form-group col-md-4">
                                                <label class="control-label mb-10" for="exampleInputuname_1">Center 
                                                </label>
                    <select class="form-control" name="center_id" id="center_id">
                                            <option value="">Select Center</option>
                         <?php foreach ($center_list as $row) :
                            if ($row['center_id'] == $item['center_id']) {
                                                                $selected = "selected";
                                                            } else {
                                                                $selected = "";
                                                            }
                          ?>

                <option <?= $selected ?>  value="<?= $row['center_id']; ?>"><?= $row['center_name']; ?></option>
                         <?php endforeach; ?>
                                                                                </select>
                                            </div>

            <div class="form-group col-md-4">
                <label class="control-label mb-10" for="exampleInputuname_1">Username
                </label>
                <input type="text" class="form-control"
                       name="username" placeholder="Username" required
                       value="<?php echo ($item !== false) ? $item['username'] : ""; ?>">
            </div>


            <div class="form-group col-md-4">
                <label class="control-label mb-10" for="exampleInputuname_1">Email
                </label>
                <input type="text" class="form-control"
                       name="email" placeholder="Email"
                       value="<?php echo ($item !== false) ? $item['email'] : ""; ?>"
                    >

            </div>
</div>
<div class="row">
            <div class="form-group col-md-4">
                <label class="control-label mb-10" for="exampleInputuname_1">Full
                    Name
                </label>
                <input type="text" class="form-control"
                       name="full_name" placeholder="Full Name"
                       value="<?php echo ($item !== false) ? $item['full_name'] : ""; ?>"
                    >
            </div>
         <!--    <div class="form-group col-md-4">
                                                <label class="control-label mb-10" for="exampleInputuname_1">Password

                                                </label>
                                                <input type="password" class="form-control"
                                                       name="password" placeholder="password"
                                                       value="<?php echo ($item !== false) ? $item['password'] : ""; ?>"
                                                       required>
                                            </div> -->

       
        

            <!--<div class="form-group col-md-4">
                <label class="control-label mb-10" for="exampleInputuname_1">Password

                </label>
                <input type="password" class="form-control"
                       name="password" placeholder="password"
                        disabled>
            </div>-->

            <div class="form-group col-md-4">
                <label class="control-label mb-10" for="exampleInputuname_1">Active
                    Login

                </label>
                <select class="form-control" name="active_login">
                    <option value="">Select Option</option>
                    <option
                        value="1" <?php echo ($item['is_login_allowed'] == 1) ? "selected" : ""; ?>>
                        Yes
                    </option>
                    <option
                        value="0" <?php echo ($item['is_login_allowed'] == 0) ? "selected" : ""; ?>>
                        No
                    </option>
                </select>
            </div>
        </div>




        <div class="form-group col-md-10">


            <button type="submit" class="btn btn-success mr-10" name="btn_submit">
                Submit
            </button>
        </div>
    </form>
</div>
</div>
</div>
</div>
</div>


</div>
<!-- /Row -->

</div>
<!-- /Main Content -->
