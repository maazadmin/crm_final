<!-- Main Content -->
<div class="page-wrapper">
    <div class="container-fluid">

        <!-- Title -->
        <div class="row heading-bg">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h5 class="txt-dark"><?= $heading ?></h5>
            </div>

            <!-- Breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="index.html">Dashboard</a></li>
                    <li><a href="active"><span><?= $heading ?></span></a></li>

                </ol>
            </div>
            <!-- /Breadcrumb -->

        </div>
        <!-- /Title -->

        <!-- Row -->
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default card-view">
                    <div class="panel-heading">
                        <div class="pull-left">
                            <h6 class="panel-title txt-dark"><?php echo $title; ?></h6>
                        </div>
                        <div class="pull-right">
                            <a href="<?= site_url($this->uri->segment(1)) ?>" class="btn btn-success btn-sm">View
                                List</a>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body">
                            <p class="text-muted"><?php echo $description; ?>.</p>

                            <div class="row">
                                <div class="col-sm-12 col-xs-12">
                                    <div class="form-wrap">
                                        <?php
                                        //Show message
                                        echo $this->session->flashdata("msg");
                                        ?>
                                        <form method="post" class="ac-type-form" enctype="multipart/form-data"
                                              action="<?= site_url($this->uri->segment(1) . '/my_account') ?>"
                                              data-toggle="validator" role="form">
                                            <div class="row">
                                                <div class="form-group col-md-4">
                                                    <label class="control-label mb-10" for="exampleInputuname_1">Username
                                                    </label>
                                                    <input type="text" class="form-control"
                                                           name="username" placeholder="Username" readonly value="<?= ($item !== false) ? $item['username'] : ""; ?>">
                                                </div>


                                                <div class="form-group col-md-4">
                                                    <label class="control-label mb-10" for="exampleInputuname_1">Email
                                                    </label>
                                                    <input type="text" class="form-control"
                                                           name="email" placeholder="Email" value="<?= ($item !== false) ? $item['email'] : ""; ?>"
                                                           required
                                                        >
                                                </div>

                                                <!--<div class="form-group col-md-4">
                                                    <label class="control-label mb-10" for="exampleInputuname_1">Full
                                                        Name
                                                    </label>
                                                    <input type="text" class="form-control"
                                                           name="full_name" placeholder="Full Name"
                                                        >
                                                </div>-->
                                                <div class="form-group col-md-4">
                                                    <label class="control-label mb-10" for="exampleInputuname_1">Change
                                                        Password

                                                    </label>
                                                    <input type="checkbox" name="change_password" id="change_password">
                                                </div>

                                            </div>
                                            <div class="row">



                                                <div class="form-group col-md-4">
                                                    <label class="control-label mb-10" for="exampleInputuname_1">Password

                                                    </label>
                                                    <input type="password" class="form-control"
                                                           name="password" placeholder="password"
                                                             id="pass"  disabled>
                                                    <div class="help-block with-errors"></div>
                                                </div>

                                                <div class="form-group col-md-4">
                                                    <label class="control-label mb-10" for="exampleInputuname_1">Confirm
                                                        Password

                                                    </label>
                                                    <input type="password" class="form-control"
                                                           name="confirm_password" placeholder="Confirm Password" disabled
                                                           data-match="#pass"  data-match-error="Whoops, these don't match" id="confirm_pass">
                                                    <div class="help-block with-errors"></div>
                                                </div>


                                            </div>


                                            <div class="form-group col-md-10">


                                                <button type="submit" class="btn btn-success mr-10" name="btn_submit">
                                                    Submit
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
                <!-- /Row -->

            </div>
            <!-- /Main Content -->
